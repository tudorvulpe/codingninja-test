-- phpMyAdmin SQL Dump
-- version 4.7.8
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 20, 2018 at 03:14 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.1.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wp_e0eov`
--

-- --------------------------------------------------------

--
-- Table structure for table `r8ISTpM09o_commentmeta`
--

CREATE TABLE `r8ISTpM09o_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `r8ISTpM09o_comments`
--

CREATE TABLE `r8ISTpM09o_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `r8ISTpM09o_comments`
--

INSERT INTO `r8ISTpM09o_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'A WordPress Commenter', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2018-08-14 09:01:28', '2018-08-14 09:01:28', 'Hi, this is a comment.\nTo get started with moderating, editing, and deleting comments, please visit the Comments screen in the dashboard.\nCommenter avatars come from <a href=\"https://gravatar.com\">Gravatar</a>.', 0, '1', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `r8ISTpM09o_failed_jobs`
--

CREATE TABLE `r8ISTpM09o_failed_jobs` (
  `id` bigint(20) NOT NULL,
  `job` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `r8ISTpM09o_links`
--

CREATE TABLE `r8ISTpM09o_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `r8ISTpM09o_mailchimp_carts`
--

CREATE TABLE `r8ISTpM09o_mailchimp_carts` (
  `id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `cart` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `r8ISTpM09o_options`
--

CREATE TABLE `r8ISTpM09o_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `r8ISTpM09o_options`
--

INSERT INTO `r8ISTpM09o_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'https://cndeveloper.loc', 'yes'),
(2, 'home', 'https://cndeveloper.loc', 'yes'),
(3, 'blogname', 'CodingNinja Test', 'yes'),
(4, 'blogdescription', '', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'tudor@contursoft.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '1', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'F j, Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:195:{s:24:\"^wc-auth/v([1]{1})/(.*)?\";s:63:\"index.php?wc-auth-version=$matches[1]&wc-auth-route=$matches[2]\";s:22:\"^wc-api/v([1-3]{1})/?$\";s:51:\"index.php?wc-api-version=$matches[1]&wc-api-route=/\";s:24:\"^wc-api/v([1-3]{1})(.*)?\";s:61:\"index.php?wc-api-version=$matches[1]&wc-api-route=$matches[2]\";s:7:\"shop/?$\";s:27:\"index.php?post_type=product\";s:37:\"shop/feed/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?post_type=product&feed=$matches[1]\";s:32:\"shop/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?post_type=product&feed=$matches[1]\";s:24:\"shop/page/([0-9]{1,})/?$\";s:45:\"index.php?post_type=product&paged=$matches[1]\";s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:32:\"category/(.+?)/wc-api(/(.*))?/?$\";s:54:\"index.php?category_name=$matches[1]&wc-api=$matches[3]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:29:\"tag/([^/]+)/wc-api(/(.*))?/?$\";s:44:\"index.php?tag=$matches[1]&wc-api=$matches[3]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:55:\"product-category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_cat=$matches[1]&feed=$matches[2]\";s:50:\"product-category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_cat=$matches[1]&feed=$matches[2]\";s:31:\"product-category/(.+?)/embed/?$\";s:44:\"index.php?product_cat=$matches[1]&embed=true\";s:43:\"product-category/(.+?)/page/?([0-9]{1,})/?$\";s:51:\"index.php?product_cat=$matches[1]&paged=$matches[2]\";s:25:\"product-category/(.+?)/?$\";s:33:\"index.php?product_cat=$matches[1]\";s:52:\"product-tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_tag=$matches[1]&feed=$matches[2]\";s:47:\"product-tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_tag=$matches[1]&feed=$matches[2]\";s:28:\"product-tag/([^/]+)/embed/?$\";s:44:\"index.php?product_tag=$matches[1]&embed=true\";s:40:\"product-tag/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?product_tag=$matches[1]&paged=$matches[2]\";s:22:\"product-tag/([^/]+)/?$\";s:33:\"index.php?product_tag=$matches[1]\";s:35:\"product/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:45:\"product/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:65:\"product/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:60:\"product/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:60:\"product/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:41:\"product/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:24:\"product/([^/]+)/embed/?$\";s:40:\"index.php?product=$matches[1]&embed=true\";s:28:\"product/([^/]+)/trackback/?$\";s:34:\"index.php?product=$matches[1]&tb=1\";s:48:\"product/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?product=$matches[1]&feed=$matches[2]\";s:43:\"product/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?product=$matches[1]&feed=$matches[2]\";s:36:\"product/([^/]+)/page/?([0-9]{1,})/?$\";s:47:\"index.php?product=$matches[1]&paged=$matches[2]\";s:43:\"product/([^/]+)/comment-page-([0-9]{1,})/?$\";s:47:\"index.php?product=$matches[1]&cpage=$matches[2]\";s:33:\"product/([^/]+)/wc-api(/(.*))?/?$\";s:48:\"index.php?product=$matches[1]&wc-api=$matches[3]\";s:39:\"product/[^/]+/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:50:\"product/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:32:\"product/([^/]+)(?:/([0-9]+))?/?$\";s:46:\"index.php?product=$matches[1]&page=$matches[2]\";s:24:\"product/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:34:\"product/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:54:\"product/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:49:\"product/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:49:\"product/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:30:\"product/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:41:\"hm_custom_css/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:51:\"hm_custom_css/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:71:\"hm_custom_css/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:66:\"hm_custom_css/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:66:\"hm_custom_css/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:47:\"hm_custom_css/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:30:\"hm_custom_css/([^/]+)/embed/?$\";s:46:\"index.php?hm_custom_css=$matches[1]&embed=true\";s:34:\"hm_custom_css/([^/]+)/trackback/?$\";s:40:\"index.php?hm_custom_css=$matches[1]&tb=1\";s:42:\"hm_custom_css/([^/]+)/page/?([0-9]{1,})/?$\";s:53:\"index.php?hm_custom_css=$matches[1]&paged=$matches[2]\";s:49:\"hm_custom_css/([^/]+)/comment-page-([0-9]{1,})/?$\";s:53:\"index.php?hm_custom_css=$matches[1]&cpage=$matches[2]\";s:39:\"hm_custom_css/([^/]+)/wc-api(/(.*))?/?$\";s:54:\"index.php?hm_custom_css=$matches[1]&wc-api=$matches[3]\";s:45:\"hm_custom_css/[^/]+/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:56:\"hm_custom_css/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:38:\"hm_custom_css/([^/]+)(?:/([0-9]+))?/?$\";s:52:\"index.php?hm_custom_css=$matches[1]&page=$matches[2]\";s:30:\"hm_custom_css/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:40:\"hm_custom_css/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:60:\"hm_custom_css/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:55:\"hm_custom_css/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:55:\"hm_custom_css/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:36:\"hm_custom_css/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:48:\"hm_custom_javascript/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:58:\"hm_custom_javascript/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:78:\"hm_custom_javascript/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:73:\"hm_custom_javascript/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:73:\"hm_custom_javascript/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:54:\"hm_custom_javascript/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:37:\"hm_custom_javascript/([^/]+)/embed/?$\";s:53:\"index.php?hm_custom_javascript=$matches[1]&embed=true\";s:41:\"hm_custom_javascript/([^/]+)/trackback/?$\";s:47:\"index.php?hm_custom_javascript=$matches[1]&tb=1\";s:49:\"hm_custom_javascript/([^/]+)/page/?([0-9]{1,})/?$\";s:60:\"index.php?hm_custom_javascript=$matches[1]&paged=$matches[2]\";s:56:\"hm_custom_javascript/([^/]+)/comment-page-([0-9]{1,})/?$\";s:60:\"index.php?hm_custom_javascript=$matches[1]&cpage=$matches[2]\";s:46:\"hm_custom_javascript/([^/]+)/wc-api(/(.*))?/?$\";s:61:\"index.php?hm_custom_javascript=$matches[1]&wc-api=$matches[3]\";s:52:\"hm_custom_javascript/[^/]+/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:63:\"hm_custom_javascript/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:45:\"hm_custom_javascript/([^/]+)(?:/([0-9]+))?/?$\";s:59:\"index.php?hm_custom_javascript=$matches[1]&page=$matches[2]\";s:37:\"hm_custom_javascript/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:47:\"hm_custom_javascript/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:67:\"hm_custom_javascript/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:62:\"hm_custom_javascript/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:62:\"hm_custom_javascript/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:43:\"hm_custom_javascript/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:12:\"robots\\.txt$\";s:18:\"index.php?robots=1\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:39:\"index.php?&page_id=14&cpage=$matches[1]\";s:17:\"wc-api(/(.*))?/?$\";s:29:\"index.php?&wc-api=$matches[2]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:26:\"comments/wc-api(/(.*))?/?$\";s:29:\"index.php?&wc-api=$matches[2]\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:29:\"search/(.+)/wc-api(/(.*))?/?$\";s:42:\"index.php?s=$matches[1]&wc-api=$matches[3]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:32:\"author/([^/]+)/wc-api(/(.*))?/?$\";s:52:\"index.php?author_name=$matches[1]&wc-api=$matches[3]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:54:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/wc-api(/(.*))?/?$\";s:82:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&wc-api=$matches[5]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:41:\"([0-9]{4})/([0-9]{1,2})/wc-api(/(.*))?/?$\";s:66:\"index.php?year=$matches[1]&monthnum=$matches[2]&wc-api=$matches[4]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:28:\"([0-9]{4})/wc-api(/(.*))?/?$\";s:45:\"index.php?year=$matches[1]&wc-api=$matches[3]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:25:\"(.?.+?)/wc-api(/(.*))?/?$\";s:49:\"index.php?pagename=$matches[1]&wc-api=$matches[3]\";s:28:\"(.?.+?)/order-pay(/(.*))?/?$\";s:52:\"index.php?pagename=$matches[1]&order-pay=$matches[3]\";s:33:\"(.?.+?)/order-received(/(.*))?/?$\";s:57:\"index.php?pagename=$matches[1]&order-received=$matches[3]\";s:25:\"(.?.+?)/orders(/(.*))?/?$\";s:49:\"index.php?pagename=$matches[1]&orders=$matches[3]\";s:29:\"(.?.+?)/view-order(/(.*))?/?$\";s:53:\"index.php?pagename=$matches[1]&view-order=$matches[3]\";s:28:\"(.?.+?)/downloads(/(.*))?/?$\";s:52:\"index.php?pagename=$matches[1]&downloads=$matches[3]\";s:31:\"(.?.+?)/edit-account(/(.*))?/?$\";s:55:\"index.php?pagename=$matches[1]&edit-account=$matches[3]\";s:31:\"(.?.+?)/edit-address(/(.*))?/?$\";s:55:\"index.php?pagename=$matches[1]&edit-address=$matches[3]\";s:34:\"(.?.+?)/payment-methods(/(.*))?/?$\";s:58:\"index.php?pagename=$matches[1]&payment-methods=$matches[3]\";s:32:\"(.?.+?)/lost-password(/(.*))?/?$\";s:56:\"index.php?pagename=$matches[1]&lost-password=$matches[3]\";s:34:\"(.?.+?)/customer-logout(/(.*))?/?$\";s:58:\"index.php?pagename=$matches[1]&customer-logout=$matches[3]\";s:37:\"(.?.+?)/add-payment-method(/(.*))?/?$\";s:61:\"index.php?pagename=$matches[1]&add-payment-method=$matches[3]\";s:40:\"(.?.+?)/delete-payment-method(/(.*))?/?$\";s:64:\"index.php?pagename=$matches[1]&delete-payment-method=$matches[3]\";s:45:\"(.?.+?)/set-default-payment-method(/(.*))?/?$\";s:69:\"index.php?pagename=$matches[1]&set-default-payment-method=$matches[3]\";s:31:\".?.+?/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:42:\".?.+?/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";s:27:\"[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\"[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\"[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"([^/]+)/embed/?$\";s:37:\"index.php?name=$matches[1]&embed=true\";s:20:\"([^/]+)/trackback/?$\";s:31:\"index.php?name=$matches[1]&tb=1\";s:40:\"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:35:\"([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:28:\"([^/]+)/page/?([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&paged=$matches[2]\";s:35:\"([^/]+)/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&cpage=$matches[2]\";s:25:\"([^/]+)/wc-api(/(.*))?/?$\";s:45:\"index.php?name=$matches[1]&wc-api=$matches[3]\";s:31:\"[^/]+/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:42:\"[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:24:\"([^/]+)(?:/([0-9]+))?/?$\";s:43:\"index.php?name=$matches[1]&page=$matches[2]\";s:16:\"[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:26:\"[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:46:\"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:22:\"[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:9:{i:0;s:55:\"custom-css-and-javascript/custom-css-and-javascript.php\";i:1;s:63:\"login-register-redirect-for-woocommerce/lrr-for-woocommerce.php\";i:2;s:51:\"mailchimp-for-woocommerce/mailchimp-woocommerce.php\";i:3;s:91:\"woocommerce-gateway-paypal-express-checkout/woocommerce-gateway-paypal-express-checkout.php\";i:4;s:57:\"woocommerce-gateway-stripe/woocommerce-gateway-stripe.php\";i:5;s:63:\"woocommerce-my-account-widget/woocommerce-my-account-widget.php\";i:6;s:45:\"woocommerce-services/woocommerce-services.php\";i:7;s:67:\"woocommerce-simple-registration/woocommerce-simple-registration.php\";i:8;s:27:\"woocommerce/woocommerce.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', 'a:3:{i:0;s:111:\"/var/www/vhosts/dev.contursoft.ro/cndeveloper.loc/wp-content/themes/twentyseventeen/functions.php\";i:2;s:107:\"/var/www/vhosts/dev.contursoft.ro/cndeveloper.loc/wp-content/themes/twentyseventeen/style.css\";i:3;s:0:\"\";}', 'no'),
(40, 'template', 'twentyseventeen', 'yes'),
(41, 'stylesheet', 'twentyseventeen', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '38590', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '0', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'page', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:6:{i:2;a:4:{s:5:\"title\";s:7:\"Find Us\";s:4:\"text\";s:168:\"<strong>Address</strong>\n123 Main Street\nNew York, NY 10001\n\n<strong>Hours</strong>\nMonday&mdash;Friday: 9:00AM&ndash;5:00PM\nSaturday &amp; Sunday: 11:00AM&ndash;3:00PM\";s:6:\"filter\";b:1;s:6:\"visual\";b:1;}i:3;a:4:{s:5:\"title\";s:15:\"About This Site\";s:4:\"text\";s:85:\"This may be a good place to introduce yourself and your site or include some credits.\";s:6:\"filter\";b:1;s:6:\"visual\";b:1;}i:4;a:4:{s:5:\"title\";s:7:\"Find Us\";s:4:\"text\";s:168:\"<strong>Address</strong>\n123 Main Street\nNew York, NY 10001\n\n<strong>Hours</strong>\nMonday&mdash;Friday: 9:00AM&ndash;5:00PM\nSaturday &amp; Sunday: 11:00AM&ndash;3:00PM\";s:6:\"filter\";b:1;s:6:\"visual\";b:1;}i:5;a:4:{s:5:\"title\";s:15:\"About This Site\";s:4:\"text\";s:85:\"This may be a good place to introduce yourself and your site or include some credits.\";s:6:\"filter\";b:1;s:6:\"visual\";b:1;}i:6;a:4:{s:5:\"title\";s:0:\"\";s:4:\"text\";s:33:\"[woocommerce_simple_registration]\";s:6:\"filter\";b:1;s:6:\"visual\";b:1;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(80, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(81, 'uninstall_plugins', 'a:1:{s:45:\"woocommerce-services/woocommerce-services.php\";a:2:{i:0;s:17:\"WC_Connect_Loader\";i:1;s:16:\"plugin_uninstall\";}}', 'no'),
(82, 'timezone_string', '', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '14', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'wp_page_for_privacy_policy', '3', 'yes'),
(92, 'show_comments_cookies_opt_in', '0', 'yes'),
(93, 'initial_db_version', '38590', 'yes'),
(94, 'r8ISTpM09o_user_roles', 'a:7:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:114:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:18:\"manage_woocommerce\";b:1;s:24:\"view_woocommerce_reports\";b:1;s:12:\"edit_product\";b:1;s:12:\"read_product\";b:1;s:14:\"delete_product\";b:1;s:13:\"edit_products\";b:1;s:20:\"edit_others_products\";b:1;s:16:\"publish_products\";b:1;s:21:\"read_private_products\";b:1;s:15:\"delete_products\";b:1;s:23:\"delete_private_products\";b:1;s:25:\"delete_published_products\";b:1;s:22:\"delete_others_products\";b:1;s:21:\"edit_private_products\";b:1;s:23:\"edit_published_products\";b:1;s:20:\"manage_product_terms\";b:1;s:18:\"edit_product_terms\";b:1;s:20:\"delete_product_terms\";b:1;s:20:\"assign_product_terms\";b:1;s:15:\"edit_shop_order\";b:1;s:15:\"read_shop_order\";b:1;s:17:\"delete_shop_order\";b:1;s:16:\"edit_shop_orders\";b:1;s:23:\"edit_others_shop_orders\";b:1;s:19:\"publish_shop_orders\";b:1;s:24:\"read_private_shop_orders\";b:1;s:18:\"delete_shop_orders\";b:1;s:26:\"delete_private_shop_orders\";b:1;s:28:\"delete_published_shop_orders\";b:1;s:25:\"delete_others_shop_orders\";b:1;s:24:\"edit_private_shop_orders\";b:1;s:26:\"edit_published_shop_orders\";b:1;s:23:\"manage_shop_order_terms\";b:1;s:21:\"edit_shop_order_terms\";b:1;s:23:\"delete_shop_order_terms\";b:1;s:23:\"assign_shop_order_terms\";b:1;s:16:\"edit_shop_coupon\";b:1;s:16:\"read_shop_coupon\";b:1;s:18:\"delete_shop_coupon\";b:1;s:17:\"edit_shop_coupons\";b:1;s:24:\"edit_others_shop_coupons\";b:1;s:20:\"publish_shop_coupons\";b:1;s:25:\"read_private_shop_coupons\";b:1;s:19:\"delete_shop_coupons\";b:1;s:27:\"delete_private_shop_coupons\";b:1;s:29:\"delete_published_shop_coupons\";b:1;s:26:\"delete_others_shop_coupons\";b:1;s:25:\"edit_private_shop_coupons\";b:1;s:27:\"edit_published_shop_coupons\";b:1;s:24:\"manage_shop_coupon_terms\";b:1;s:22:\"edit_shop_coupon_terms\";b:1;s:24:\"delete_shop_coupon_terms\";b:1;s:24:\"assign_shop_coupon_terms\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}s:8:\"customer\";a:2:{s:4:\"name\";s:8:\"Customer\";s:12:\"capabilities\";a:1:{s:4:\"read\";b:1;}}s:12:\"shop_manager\";a:2:{s:4:\"name\";s:12:\"Shop manager\";s:12:\"capabilities\";a:92:{s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:4:\"read\";b:1;s:18:\"read_private_pages\";b:1;s:18:\"read_private_posts\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_posts\";b:1;s:10:\"edit_pages\";b:1;s:20:\"edit_published_posts\";b:1;s:20:\"edit_published_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"edit_private_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:17:\"edit_others_pages\";b:1;s:13:\"publish_posts\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_posts\";b:1;s:12:\"delete_pages\";b:1;s:20:\"delete_private_pages\";b:1;s:20:\"delete_private_posts\";b:1;s:22:\"delete_published_pages\";b:1;s:22:\"delete_published_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:19:\"delete_others_pages\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:17:\"moderate_comments\";b:1;s:12:\"upload_files\";b:1;s:6:\"export\";b:1;s:6:\"import\";b:1;s:10:\"list_users\";b:1;s:18:\"manage_woocommerce\";b:1;s:24:\"view_woocommerce_reports\";b:1;s:12:\"edit_product\";b:1;s:12:\"read_product\";b:1;s:14:\"delete_product\";b:1;s:13:\"edit_products\";b:1;s:20:\"edit_others_products\";b:1;s:16:\"publish_products\";b:1;s:21:\"read_private_products\";b:1;s:15:\"delete_products\";b:1;s:23:\"delete_private_products\";b:1;s:25:\"delete_published_products\";b:1;s:22:\"delete_others_products\";b:1;s:21:\"edit_private_products\";b:1;s:23:\"edit_published_products\";b:1;s:20:\"manage_product_terms\";b:1;s:18:\"edit_product_terms\";b:1;s:20:\"delete_product_terms\";b:1;s:20:\"assign_product_terms\";b:1;s:15:\"edit_shop_order\";b:1;s:15:\"read_shop_order\";b:1;s:17:\"delete_shop_order\";b:1;s:16:\"edit_shop_orders\";b:1;s:23:\"edit_others_shop_orders\";b:1;s:19:\"publish_shop_orders\";b:1;s:24:\"read_private_shop_orders\";b:1;s:18:\"delete_shop_orders\";b:1;s:26:\"delete_private_shop_orders\";b:1;s:28:\"delete_published_shop_orders\";b:1;s:25:\"delete_others_shop_orders\";b:1;s:24:\"edit_private_shop_orders\";b:1;s:26:\"edit_published_shop_orders\";b:1;s:23:\"manage_shop_order_terms\";b:1;s:21:\"edit_shop_order_terms\";b:1;s:23:\"delete_shop_order_terms\";b:1;s:23:\"assign_shop_order_terms\";b:1;s:16:\"edit_shop_coupon\";b:1;s:16:\"read_shop_coupon\";b:1;s:18:\"delete_shop_coupon\";b:1;s:17:\"edit_shop_coupons\";b:1;s:24:\"edit_others_shop_coupons\";b:1;s:20:\"publish_shop_coupons\";b:1;s:25:\"read_private_shop_coupons\";b:1;s:19:\"delete_shop_coupons\";b:1;s:27:\"delete_private_shop_coupons\";b:1;s:29:\"delete_published_shop_coupons\";b:1;s:26:\"delete_others_shop_coupons\";b:1;s:25:\"edit_private_shop_coupons\";b:1;s:27:\"edit_published_shop_coupons\";b:1;s:24:\"manage_shop_coupon_terms\";b:1;s:22:\"edit_shop_coupon_terms\";b:1;s:24:\"delete_shop_coupon_terms\";b:1;s:24:\"assign_shop_coupon_terms\";b:1;}}}', 'yes'),
(95, 'fresh_site', '0', 'yes'),
(96, 'widget_search', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(97, 'widget_recent-posts', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(98, 'widget_recent-comments', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'widget_archives', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(100, 'widget_meta', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(101, 'sidebars_widgets', 'a:5:{s:19:\"wp_inactive_widgets\";a:4:{i:0;s:6:\"text-2\";i:1;s:6:\"text-3\";i:2;s:6:\"text-4\";i:3;s:6:\"text-5\";}s:9:\"sidebar-1\";a:2:{i:0;s:28:\"woocommercemyaccountwidget-2\";i:1;s:6:\"text-6\";}s:9:\"sidebar-2\";a:0:{}s:9:\"sidebar-3\";a:0:{}s:13:\"array_version\";i:3;}', 'yes'),
(102, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(103, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(104, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(105, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'nonce_key', '4yTPsB-<5Y dgIl<QM/-aeBBxc!>o>J+qywa[:&3_1T4qto%:Rpn>U S~4`a6j`T', 'no'),
(109, 'nonce_salt', 'W!BPdut8,2PL.@:igTi=@7y`JAfQOzuh*-BiArW.J0V;2E{(1RE#$nFA)1US3[$Y', 'no'),
(110, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(112, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(113, 'cron', 'a:13:{i:1534677075;a:1:{s:28:\"wp_1_wc_privacy_cleanup_cron\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:37:\"wp_1_wc_privacy_cleanup_cron_interval\";s:4:\"args\";a:0:{}s:8:\"interval\";i:300;}}}i:1534677229;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1534680672;a:1:{s:32:\"woocommerce_cancel_unpaid_orders\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:2:{s:8:\"schedule\";b:0;s:4:\"args\";a:0:{}}}}i:1534680718;a:1:{s:24:\"woocommerce_cleanup_logs\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1534691518;a:1:{s:28:\"woocommerce_cleanup_sessions\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1534712489;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1534723200;a:1:{s:27:\"woocommerce_scheduled_sales\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1534755728;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1534756318;a:1:{s:33:\"woocommerce_cleanup_personal_data\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1534756328;a:1:{s:30:\"woocommerce_tracker_send_event\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1534760755;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1536019200;a:1:{s:25:\"woocommerce_geoip_updater\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:7:\"monthly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:2635200;}}}s:7:\"version\";i:2;}', 'yes'),
(114, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:1:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:6:\"latest\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-4.9.8.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-4.9.8.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-4.9.8-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-4.9.8-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"4.9.8\";s:7:\"version\";s:5:\"4.9.8\";s:11:\"php_version\";s:5:\"5.2.4\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"4.7\";s:15:\"partial_version\";s:0:\"\";}}s:12:\"last_checked\";i:1534719619;s:15:\"version_checked\";s:5:\"4.9.8\";s:12:\"translations\";a:0:{}}', 'no'),
(118, '_site_transient_update_themes', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1534677074;s:7:\"checked\";a:3:{s:13:\"twentyfifteen\";s:3:\"2.0\";s:15:\"twentyseventeen\";s:3:\"1.7\";s:13:\"twentysixteen\";s:3:\"1.5\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}}', 'no'),
(120, '_site_transient_timeout_browser_01fe6d96f512df15cc1b10345d6b37d9', '1534842129', 'no'),
(121, '_site_transient_browser_01fe6d96f512df15cc1b10345d6b37d9', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:13:\"68.0.3440.106\";s:8:\"platform\";s:7:\"Windows\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(122, 'can_compress_scripts', '1', 'no'),
(135, 'theme_mods_twentyseventeen', 'a:4:{s:18:\"custom_css_post_id\";i:22;s:12:\"header_image\";s:13:\"remove-header\";s:18:\"nav_menu_locations\";a:2:{s:3:\"top\";i:17;s:6:\"social\";i:0;}s:11:\"page_layout\";s:10:\"two-column\";}', 'yes'),
(153, 'woocommerce_store_address', 'Lorem ipsum', 'yes'),
(154, 'woocommerce_store_address_2', '', 'yes'),
(155, 'woocommerce_store_city', 'Dolor', 'yes'),
(156, 'woocommerce_default_country', 'US:CA', 'yes'),
(157, 'woocommerce_store_postcode', '223329', 'yes'),
(158, 'woocommerce_allowed_countries', 'all', 'yes'),
(159, 'woocommerce_all_except_countries', 'a:0:{}', 'yes'),
(160, 'woocommerce_specific_allowed_countries', 'a:0:{}', 'yes'),
(161, 'woocommerce_ship_to_countries', '', 'yes'),
(162, 'woocommerce_specific_ship_to_countries', 'a:0:{}', 'yes'),
(163, 'woocommerce_default_customer_address', 'geolocation', 'yes'),
(164, 'woocommerce_calc_taxes', 'no', 'yes'),
(165, 'woocommerce_enable_coupons', 'no', 'yes'),
(166, 'woocommerce_calc_discounts_sequentially', 'no', 'no'),
(167, 'woocommerce_currency', 'USD', 'yes'),
(168, 'woocommerce_currency_pos', 'left', 'yes'),
(169, 'woocommerce_price_thousand_sep', ',', 'yes'),
(170, 'woocommerce_price_decimal_sep', '.', 'yes'),
(171, 'woocommerce_price_num_decimals', '2', 'yes'),
(172, 'woocommerce_shop_page_id', '14', 'yes'),
(173, 'woocommerce_cart_redirect_after_add', 'yes', 'yes'),
(174, 'woocommerce_enable_ajax_add_to_cart', 'yes', 'yes'),
(175, 'woocommerce_weight_unit', 'oz', 'yes'),
(176, 'woocommerce_dimension_unit', 'in', 'yes'),
(177, 'woocommerce_enable_reviews', 'no', 'yes'),
(178, 'woocommerce_review_rating_verification_label', 'yes', 'no'),
(179, 'woocommerce_review_rating_verification_required', 'no', 'no'),
(180, 'woocommerce_enable_review_rating', 'yes', 'yes'),
(181, 'woocommerce_review_rating_required', 'yes', 'no'),
(182, 'woocommerce_manage_stock', 'yes', 'yes'),
(183, 'woocommerce_hold_stock_minutes', '60', 'no'),
(184, 'woocommerce_notify_low_stock', 'yes', 'no'),
(185, 'woocommerce_notify_no_stock', 'yes', 'no'),
(186, 'woocommerce_stock_email_recipient', 'tudor@contursoft.com', 'no'),
(187, 'woocommerce_notify_low_stock_amount', '2', 'no'),
(188, 'woocommerce_notify_no_stock_amount', '0', 'yes'),
(189, 'woocommerce_hide_out_of_stock_items', 'no', 'yes'),
(190, 'woocommerce_stock_format', '', 'yes'),
(191, 'woocommerce_file_download_method', 'force', 'no'),
(192, 'woocommerce_downloads_require_login', 'no', 'no'),
(193, 'woocommerce_downloads_grant_access_after_payment', 'yes', 'no'),
(194, 'woocommerce_prices_include_tax', 'no', 'yes'),
(195, 'woocommerce_tax_based_on', 'shipping', 'yes'),
(196, 'woocommerce_shipping_tax_class', 'inherit', 'yes'),
(197, 'woocommerce_tax_round_at_subtotal', 'no', 'yes'),
(198, 'woocommerce_tax_classes', 'Reduced rate\nZero rate', 'yes'),
(199, 'woocommerce_tax_display_shop', 'excl', 'yes'),
(200, 'woocommerce_tax_display_cart', 'excl', 'yes'),
(201, 'woocommerce_price_display_suffix', '', 'yes'),
(202, 'woocommerce_tax_total_display', 'itemized', 'no'),
(203, 'woocommerce_enable_shipping_calc', 'yes', 'no'),
(204, 'woocommerce_shipping_cost_requires_address', 'no', 'yes'),
(205, 'woocommerce_ship_to_destination', 'billing', 'no'),
(206, 'woocommerce_shipping_debug_mode', 'no', 'yes'),
(207, 'woocommerce_enable_guest_checkout', 'no', 'no'),
(208, 'woocommerce_enable_checkout_login_reminder', 'no', 'no'),
(209, 'woocommerce_enable_signup_and_login_from_checkout', 'yes', 'no'),
(210, 'woocommerce_enable_myaccount_registration', 'yes', 'no'),
(211, 'woocommerce_registration_generate_username', 'yes', 'no'),
(212, 'woocommerce_registration_generate_password', 'yes', 'no'),
(213, 'woocommerce_erasure_request_removes_order_data', 'no', 'no'),
(214, 'woocommerce_erasure_request_removes_download_data', 'no', 'no'),
(215, 'woocommerce_registration_privacy_policy_text', 'Your personal data will be used to support your experience throughout this website, to manage access to your account, and for other purposes described in our [privacy_policy].', 'yes'),
(216, 'woocommerce_checkout_privacy_policy_text', 'Your personal data will be used to process your order, support your experience throughout this website, and for other purposes described in our [privacy_policy].', 'yes'),
(217, 'woocommerce_delete_inactive_accounts', 'a:2:{s:6:\"number\";s:0:\"\";s:4:\"unit\";s:6:\"months\";}', 'no'),
(218, 'woocommerce_trash_pending_orders', 'a:2:{s:6:\"number\";s:0:\"\";s:4:\"unit\";s:4:\"days\";}', 'no'),
(219, 'woocommerce_trash_failed_orders', 'a:2:{s:6:\"number\";s:0:\"\";s:4:\"unit\";s:4:\"days\";}', 'no'),
(220, 'woocommerce_trash_cancelled_orders', 'a:2:{s:6:\"number\";s:0:\"\";s:4:\"unit\";s:4:\"days\";}', 'no'),
(221, 'woocommerce_anonymize_completed_orders', 'a:2:{s:6:\"number\";s:0:\"\";s:4:\"unit\";s:6:\"months\";}', 'no'),
(222, 'woocommerce_email_from_name', 'CodingNinja Test', 'no'),
(223, 'woocommerce_email_from_address', 'tudor@contursoft.com', 'no'),
(224, 'woocommerce_email_header_image', '', 'no'),
(225, 'woocommerce_email_footer_text', '{site_title}', 'no'),
(226, 'woocommerce_email_base_color', '#96588a', 'no'),
(227, 'woocommerce_email_background_color', '#f7f7f7', 'no'),
(228, 'woocommerce_email_body_background_color', '#ffffff', 'no'),
(229, 'woocommerce_email_text_color', '#3c3c3c', 'no'),
(230, 'woocommerce_cart_page_id', '15', 'yes'),
(231, 'woocommerce_checkout_page_id', '16', 'yes'),
(232, 'woocommerce_myaccount_page_id', '17', 'yes'),
(233, 'woocommerce_terms_page_id', '', 'no'),
(234, 'woocommerce_checkout_pay_endpoint', 'order-pay', 'yes'),
(235, 'woocommerce_checkout_order_received_endpoint', 'order-received', 'yes'),
(236, 'woocommerce_myaccount_add_payment_method_endpoint', 'add-payment-method', 'yes'),
(237, 'woocommerce_myaccount_delete_payment_method_endpoint', 'delete-payment-method', 'yes'),
(238, 'woocommerce_myaccount_set_default_payment_method_endpoint', 'set-default-payment-method', 'yes'),
(239, 'woocommerce_myaccount_orders_endpoint', 'orders', 'yes'),
(240, 'woocommerce_myaccount_view_order_endpoint', 'view-order', 'yes'),
(241, 'woocommerce_myaccount_downloads_endpoint', 'downloads', 'yes'),
(242, 'woocommerce_myaccount_edit_account_endpoint', 'edit-account', 'yes'),
(243, 'woocommerce_myaccount_edit_address_endpoint', 'edit-address', 'yes'),
(244, 'woocommerce_myaccount_payment_methods_endpoint', 'payment-methods', 'yes'),
(245, 'woocommerce_myaccount_lost_password_endpoint', 'lost-password', 'yes'),
(246, 'woocommerce_logout_endpoint', 'customer-logout', 'yes'),
(247, 'woocommerce_api_enabled', 'no', 'yes'),
(248, 'woocommerce_single_image_width', '600', 'yes'),
(249, 'woocommerce_thumbnail_image_width', '300', 'yes'),
(250, 'woocommerce_checkout_highlight_required_fields', 'yes', 'yes'),
(251, 'woocommerce_demo_store', 'no', 'no'),
(252, 'woocommerce_permalinks', 'a:5:{s:12:\"product_base\";s:7:\"product\";s:13:\"category_base\";s:16:\"product-category\";s:8:\"tag_base\";s:11:\"product-tag\";s:14:\"attribute_base\";s:0:\"\";s:22:\"use_verbose_page_rules\";b:0;}', 'yes'),
(253, 'current_theme_supports_woocommerce', 'yes', 'yes'),
(254, 'woocommerce_queue_flush_rewrite_rules', 'no', 'yes'),
(255, '_transient_wc_attribute_taxonomies', 'a:0:{}', 'yes'),
(257, 'default_product_cat', '15', 'yes'),
(260, 'woocommerce_version', '3.4.4', 'yes'),
(261, 'woocommerce_db_version', '3.4.4', 'yes'),
(262, 'recently_activated', 'a:3:{s:19:\"jetpack/jetpack.php\";i:1534250818;s:27:\"woosidebars/woosidebars.php\";i:1534250539;s:70:\"woocommerce-login-and-registration/woocommerce_login_signup_plugin.php\";i:1534247234;}', 'yes'),
(263, 'woocommerce_admin_notices', 'a:0:{}', 'yes'),
(264, '_transient_woocommerce_webhook_ids', 'a:0:{}', 'yes'),
(265, 'widget_woocommerce_widget_cart', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(266, 'widget_woocommerce_layered_nav_filters', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(267, 'widget_woocommerce_layered_nav', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(268, 'widget_woocommerce_price_filter', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(269, 'widget_woocommerce_product_categories', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(270, 'widget_woocommerce_product_search', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(271, 'widget_woocommerce_product_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(272, 'widget_woocommerce_products', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(273, 'widget_woocommerce_recently_viewed_products', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(274, 'widget_woocommerce_top_rated_products', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(275, 'widget_woocommerce_recent_reviews', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(276, 'widget_woocommerce_rating_filter', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(278, 'woocommerce_meta_box_errors', 'a:0:{}', 'yes'),
(279, 'woocommerce_product_type', 'both', 'yes'),
(280, 'woocommerce_allow_tracking', 'yes', 'yes'),
(282, 'woocommerce_stripe_settings', 'a:17:{s:7:\"enabled\";s:2:\"no\";s:14:\"create_account\";b:0;s:5:\"email\";s:20:\"tudor@contursoft.com\";s:5:\"title\";s:20:\"Credit Card (Stripe)\";s:11:\"description\";s:37:\"Pay with your credit card via Stripe.\";s:8:\"testmode\";s:3:\"yes\";s:14:\"inline_cc_form\";s:2:\"no\";s:7:\"capture\";s:3:\"yes\";s:20:\"statement_descriptor\";s:0:\"\";s:14:\"three_d_secure\";s:3:\"yes\";s:15:\"stripe_checkout\";s:2:\"no\";s:21:\"stripe_checkout_image\";s:0:\"\";s:27:\"stripe_checkout_description\";s:0:\"\";s:11:\"saved_cards\";s:2:\"no\";s:15:\"test_secret_key\";s:0:\"\";s:20:\"test_publishable_key\";s:0:\"\";s:15:\"payment_request\";s:3:\"yes\";}', 'yes'),
(284, 'woocommerce_ppec_paypal_settings', 'a:2:{s:16:\"reroute_requests\";b:0;s:5:\"email\";s:20:\"tudor@contursoft.com\";}', 'yes');
INSERT INTO `r8ISTpM09o_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(285, 'woocommerce_cheque_settings', 'a:1:{s:7:\"enabled\";s:2:\"no\";}', 'yes'),
(286, 'woocommerce_bacs_settings', 'a:10:{s:7:\"enabled\";s:3:\"yes\";s:5:\"title\";s:20:\"Direct bank transfer\";s:11:\"description\";s:176:\"Make your payment directly into our bank account. Please use your Order ID as the payment reference. Your order will not be shipped until the funds have cleared in our account.\";s:12:\"instructions\";s:0:\"\";s:12:\"account_name\";s:0:\"\";s:14:\"account_number\";s:0:\"\";s:9:\"sort_code\";s:0:\"\";s:9:\"bank_name\";s:0:\"\";s:4:\"iban\";s:0:\"\";s:3:\"bic\";s:0:\"\";}', 'yes'),
(287, 'woocommerce_cod_settings', 'a:1:{s:7:\"enabled\";s:2:\"no\";}', 'yes'),
(288, 'wc_ppec_version', '1.6.1', 'yes'),
(296, 'wc_stripe_version', '4.1.8', 'yes'),
(299, 'woocommerce_setup_domestic_live_rates_zone', '1', 'no'),
(300, '_transient_shipping-transient-version', '1534240640', 'yes'),
(301, 'woocommerce_setup_intl_live_rates_zone', '1', 'no'),
(304, 'jetpack_activated', '1', 'yes'),
(307, 'jetpack_activation_source', 'a:2:{i:0;s:7:\"unknown\";i:1;N;}', 'yes'),
(309, '_transient_timeout_jetpack_file_data_6.4.2', '1536746246', 'no'),
(310, '_transient_jetpack_file_data_6.4.2', 'a:58:{s:32:\"c22c48d7cfe9d38dff2864cfea64636a\";a:15:{s:4:\"name\";s:20:\"Spelling and Grammar\";s:11:\"description\";s:39:\"Check your spelling, style, and grammar\";s:14:\"jumpstart_desc\";s:0:\"\";s:4:\"sort\";s:1:\"6\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:3:\"1.1\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:3:\"Yes\";s:11:\"module_tags\";s:7:\"Writing\";s:7:\"feature\";s:7:\"Writing\";s:25:\"additional_search_queries\";s:115:\"after the deadline, afterthedeadline, spell, spellchecker, spelling, grammar, proofreading, style, language, cliche\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"fb5c4814ddc3946a3f22cc838fcb2af3\";a:15:{s:4:\"name\";s:8:\"Carousel\";s:11:\"description\";s:75:\"Display images and galleries in a gorgeous, full-screen browsing experience\";s:14:\"jumpstart_desc\";s:79:\"Brings your photos and images to life as full-size, easily navigable galleries.\";s:4:\"sort\";s:2:\"22\";s:20:\"recommendation_order\";s:2:\"12\";s:10:\"introduced\";s:3:\"1.5\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:2:\"No\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:17:\"Photos and Videos\";s:7:\"feature\";s:21:\"Appearance, Jumpstart\";s:25:\"additional_search_queries\";s:80:\"gallery, carousel, diaporama, slideshow, images, lightbox, exif, metadata, image\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"5813eda53235a9a81a69b1f6a4a15db6\";a:15:{s:4:\"name\";s:13:\"Comment Likes\";s:11:\"description\";s:64:\"Increase visitor engagement by adding a Like button to comments.\";s:14:\"jumpstart_desc\";s:0:\"\";s:4:\"sort\";s:2:\"39\";s:20:\"recommendation_order\";s:2:\"17\";s:10:\"introduced\";s:3:\"5.1\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:6:\"Social\";s:7:\"feature\";s:0:\"\";s:25:\"additional_search_queries\";s:37:\"like widget, like button, like, likes\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"7ef4ca32a1c84fc10ef50c8293cae5df\";a:15:{s:4:\"name\";s:8:\"Comments\";s:11:\"description\";s:80:\"Let readers use WordPress.com, Twitter, Facebook, or Google+ accounts to comment\";s:14:\"jumpstart_desc\";s:0:\"\";s:4:\"sort\";s:2:\"20\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:3:\"1.4\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:6:\"Social\";s:7:\"feature\";s:10:\"Engagement\";s:25:\"additional_search_queries\";s:53:\"comments, comment, facebook, twitter, google+, social\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"c5331bfc2648dfeeebe486736d79a72c\";a:15:{s:4:\"name\";s:12:\"Contact Form\";s:11:\"description\";s:57:\"Insert a customizable contact form anywhere on your site.\";s:14:\"jumpstart_desc\";s:111:\"Adds a button to your post and page editors, allowing you to build simple forms to help visitors stay in touch.\";s:4:\"sort\";s:2:\"15\";s:20:\"recommendation_order\";s:2:\"14\";s:10:\"introduced\";s:3:\"1.3\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:2:\"No\";s:13:\"auto_activate\";s:3:\"Yes\";s:11:\"module_tags\";s:5:\"Other\";s:7:\"feature\";s:18:\"Writing, Jumpstart\";s:25:\"additional_search_queries\";s:44:\"contact, form, grunion, feedback, submission\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"707c77d2e8cb0c12d094e5423c8beda8\";a:15:{s:4:\"name\";s:20:\"Custom content types\";s:11:\"description\";s:74:\"Display different types of content on your site with custom content types.\";s:14:\"jumpstart_desc\";s:0:\"\";s:4:\"sort\";s:2:\"34\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:3:\"3.1\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:2:\"No\";s:13:\"auto_activate\";s:3:\"Yes\";s:11:\"module_tags\";s:7:\"Writing\";s:7:\"feature\";s:7:\"Writing\";s:25:\"additional_search_queries\";s:72:\"cpt, custom post types, portfolio, portfolios, testimonial, testimonials\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"cd499b1678cfe3aabfc8ca0d3eb7e8b9\";a:15:{s:4:\"name\";s:10:\"Custom CSS\";s:11:\"description\";s:53:\"Tweak your site’s CSS without modifying your theme.\";s:14:\"jumpstart_desc\";s:0:\"\";s:4:\"sort\";s:1:\"2\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:3:\"1.7\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:2:\"No\";s:13:\"auto_activate\";s:3:\"Yes\";s:11:\"module_tags\";s:10:\"Appearance\";s:7:\"feature\";s:10:\"Appearance\";s:25:\"additional_search_queries\";s:108:\"css, customize, custom, style, editor, less, sass, preprocessor, font, mobile, appearance, theme, stylesheet\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"7d266d6546645f42cf52a66387699c50\";a:15:{s:4:\"name\";s:0:\"\";s:11:\"description\";s:0:\"\";s:14:\"jumpstart_desc\";s:0:\"\";s:4:\"sort\";s:0:\"\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:0:\"\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:0:\"\";s:13:\"auto_activate\";s:0:\"\";s:11:\"module_tags\";s:0:\"\";s:7:\"feature\";s:0:\"\";s:25:\"additional_search_queries\";s:0:\"\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"5d436678d5e010ac6b0f157aa1021554\";a:15:{s:4:\"name\";s:21:\"Enhanced Distribution\";s:11:\"description\";s:27:\"Increase reach and traffic.\";s:14:\"jumpstart_desc\";s:0:\"\";s:4:\"sort\";s:1:\"5\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:3:\"1.2\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:6:\"Public\";s:11:\"module_tags\";s:7:\"Writing\";s:7:\"feature\";s:10:\"Engagement\";s:25:\"additional_search_queries\";s:54:\"google, seo, firehose, search, broadcast, broadcasting\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"2c9ff765b826940496a65c0f927a594a\";a:15:{s:4:\"name\";s:0:\"\";s:11:\"description\";s:0:\"\";s:14:\"jumpstart_desc\";s:0:\"\";s:4:\"sort\";s:0:\"\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:0:\"\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:0:\"\";s:13:\"auto_activate\";s:0:\"\";s:11:\"module_tags\";s:0:\"\";s:7:\"feature\";s:0:\"\";s:25:\"additional_search_queries\";s:0:\"\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"092b94702bb483a5472578283c2103d6\";a:15:{s:4:\"name\";s:16:\"Google Analytics\";s:11:\"description\";s:56:\"Set up Google Analytics without touching a line of code.\";s:14:\"jumpstart_desc\";s:0:\"\";s:4:\"sort\";s:2:\"37\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:3:\"4.5\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:0:\"\";s:7:\"feature\";s:10:\"Engagement\";s:25:\"additional_search_queries\";s:37:\"webmaster, google, analytics, console\";s:12:\"plan_classes\";s:17:\"business, premium\";}s:32:\"6bd77e09440df2b63044cf8cb7963773\";a:15:{s:4:\"name\";s:0:\"\";s:11:\"description\";s:0:\"\";s:14:\"jumpstart_desc\";s:0:\"\";s:4:\"sort\";s:0:\"\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:0:\"\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:0:\"\";s:13:\"auto_activate\";s:0:\"\";s:11:\"module_tags\";s:0:\"\";s:7:\"feature\";s:0:\"\";s:25:\"additional_search_queries\";s:0:\"\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"ee1a10e2ef5733ab19eb1eb552d5ecb3\";a:15:{s:4:\"name\";s:19:\"Gravatar Hovercards\";s:11:\"description\";s:58:\"Enable pop-up business cards over commenters’ Gravatars.\";s:14:\"jumpstart_desc\";s:131:\"Let commenters link their profiles to their Gravatar accounts, making it easy for your visitors to learn more about your community.\";s:4:\"sort\";s:2:\"11\";s:20:\"recommendation_order\";s:2:\"13\";s:10:\"introduced\";s:3:\"1.1\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:2:\"No\";s:13:\"auto_activate\";s:3:\"Yes\";s:11:\"module_tags\";s:18:\"Social, Appearance\";s:7:\"feature\";s:21:\"Appearance, Jumpstart\";s:25:\"additional_search_queries\";s:20:\"gravatar, hovercards\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"284c08538b0bdc266315b2cf80b9c044\";a:15:{s:4:\"name\";s:0:\"\";s:11:\"description\";s:0:\"\";s:14:\"jumpstart_desc\";s:0:\"\";s:4:\"sort\";s:0:\"\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:0:\"\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:0:\"\";s:13:\"auto_activate\";s:0:\"\";s:11:\"module_tags\";s:0:\"\";s:7:\"feature\";s:0:\"\";s:25:\"additional_search_queries\";s:0:\"\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"0ce5c3ac630dea9f41215e48bb0f52f3\";a:15:{s:4:\"name\";s:15:\"Infinite Scroll\";s:11:\"description\";s:53:\"Automatically load new content when a visitor scrolls\";s:14:\"jumpstart_desc\";s:0:\"\";s:4:\"sort\";s:2:\"26\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:3:\"2.0\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:2:\"No\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:10:\"Appearance\";s:7:\"feature\";s:10:\"Appearance\";s:25:\"additional_search_queries\";s:33:\"scroll, infinite, infinite scroll\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"87da2858d4f9cadb6a44fdcf32e8d2b5\";a:15:{s:4:\"name\";s:8:\"JSON API\";s:11:\"description\";s:51:\"Allow applications to securely access your content.\";s:14:\"jumpstart_desc\";s:0:\"\";s:4:\"sort\";s:2:\"19\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:3:\"1.9\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:6:\"Public\";s:11:\"module_tags\";s:19:\"Writing, Developers\";s:7:\"feature\";s:7:\"General\";s:25:\"additional_search_queries\";s:50:\"api, rest, develop, developers, json, klout, oauth\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"004962cb7cb9ec2b64769ac4df509217\";a:15:{s:4:\"name\";s:14:\"Beautiful Math\";s:11:\"description\";s:57:\"Use LaTeX markup for complex equations and other geekery.\";s:14:\"jumpstart_desc\";s:0:\"\";s:4:\"sort\";s:2:\"12\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:3:\"1.1\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:2:\"No\";s:13:\"auto_activate\";s:3:\"Yes\";s:11:\"module_tags\";s:7:\"Writing\";s:7:\"feature\";s:7:\"Writing\";s:25:\"additional_search_queries\";s:47:\"latex, math, equation, equations, formula, code\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"7f408184bee8850d439c01322867e72c\";a:15:{s:4:\"name\";s:11:\"Lazy Images\";s:11:\"description\";s:16:\"Lazy load images\";s:14:\"jumpstart_desc\";s:164:\"Lazy-loading images improve your site\'s speed and create a smoother viewing experience. Images will load as visitors scroll down the screen, instead of all at once.\";s:4:\"sort\";s:2:\"24\";s:20:\"recommendation_order\";s:2:\"14\";s:10:\"introduced\";s:5:\"5.6.0\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:2:\"No\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:23:\"Appearance, Recommended\";s:7:\"feature\";s:21:\"Appearance, Jumpstart\";s:25:\"additional_search_queries\";s:33:\"mobile, theme, performance, image\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"2ad914b747f382ae918ed3b37077d4a1\";a:15:{s:4:\"name\";s:5:\"Likes\";s:11:\"description\";s:63:\"Give visitors an easy way to show they appreciate your content.\";s:14:\"jumpstart_desc\";s:0:\"\";s:4:\"sort\";s:2:\"23\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:3:\"2.2\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:6:\"Social\";s:7:\"feature\";s:10:\"Engagement\";s:25:\"additional_search_queries\";s:26:\"like, likes, wordpress.com\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"b347263e3470979442ebf0514e41e893\";a:15:{s:4:\"name\";s:6:\"Manage\";s:11:\"description\";s:54:\"Manage all of your sites from a centralized dashboard.\";s:14:\"jumpstart_desc\";s:151:\"Helps you remotely manage plugins, turn on automated updates, and more from <a href=\"https://wordpress.com/plugins/\" target=\"_blank\">wordpress.com</a>.\";s:4:\"sort\";s:1:\"1\";s:20:\"recommendation_order\";s:1:\"3\";s:10:\"introduced\";s:3:\"3.4\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:3:\"Yes\";s:11:\"module_tags\";s:35:\"Centralized Management, Recommended\";s:7:\"feature\";s:7:\"General\";s:25:\"additional_search_queries\";s:26:\"manage, management, remote\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"589982245aa6f495b72ab7cf57a1a48e\";a:15:{s:4:\"name\";s:8:\"Markdown\";s:11:\"description\";s:50:\"Write posts or pages in plain-text Markdown syntax\";s:14:\"jumpstart_desc\";s:0:\"\";s:4:\"sort\";s:2:\"31\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:3:\"2.8\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:2:\"No\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:7:\"Writing\";s:7:\"feature\";s:7:\"Writing\";s:25:\"additional_search_queries\";s:12:\"md, markdown\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"d3bec8e063d637bc285018241b783725\";a:15:{s:4:\"name\";s:21:\"WordPress.com Toolbar\";s:11:\"description\";s:91:\"Replaces the admin bar with a useful toolbar to quickly manage your site via WordPress.com.\";s:14:\"jumpstart_desc\";s:0:\"\";s:4:\"sort\";s:2:\"38\";s:20:\"recommendation_order\";s:2:\"16\";s:10:\"introduced\";s:3:\"4.8\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:7:\"General\";s:7:\"feature\";s:0:\"\";s:25:\"additional_search_queries\";s:19:\"adminbar, masterbar\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"6ab1c3e749bcfba2dedbaebe6c9fc614\";a:15:{s:4:\"name\";s:12:\"Mobile Theme\";s:11:\"description\";s:31:\"Enable the Jetpack Mobile theme\";s:14:\"jumpstart_desc\";s:0:\"\";s:4:\"sort\";s:2:\"21\";s:20:\"recommendation_order\";s:2:\"11\";s:10:\"introduced\";s:3:\"1.8\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:2:\"No\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:31:\"Appearance, Mobile, Recommended\";s:7:\"feature\";s:10:\"Appearance\";s:25:\"additional_search_queries\";s:24:\"mobile, theme, minileven\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"b7be7da643ec641511839ecc6afb6def\";a:15:{s:4:\"name\";s:0:\"\";s:11:\"description\";s:0:\"\";s:14:\"jumpstart_desc\";s:0:\"\";s:4:\"sort\";s:0:\"\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:0:\"\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:0:\"\";s:13:\"auto_activate\";s:0:\"\";s:11:\"module_tags\";s:0:\"\";s:7:\"feature\";s:0:\"\";s:25:\"additional_search_queries\";s:0:\"\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"d54f83ff429a8a37ace796de98459411\";a:15:{s:4:\"name\";s:0:\"\";s:11:\"description\";s:0:\"\";s:14:\"jumpstart_desc\";s:0:\"\";s:4:\"sort\";s:0:\"\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:0:\"\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:0:\"\";s:13:\"auto_activate\";s:0:\"\";s:11:\"module_tags\";s:0:\"\";s:7:\"feature\";s:0:\"\";s:25:\"additional_search_queries\";s:0:\"\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"0f8b373fa12c825162c0b0bc20b8bbdd\";a:15:{s:4:\"name\";s:0:\"\";s:11:\"description\";s:0:\"\";s:14:\"jumpstart_desc\";s:0:\"\";s:4:\"sort\";s:0:\"\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:0:\"\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:0:\"\";s:13:\"auto_activate\";s:0:\"\";s:11:\"module_tags\";s:0:\"\";s:7:\"feature\";s:0:\"\";s:25:\"additional_search_queries\";s:0:\"\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"5d7b0750cb34a4a72a44fa67790de639\";a:15:{s:4:\"name\";s:0:\"\";s:11:\"description\";s:0:\"\";s:14:\"jumpstart_desc\";s:0:\"\";s:4:\"sort\";s:0:\"\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:0:\"\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:0:\"\";s:13:\"auto_activate\";s:0:\"\";s:11:\"module_tags\";s:0:\"\";s:7:\"feature\";s:0:\"\";s:25:\"additional_search_queries\";s:0:\"\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"f07fde8db279ffb0116c727df72c6374\";a:15:{s:4:\"name\";s:7:\"Monitor\";s:11:\"description\";s:61:\"Receive immediate notifications if your site goes down, 24/7.\";s:14:\"jumpstart_desc\";s:61:\"Receive immediate notifications if your site goes down, 24/7.\";s:4:\"sort\";s:2:\"28\";s:20:\"recommendation_order\";s:2:\"10\";s:10:\"introduced\";s:3:\"2.6\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:11:\"Recommended\";s:7:\"feature\";s:19:\"Security, Jumpstart\";s:25:\"additional_search_queries\";s:37:\"monitor, uptime, downtime, monitoring\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"136a5445a49150db75472862f3d3aefb\";a:15:{s:4:\"name\";s:13:\"Notifications\";s:11:\"description\";s:57:\"Receive instant notifications of site comments and likes.\";s:14:\"jumpstart_desc\";s:0:\"\";s:4:\"sort\";s:2:\"13\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:3:\"1.9\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:3:\"Yes\";s:11:\"module_tags\";s:5:\"Other\";s:7:\"feature\";s:7:\"General\";s:25:\"additional_search_queries\";s:62:\"notification, notifications, toolbar, adminbar, push, comments\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"395d8ae651afabb54d1e98440674b384\";a:15:{s:4:\"name\";s:0:\"\";s:11:\"description\";s:0:\"\";s:14:\"jumpstart_desc\";s:0:\"\";s:4:\"sort\";s:0:\"\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:0:\"\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:0:\"\";s:13:\"auto_activate\";s:0:\"\";s:11:\"module_tags\";s:0:\"\";s:7:\"feature\";s:0:\"\";s:25:\"additional_search_queries\";s:0:\"\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"4484ac68583fbbaab0ef698cdc986950\";a:15:{s:4:\"name\";s:6:\"Photon\";s:11:\"description\";s:29:\"Serve images from our servers\";s:14:\"jumpstart_desc\";s:141:\"Mirrors and serves your images from our free and fast image CDN, improving your site’s performance with no additional load on your servers.\";s:4:\"sort\";s:2:\"25\";s:20:\"recommendation_order\";s:1:\"1\";s:10:\"introduced\";s:3:\"2.0\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:42:\"Photos and Videos, Appearance, Recommended\";s:7:\"feature\";s:34:\"Recommended, Jumpstart, Appearance\";s:25:\"additional_search_queries\";s:38:\"photon, image, cdn, performance, speed\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"6f30193afa5b1360e3fa2676501b5e3a\";a:15:{s:4:\"name\";s:13:\"Post by email\";s:11:\"description\";s:33:\"Publish posts by sending an email\";s:14:\"jumpstart_desc\";s:0:\"\";s:4:\"sort\";s:2:\"14\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:3:\"2.0\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:3:\"Yes\";s:11:\"module_tags\";s:7:\"Writing\";s:7:\"feature\";s:7:\"Writing\";s:25:\"additional_search_queries\";s:20:\"post by email, email\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"3e9f8bd3755d92e8e5d06966a957beb8\";a:15:{s:4:\"name\";s:7:\"Protect\";s:11:\"description\";s:41:\"Block suspicious-looking sign in activity\";s:14:\"jumpstart_desc\";s:0:\"\";s:4:\"sort\";s:1:\"1\";s:20:\"recommendation_order\";s:1:\"4\";s:10:\"introduced\";s:3:\"3.4\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:3:\"Yes\";s:11:\"module_tags\";s:11:\"Recommended\";s:7:\"feature\";s:8:\"Security\";s:25:\"additional_search_queries\";s:65:\"security, secure, protection, botnet, brute force, protect, login\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"0cacc8ab2145ad11cb54d181a98aa550\";a:15:{s:4:\"name\";s:9:\"Publicize\";s:11:\"description\";s:27:\"Automated social marketing.\";s:14:\"jumpstart_desc\";s:0:\"\";s:4:\"sort\";s:2:\"10\";s:20:\"recommendation_order\";s:1:\"7\";s:10:\"introduced\";s:3:\"2.0\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:3:\"Yes\";s:11:\"module_tags\";s:19:\"Social, Recommended\";s:7:\"feature\";s:10:\"Engagement\";s:25:\"additional_search_queries\";s:107:\"facebook, twitter, google+, googleplus, google, path, tumblr, linkedin, social, tweet, connections, sharing\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"a528c2f803a92c5c2effa67cd33ab33a\";a:15:{s:4:\"name\";s:20:\"Progressive Web Apps\";s:11:\"description\";s:85:\"Speed up and improve the reliability of your site using the latest in web technology.\";s:14:\"jumpstart_desc\";s:0:\"\";s:4:\"sort\";s:2:\"38\";s:20:\"recommendation_order\";s:2:\"18\";s:10:\"introduced\";s:5:\"5.6.0\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:10:\"Developers\";s:7:\"feature\";s:7:\"Traffic\";s:25:\"additional_search_queries\";s:26:\"manifest, pwa, progressive\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"329b8efce059081d46936ece0c6736b3\";a:15:{s:4:\"name\";s:0:\"\";s:11:\"description\";s:0:\"\";s:14:\"jumpstart_desc\";s:0:\"\";s:4:\"sort\";s:0:\"\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:0:\"\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:0:\"\";s:13:\"auto_activate\";s:0:\"\";s:11:\"module_tags\";s:0:\"\";s:7:\"feature\";s:0:\"\";s:25:\"additional_search_queries\";s:0:\"\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"5fdd42d482712fbdaf000b28ea7adce9\";a:15:{s:4:\"name\";s:13:\"Related posts\";s:11:\"description\";s:64:\"Increase page views by showing related content to your visitors.\";s:14:\"jumpstart_desc\";s:113:\"Keep visitors engaged on your blog by highlighting relevant and new content at the bottom of each published post.\";s:4:\"sort\";s:2:\"29\";s:20:\"recommendation_order\";s:1:\"9\";s:10:\"introduced\";s:3:\"2.9\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:11:\"Recommended\";s:7:\"feature\";s:21:\"Engagement, Jumpstart\";s:25:\"additional_search_queries\";s:22:\"related, related posts\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"2c5096ef610018e98a8bcccfbea4471e\";a:15:{s:4:\"name\";s:6:\"Search\";s:11:\"description\";s:41:\"Enhanced search, powered by Elasticsearch\";s:14:\"jumpstart_desc\";s:0:\"\";s:4:\"sort\";s:2:\"34\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:3:\"5.0\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:5:\"false\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:0:\"\";s:7:\"feature\";s:6:\"Search\";s:25:\"additional_search_queries\";s:6:\"search\";s:12:\"plan_classes\";s:8:\"business\";}s:32:\"0d81dd7df3ad2f245e84fd4fb66bf829\";a:15:{s:4:\"name\";s:9:\"SEO Tools\";s:11:\"description\";s:50:\"Better results on search engines and social media.\";s:14:\"jumpstart_desc\";s:0:\"\";s:4:\"sort\";s:2:\"35\";s:20:\"recommendation_order\";s:2:\"15\";s:10:\"introduced\";s:3:\"4.4\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:18:\"Social, Appearance\";s:7:\"feature\";s:7:\"Traffic\";s:25:\"additional_search_queries\";s:81:\"search engine optimization, social preview, meta description, custom title format\";s:12:\"plan_classes\";s:17:\"business, premium\";}s:32:\"32aaa676b3b6c9f3ef22430e1e0bca24\";a:15:{s:4:\"name\";s:7:\"Sharing\";s:11:\"description\";s:37:\"Allow visitors to share your content.\";s:14:\"jumpstart_desc\";s:116:\"Twitter, Facebook and Google+ buttons at the bottom of each post, making it easy for visitors to share your content.\";s:4:\"sort\";s:1:\"7\";s:20:\"recommendation_order\";s:1:\"6\";s:10:\"introduced\";s:3:\"1.1\";s:7:\"changed\";s:3:\"1.2\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:2:\"No\";s:13:\"auto_activate\";s:3:\"Yes\";s:11:\"module_tags\";s:19:\"Social, Recommended\";s:7:\"feature\";s:21:\"Engagement, Jumpstart\";s:25:\"additional_search_queries\";s:141:\"share, sharing, sharedaddy, buttons, icons, email, facebook, twitter, google+, linkedin, pinterest, pocket, press this, print, reddit, tumblr\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"948472b453cda59b38bb7c37af889af0\";a:15:{s:4:\"name\";s:16:\"Shortcode Embeds\";s:11:\"description\";s:50:\"Embed media from popular sites without any coding.\";s:14:\"jumpstart_desc\";s:0:\"\";s:4:\"sort\";s:1:\"3\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:3:\"1.1\";s:7:\"changed\";s:3:\"1.2\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:2:\"No\";s:13:\"auto_activate\";s:3:\"Yes\";s:11:\"module_tags\";s:46:\"Photos and Videos, Social, Writing, Appearance\";s:7:\"feature\";s:7:\"Writing\";s:25:\"additional_search_queries\";s:236:\"shortcodes, shortcode, embeds, media, bandcamp, dailymotion, facebook, flickr, google calendars, google maps, google+, polldaddy, recipe, recipes, scribd, slideshare, slideshow, slideshows, soundcloud, ted, twitter, vimeo, vine, youtube\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"7d00a6ca0a79fbe893275aaf6ed6ae42\";a:15:{s:4:\"name\";s:16:\"WP.me Shortlinks\";s:11:\"description\";s:54:\"Create short and simple links for all posts and pages.\";s:14:\"jumpstart_desc\";s:0:\"\";s:4:\"sort\";s:1:\"8\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:3:\"1.1\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:3:\"Yes\";s:11:\"module_tags\";s:6:\"Social\";s:7:\"feature\";s:7:\"Writing\";s:25:\"additional_search_queries\";s:17:\"shortlinks, wp.me\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"372e711395f23c466e04d4fd07f73099\";a:15:{s:4:\"name\";s:0:\"\";s:11:\"description\";s:0:\"\";s:14:\"jumpstart_desc\";s:0:\"\";s:4:\"sort\";s:0:\"\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:0:\"\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:0:\"\";s:13:\"auto_activate\";s:0:\"\";s:11:\"module_tags\";s:0:\"\";s:7:\"feature\";s:0:\"\";s:25:\"additional_search_queries\";s:0:\"\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"2ea687cec293289a2a3e5f0459e79768\";a:15:{s:4:\"name\";s:8:\"Sitemaps\";s:11:\"description\";s:50:\"Make it easy for search engines to find your site.\";s:14:\"jumpstart_desc\";s:0:\"\";s:4:\"sort\";s:2:\"13\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:3:\"3.9\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:2:\"No\";s:13:\"auto_activate\";s:6:\"Public\";s:11:\"module_tags\";s:20:\"Recommended, Traffic\";s:7:\"feature\";s:0:\"\";s:25:\"additional_search_queries\";s:39:\"sitemap, traffic, search, site map, seo\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"2fe9dc2c7389d4f0825a0b23bc8b19d1\";a:15:{s:4:\"name\";s:0:\"\";s:11:\"description\";s:0:\"\";s:14:\"jumpstart_desc\";s:0:\"\";s:4:\"sort\";s:0:\"\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:0:\"\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:0:\"\";s:13:\"auto_activate\";s:0:\"\";s:11:\"module_tags\";s:0:\"\";s:7:\"feature\";s:0:\"\";s:25:\"additional_search_queries\";s:0:\"\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"e7cf8a7e0f151ccf7cbdc6d8f118f316\";a:15:{s:4:\"name\";s:14:\"Single Sign On\";s:11:\"description\";s:62:\"Allow users to log into this site using WordPress.com accounts\";s:14:\"jumpstart_desc\";s:98:\"Lets you log in to all your Jetpack-enabled sites with one click using your WordPress.com account.\";s:4:\"sort\";s:2:\"30\";s:20:\"recommendation_order\";s:1:\"5\";s:10:\"introduced\";s:3:\"2.6\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:10:\"Developers\";s:7:\"feature\";s:19:\"Security, Jumpstart\";s:25:\"additional_search_queries\";s:34:\"sso, single sign on, login, log in\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"34fb073ed896af853ed48bd5739240cb\";a:15:{s:4:\"name\";s:10:\"Site Stats\";s:11:\"description\";s:44:\"Collect valuable traffic stats and insights.\";s:14:\"jumpstart_desc\";s:0:\"\";s:4:\"sort\";s:1:\"1\";s:20:\"recommendation_order\";s:1:\"2\";s:10:\"introduced\";s:3:\"1.1\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:3:\"Yes\";s:11:\"module_tags\";s:23:\"Site Stats, Recommended\";s:7:\"feature\";s:10:\"Engagement\";s:25:\"additional_search_queries\";s:54:\"statistics, tracking, analytics, views, traffic, stats\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"8de0dfff24a17cf0fa0011dfc691a3f3\";a:15:{s:4:\"name\";s:13:\"Subscriptions\";s:11:\"description\";s:87:\"Allow users to subscribe to your posts and comments and receive notifications via email\";s:14:\"jumpstart_desc\";s:126:\"Give visitors two easy subscription options — while commenting, or via a separate email subscription widget you can display.\";s:4:\"sort\";s:1:\"9\";s:20:\"recommendation_order\";s:1:\"8\";s:10:\"introduced\";s:3:\"1.2\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:3:\"Yes\";s:11:\"module_tags\";s:6:\"Social\";s:7:\"feature\";s:21:\"Engagement, Jumpstart\";s:25:\"additional_search_queries\";s:74:\"subscriptions, subscription, email, follow, followers, subscribers, signup\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"4744f348db095538d3edcacb0ed99c89\";a:15:{s:4:\"name\";s:0:\"\";s:11:\"description\";s:0:\"\";s:14:\"jumpstart_desc\";s:0:\"\";s:4:\"sort\";s:0:\"\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:0:\"\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:0:\"\";s:13:\"auto_activate\";s:0:\"\";s:11:\"module_tags\";s:0:\"\";s:7:\"feature\";s:0:\"\";s:25:\"additional_search_queries\";s:0:\"\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"d89db0d934b39f86065ff58e73594070\";a:15:{s:4:\"name\";s:15:\"Tiled Galleries\";s:11:\"description\";s:61:\"Display image galleries in a variety of elegant arrangements.\";s:14:\"jumpstart_desc\";s:61:\"Display image galleries in a variety of elegant arrangements.\";s:4:\"sort\";s:2:\"24\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:3:\"2.1\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:2:\"No\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:17:\"Photos and Videos\";s:7:\"feature\";s:21:\"Appearance, Jumpstart\";s:25:\"additional_search_queries\";s:43:\"gallery, tiles, tiled, grid, mosaic, images\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"01987a7ba5e19786f2992501add8181a\";a:15:{s:4:\"name\";s:0:\"\";s:11:\"description\";s:0:\"\";s:14:\"jumpstart_desc\";s:0:\"\";s:4:\"sort\";s:0:\"\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:0:\"\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:0:\"\";s:13:\"auto_activate\";s:0:\"\";s:11:\"module_tags\";s:0:\"\";s:7:\"feature\";s:0:\"\";s:25:\"additional_search_queries\";s:0:\"\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"20459cc462babfc5a82adf6b34f6e8b1\";a:15:{s:4:\"name\";s:12:\"Data Backups\";s:11:\"description\";s:54:\"Off-site backups, security scans, and automatic fixes.\";s:14:\"jumpstart_desc\";s:0:\"\";s:4:\"sort\";s:2:\"32\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:5:\"0:1.2\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:5:\"false\";s:4:\"free\";s:5:\"false\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:3:\"Yes\";s:11:\"module_tags\";s:0:\"\";s:7:\"feature\";s:16:\"Security, Health\";s:25:\"additional_search_queries\";s:28:\"vaultpress, backup, security\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"836245eb0a8f0c5272542305a88940c1\";a:15:{s:4:\"name\";s:17:\"Site verification\";s:11:\"description\";s:58:\"Establish your site\'s authenticity with external services.\";s:14:\"jumpstart_desc\";s:0:\"\";s:4:\"sort\";s:2:\"33\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:3:\"3.0\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:2:\"No\";s:13:\"auto_activate\";s:3:\"Yes\";s:11:\"module_tags\";s:0:\"\";s:7:\"feature\";s:10:\"Engagement\";s:25:\"additional_search_queries\";s:56:\"webmaster, seo, google, bing, pinterest, search, console\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"e94397a5c47c1be995eff613e65a674f\";a:15:{s:4:\"name\";s:10:\"VideoPress\";s:11:\"description\";s:27:\"Fast, ad-free video hosting\";s:14:\"jumpstart_desc\";s:0:\"\";s:4:\"sort\";s:2:\"27\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:3:\"2.5\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:5:\"false\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:0:\"\";s:11:\"module_tags\";s:17:\"Photos and Videos\";s:7:\"feature\";s:7:\"Writing\";s:25:\"additional_search_queries\";s:25:\"video, videos, videopress\";s:12:\"plan_classes\";s:17:\"business, premium\";}s:32:\"032cd76e08467c732ccb026efda0c9cd\";a:15:{s:4:\"name\";s:17:\"Widget Visibility\";s:11:\"description\";s:42:\"Control where widgets appear on your site.\";s:14:\"jumpstart_desc\";s:0:\"\";s:4:\"sort\";s:2:\"17\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:3:\"2.4\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:2:\"No\";s:13:\"auto_activate\";s:3:\"Yes\";s:11:\"module_tags\";s:10:\"Appearance\";s:7:\"feature\";s:10:\"Appearance\";s:25:\"additional_search_queries\";s:54:\"widget visibility, logic, conditional, widgets, widget\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"9b3e84beedf2e96f1ac5dd6498d2b1aa\";a:15:{s:4:\"name\";s:21:\"Extra Sidebar Widgets\";s:11:\"description\";s:54:\"Add images, Twitter streams, and more to your sidebar.\";s:14:\"jumpstart_desc\";s:0:\"\";s:4:\"sort\";s:1:\"4\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:3:\"1.2\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:2:\"No\";s:13:\"auto_activate\";s:3:\"Yes\";s:11:\"module_tags\";s:18:\"Social, Appearance\";s:7:\"feature\";s:10:\"Appearance\";s:25:\"additional_search_queries\";s:65:\"widget, widgets, facebook, gallery, twitter, gravatar, image, rss\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"7724fd9600745cf93e37cc09282e1a37\";a:15:{s:4:\"name\";s:3:\"Ads\";s:11:\"description\";s:60:\"Earn income by allowing Jetpack to display high quality ads.\";s:14:\"jumpstart_desc\";s:0:\"\";s:4:\"sort\";s:1:\"1\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:5:\"4.5.0\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:19:\"Traffic, Appearance\";s:7:\"feature\";s:0:\"\";s:25:\"additional_search_queries\";s:26:\"advertising, ad codes, ads\";s:12:\"plan_classes\";s:17:\"premium, business\";}s:32:\"5b8f8e5b5a1887e3c0393cb78d5143a3\";a:15:{s:4:\"name\";s:0:\"\";s:11:\"description\";s:0:\"\";s:14:\"jumpstart_desc\";s:0:\"\";s:4:\"sort\";s:0:\"\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:0:\"\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:0:\"\";s:13:\"auto_activate\";s:0:\"\";s:11:\"module_tags\";s:0:\"\";s:7:\"feature\";s:0:\"\";s:25:\"additional_search_queries\";s:0:\"\";s:12:\"plan_classes\";s:0:\"\";}}', 'no'),
(311, 'jetpack_available_modules', 'a:1:{s:5:\"6.4.2\";a:43:{s:18:\"after-the-deadline\";s:3:\"1.1\";s:8:\"carousel\";s:3:\"1.5\";s:13:\"comment-likes\";s:3:\"5.1\";s:8:\"comments\";s:3:\"1.4\";s:12:\"contact-form\";s:3:\"1.3\";s:20:\"custom-content-types\";s:3:\"3.1\";s:10:\"custom-css\";s:3:\"1.7\";s:21:\"enhanced-distribution\";s:3:\"1.2\";s:16:\"google-analytics\";s:3:\"4.5\";s:19:\"gravatar-hovercards\";s:3:\"1.1\";s:15:\"infinite-scroll\";s:3:\"2.0\";s:8:\"json-api\";s:3:\"1.9\";s:5:\"latex\";s:3:\"1.1\";s:11:\"lazy-images\";s:5:\"5.6.0\";s:5:\"likes\";s:3:\"2.2\";s:6:\"manage\";s:3:\"3.4\";s:8:\"markdown\";s:3:\"2.8\";s:9:\"masterbar\";s:3:\"4.8\";s:9:\"minileven\";s:3:\"1.8\";s:7:\"monitor\";s:3:\"2.6\";s:5:\"notes\";s:3:\"1.9\";s:6:\"photon\";s:3:\"2.0\";s:13:\"post-by-email\";s:3:\"2.0\";s:7:\"protect\";s:3:\"3.4\";s:9:\"publicize\";s:3:\"2.0\";s:3:\"pwa\";s:5:\"5.6.0\";s:13:\"related-posts\";s:3:\"2.9\";s:6:\"search\";s:3:\"5.0\";s:9:\"seo-tools\";s:3:\"4.4\";s:10:\"sharedaddy\";s:3:\"1.1\";s:10:\"shortcodes\";s:3:\"1.1\";s:10:\"shortlinks\";s:3:\"1.1\";s:8:\"sitemaps\";s:3:\"3.9\";s:3:\"sso\";s:3:\"2.6\";s:5:\"stats\";s:3:\"1.1\";s:13:\"subscriptions\";s:3:\"1.2\";s:13:\"tiled-gallery\";s:3:\"2.1\";s:10:\"vaultpress\";s:5:\"0:1.2\";s:18:\"verification-tools\";s:3:\"3.0\";s:10:\"videopress\";s:3:\"2.5\";s:17:\"widget-visibility\";s:3:\"2.4\";s:7:\"widgets\";s:3:\"1.2\";s:7:\"wordads\";s:5:\"4.5.0\";}}', 'yes'),
(312, 'jetpack_options', 'a:5:{s:7:\"version\";s:16:\"6.4.2:1534240646\";s:11:\"old_version\";s:16:\"6.4.2:1534240646\";s:2:\"id\";i:150355129;s:6:\"public\";i:1;s:9:\"jumpstart\";s:14:\"new_connection\";}', 'yes'),
(314, 'do_activate', '0', 'yes'),
(319, 'woocommerce_setup_automated_taxes', '1', 'yes'),
(320, 'mailchimp_woocommerce_plugin_do_activation_redirect', '', 'yes'),
(322, 'mailchimp_woocommerce_version', '2.1.9', 'no'),
(323, 'mailchimp-woocommerce', 'a:0:{}', 'yes'),
(325, 'mailchimp-woocommerce-store_id', '5b72a79431de0', 'yes'),
(327, 'mailchimp_woocommerce_db_mailchimp_carts', '1', 'no'),
(331, 'jetpack_tos_agreed', '1', 'yes'),
(333, 'wc_connect_options', 'a:1:{s:12:\"tos_accepted\";b:1;}', 'yes'),
(334, '_transient_timeout_external_ip_address_192.0.86.115', '1534845490', 'no'),
(335, '_transient_external_ip_address_192.0.86.115', '87.239.23.31', 'no'),
(336, 'jetpack_private_options', 'a:0:{}', 'yes'),
(337, 'jetpack_last_connect_url_check', '1534250650', 'no'),
(338, '_transient_timeout_external_ip_address_192.0.116.83', '1534845494', 'no'),
(339, '_transient_external_ip_address_192.0.116.83', '87.239.23.31', 'no'),
(350, '_transient_timeout_external_ip_address_86.122.8.23', '1534845532', 'no'),
(351, '_transient_external_ip_address_86.122.8.23', '87.239.23.31', 'no'),
(356, '_transient_timeout_external_ip_address_87.239.23.31', '1534845593', 'no'),
(357, '_transient_external_ip_address_87.239.23.31', '87.239.23.31', 'no'),
(360, 'woocommerce_tracker_last_send', '1534240793', 'yes'),
(362, '_transient_timeout_external_ip_address_35.195.155.157', '1534845667', 'no'),
(363, '_transient_external_ip_address_35.195.155.157', '87.239.23.31', 'no'),
(369, 'product_cat_children', 'a:0:{}', 'yes'),
(370, '_transient_product_query-transient-version', '1534242791', 'yes'),
(372, '_transient_product-transient-version', '1534242791', 'yes'),
(386, '_transient_wc_count_comments', 'O:8:\"stdClass\":7:{s:14:\"total_comments\";i:1;s:3:\"all\";i:1;s:8:\"approved\";s:1:\"1\";s:9:\"moderated\";i:0;s:4:\"spam\";i:0;s:5:\"trash\";i:0;s:12:\"post-trashed\";i:0;}', 'yes'),
(388, '_transient_timeout_wc_term_counts', '1536834793', 'no'),
(389, '_transient_wc_term_counts', 'a:1:{i:16;s:1:\"1\";}', 'no'),
(399, '_transient_timeout_wc_shipping_method_count_1_1534240640', '1536834992', 'no'),
(400, '_transient_wc_shipping_method_count_1_1534240640', '0', 'no'),
(401, '_transient_timeout_wc_shipping_method_count_0_1534240640', '1536835135', 'no'),
(402, '_transient_wc_shipping_method_count_0_1534240640', '0', 'no'),
(403, 'woocommerce_maybe_regenerate_images_hash', '494bdb3708d5a8a5b3be00abab75910e', 'yes'),
(416, 'woocommerce_gateway_stripe_retention', 'a:2:{s:6:\"number\";s:0:\"\";s:4:\"unit\";s:4:\"days\";}', 'no'),
(417, 'woocommerce_paypal_settings', 'a:23:{s:7:\"enabled\";s:2:\"no\";s:5:\"title\";s:6:\"PayPal\";s:11:\"description\";s:85:\"Pay via PayPal; you can pay with your credit card if you don\'t have a PayPal account.\";s:5:\"email\";s:20:\"tudor@contursoft.com\";s:8:\"advanced\";s:0:\"\";s:8:\"testmode\";s:2:\"no\";s:5:\"debug\";s:2:\"no\";s:16:\"ipn_notification\";s:3:\"yes\";s:14:\"receiver_email\";s:20:\"tudor@contursoft.com\";s:14:\"identity_token\";s:0:\"\";s:14:\"invoice_prefix\";s:3:\"WC-\";s:13:\"send_shipping\";s:3:\"yes\";s:16:\"address_override\";s:2:\"no\";s:13:\"paymentaction\";s:4:\"sale\";s:10:\"page_style\";s:0:\"\";s:9:\"image_url\";s:0:\"\";s:11:\"api_details\";s:0:\"\";s:12:\"api_username\";s:0:\"\";s:12:\"api_password\";s:0:\"\";s:13:\"api_signature\";s:0:\"\";s:20:\"sandbox_api_username\";s:0:\"\";s:20:\"sandbox_api_password\";s:0:\"\";s:21:\"sandbox_api_signature\";s:0:\"\";}', 'yes'),
(418, 'woocommerce_gateway_order', 'a:15:{s:4:\"bacs\";i:0;s:6:\"cheque\";i:1;s:3:\"cod\";i:2;s:6:\"paypal\";i:3;s:11:\"ppec_paypal\";i:4;s:6:\"stripe\";i:5;s:11:\"stripe_sepa\";i:6;s:17:\"stripe_bancontact\";i:7;s:13:\"stripe_sofort\";i:8;s:14:\"stripe_giropay\";i:9;s:10:\"stripe_eps\";i:10;s:12:\"stripe_ideal\";i:11;s:10:\"stripe_p24\";i:12;s:13:\"stripe_alipay\";i:13;s:17:\"stripe_multibanco\";i:14;}', 'yes'),
(428, 'woocommerce_tracker_ua', 'a:1:{i:0;s:115:\"mozilla/5.0 (windows nt 10.0; win64; x64) applewebkit/537.36 (khtml, like gecko) chrome/68.0.3440.106 safari/537.36\";}', 'yes'),
(430, '_transient_timeout_wc_low_stock_count', '1536836912', 'no'),
(431, '_transient_wc_low_stock_count', '0', 'no'),
(432, '_transient_timeout_wc_outofstock_count', '1536836912', 'no'),
(433, '_transient_wc_outofstock_count', '0', 'no'),
(445, 'popup_status', 'on', 'yes'),
(448, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}', 'yes'),
(467, 'woosidebars-version', '1.4.5', 'yes'),
(469, '_transient_timeout_wc_addons_sections', '1534852894', 'no'),
(470, '_transient_wc_addons_sections', 'a:9:{i:0;O:8:\"stdClass\":2:{s:4:\"slug\";s:9:\"_featured\";s:5:\"label\";s:8:\"Featured\";}i:1;O:8:\"stdClass\":2:{s:4:\"slug\";s:4:\"_all\";s:5:\"label\";s:3:\"All\";}i:2;O:8:\"stdClass\":2:{s:4:\"slug\";s:7:\"bundles\";s:5:\"label\";s:7:\"Bundles\";}i:3;O:8:\"stdClass\":2:{s:4:\"slug\";s:18:\"product-extensions\";s:5:\"label\";s:12:\"Enhancements\";}i:4;O:8:\"stdClass\":2:{s:4:\"slug\";s:20:\"marketing-extensions\";s:5:\"label\";s:9:\"Marketing\";}i:5;O:8:\"stdClass\":2:{s:4:\"slug\";s:16:\"payment-gateways\";s:5:\"label\";s:8:\"Payments\";}i:6;O:8:\"stdClass\":2:{s:4:\"slug\";s:12:\"product-type\";s:5:\"label\";s:12:\"Product Type\";}i:7;O:8:\"stdClass\":2:{s:4:\"slug\";s:16:\"shipping-methods\";s:5:\"label\";s:8:\"Shipping\";}i:8;O:8:\"stdClass\":2:{s:4:\"slug\";s:10:\"operations\";s:5:\"label\";s:16:\"Store Management\";}}', 'no'),
(471, '_transient_timeout_wc_addons_featured', '1534852894', 'no');
INSERT INTO `r8ISTpM09o_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(472, '_transient_wc_addons_featured', 'O:8:\"stdClass\":1:{s:8:\"sections\";a:11:{i:0;O:8:\"stdClass\":4:{s:6:\"module\";s:12:\"banner_block\";s:5:\"title\";s:50:\"Take your store beyond the typical - sell anything\";s:11:\"description\";s:81:\"From services to content, there\'s no limit to what you can sell with WooCommerce.\";s:5:\"items\";a:9:{i:0;O:8:\"stdClass\":6:{s:4:\"href\";s:139:\"https://woocommerce.com/products/woocommerce-subscriptions/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:5:\"title\";s:13:\"Subscriptions\";s:5:\"image\";s:71:\"https://d3t0oesq8995hv.cloudfront.net/add-ons/subscriptions-icon@2x.png\";s:11:\"description\";s:98:\"Let customers subscribe to your products or services and pay on a weekly, monthly or annual basis.\";s:6:\"button\";s:10:\"From: $199\";s:6:\"plugin\";s:55:\"woocommerce-subscriptions/woocommerce-subscriptions.php\";}i:1;O:8:\"stdClass\":6:{s:4:\"href\";s:134:\"https://woocommerce.com/products/woocommerce-bookings/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:5:\"title\";s:8:\"Bookings\";s:5:\"image\";s:66:\"https://d3t0oesq8995hv.cloudfront.net/add-ons/bookings-icon@2x.png\";s:11:\"description\";s:76:\"Allow customers to book appointments for services without leaving your site.\";s:6:\"button\";s:10:\"From: $249\";s:6:\"plugin\";s:45:\"woocommerce-bookings/woocommerce-bookings.php\";}i:2;O:8:\"stdClass\":6:{s:4:\"href\";s:137:\"https://woocommerce.com/products/woocommerce-memberships/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:5:\"title\";s:11:\"Memberships\";s:5:\"image\";s:69:\"https://d3t0oesq8995hv.cloudfront.net/add-ons/memberships-icon@2x.png\";s:11:\"description\";s:76:\"Give members access to restricted content or products, for a fee or for free\";s:6:\"button\";s:10:\"From: $149\";s:6:\"plugin\";s:51:\"woocommerce-memberships/woocommerce-memberships.php\";}i:3;O:8:\"stdClass\":6:{s:4:\"href\";s:129:\"https://woocommerce.com/products/product-bundles/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:5:\"title\";s:15:\"Product Bundles\";s:5:\"image\";s:57:\"https://d3t0oesq8995hv.cloudfront.net/add-ons/generic.png\";s:11:\"description\";s:50:\"Offer customizable bundles and assembled products.\";s:6:\"button\";s:9:\"From: $49\";s:6:\"plugin\";s:59:\"woocommerce-product-bundles/woocommerce-product-bundles.php\";}i:4;O:8:\"stdClass\":6:{s:4:\"href\";s:132:\"https://woocommerce.com/products/composite-products/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:5:\"title\";s:18:\"Composite Products\";s:5:\"image\";s:57:\"https://d3t0oesq8995hv.cloudfront.net/add-ons/generic.png\";s:11:\"description\";s:59:\"Create and offer product kits with configurable components.\";s:6:\"button\";s:9:\"From: $79\";s:6:\"plugin\";s:65:\"woocommerce-composite-products/woocommerce-composite-products.php\";}i:5;O:8:\"stdClass\":6:{s:4:\"href\";s:129:\"https://woocommerce.com/products/product-vendors/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:5:\"title\";s:15:\"Product Vendors\";s:5:\"image\";s:57:\"https://d3t0oesq8995hv.cloudfront.net/add-ons/generic.png\";s:11:\"description\";s:47:\"Turn your store into a multi-vendor marketplace\";s:6:\"button\";s:9:\"From: $79\";s:6:\"plugin\";s:59:\"woocommerce-product-vendors/woocommerce-product-vendors.php\";}i:6;O:8:\"stdClass\":6:{s:4:\"href\";s:132:\"https://woocommerce.com/products/groups-woocommerce/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:5:\"title\";s:22:\"Groups for WooCommerce\";s:5:\"image\";s:57:\"https://d3t0oesq8995hv.cloudfront.net/add-ons/generic.png\";s:11:\"description\";s:94:\"Sell memberships using the free &#039;Groups&#039; plugin, Groups integration and WooCommerce.\";s:6:\"button\";s:9:\"From: $79\";s:6:\"plugin\";s:41:\"groups-woocommerce/groups-woocommerce.php\";}i:7;O:8:\"stdClass\":6:{s:4:\"href\";s:136:\"https://woocommerce.com/products/woocommerce-pre-orders/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:5:\"title\";s:22:\"WooCommerce Pre-Orders\";s:5:\"image\";s:57:\"https://d3t0oesq8995hv.cloudfront.net/add-ons/generic.png\";s:11:\"description\";s:60:\"Allow customers to order products before they are available.\";s:6:\"button\";s:10:\"From: $129\";s:6:\"plugin\";s:49:\"woocommerce-pre-orders/woocommerce-pre-orders.php\";}i:8;O:8:\"stdClass\":6:{s:4:\"href\";s:130:\"https://woocommerce.com/products/chained-products/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:5:\"title\";s:16:\"Chained Products\";s:5:\"image\";s:57:\"https://d3t0oesq8995hv.cloudfront.net/add-ons/generic.png\";s:11:\"description\";s:69:\"Create and sell pre-configured product bundles and discounted combos.\";s:6:\"button\";s:9:\"From: $49\";s:6:\"plugin\";s:61:\"woocommerce-chained-products/woocommerce-chained-products.php\";}}}i:1;O:8:\"stdClass\":1:{s:6:\"module\";s:16:\"wcs_banner_block\";}i:2;O:8:\"stdClass\":2:{s:6:\"module\";s:12:\"column_start\";s:9:\"container\";s:22:\"column_container_start\";}i:3;O:8:\"stdClass\":4:{s:6:\"module\";s:12:\"column_block\";s:5:\"title\";s:46:\"Improve the main features of your online store\";s:11:\"description\";s:71:\"Sell more by helping customers find the products and options they want.\";s:5:\"items\";a:9:{i:0;O:8:\"stdClass\":6:{s:4:\"href\";s:129:\"https://woocommerce.com/products/product-add-ons/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:5:\"title\";s:15:\"Product Add-ons\";s:5:\"image\";s:73:\"https://d3t0oesq8995hv.cloudfront.net/add-ons/product-add-ons-icon@2x.png\";s:11:\"description\";s:82:\"Give your customers the option to customize their purchase or add personalization.\";s:6:\"button\";s:9:\"From: $49\";s:6:\"plugin\";s:57:\"woocommerce-product-addons/woocommerce-product-addons.php\";}i:1;O:8:\"stdClass\":6:{s:4:\"href\";s:140:\"https://woocommerce.com/products/woocommerce-product-search/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:5:\"title\";s:14:\"Product Search\";s:5:\"image\";s:72:\"https://d3t0oesq8995hv.cloudfront.net/add-ons/product-search-icon@2x.png\";s:11:\"description\";s:67:\"Make sure customers find what they want when they search your site.\";s:6:\"button\";s:9:\"From: $49\";s:6:\"plugin\";s:57:\"woocommerce-product-search/woocommerce-product-search.php\";}i:2;O:8:\"stdClass\":6:{s:4:\"href\";s:142:\"https://woocommerce.com/products/woocommerce-checkout-add-ons/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:5:\"title\";s:16:\"Checkout Add-ons\";s:5:\"image\";s:74:\"https://d3t0oesq8995hv.cloudfront.net/add-ons/checkout-add-ons-icon@2x.png\";s:11:\"description\";s:89:\"Highlight relevant products, offers like free shipping and other upsells during checkout.\";s:6:\"button\";s:9:\"From: $49\";s:6:\"plugin\";s:61:\"woocommerce-checkout-add-ons/woocommerce-checkout-add-ons.php\";}i:3;O:8:\"stdClass\":6:{s:4:\"href\";s:147:\"https://woocommerce.com/products/woocommerce-checkout-field-editor/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:5:\"title\";s:21:\"Checkout Field Editor\";s:5:\"image\";s:57:\"https://d3t0oesq8995hv.cloudfront.net/add-ons/generic.png\";s:11:\"description\";s:128:\"The checkout field editor provides you with an interface to add, edit and remove fields shown on your WooCommerce checkout page.\";s:6:\"button\";s:9:\"From: $49\";s:6:\"plugin\";s:71:\"woocommerce-checkout-field-editor/woocommerce-checkout-field-editor.php\";}i:4;O:8:\"stdClass\":6:{s:4:\"href\";s:138:\"https://woocommerce.com/products/woocommerce-social-login/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:5:\"title\";s:24:\"WooCommerce Social Login\";s:5:\"image\";s:57:\"https://d3t0oesq8995hv.cloudfront.net/add-ons/generic.png\";s:11:\"description\";s:62:\"Enable Social Login for Seamless Checkout and Account Creation\";s:6:\"button\";s:9:\"From: $79\";s:6:\"plugin\";s:53:\"woocommerce-social-login/woocommerce-social-login.php\";}i:5;O:8:\"stdClass\":6:{s:4:\"href\";s:135:\"https://woocommerce.com/products/woocommerce-wishlists/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:5:\"title\";s:21:\"WooCommerce Wishlists\";s:5:\"image\";s:57:\"https://d3t0oesq8995hv.cloudfront.net/add-ons/generic.png\";s:11:\"description\";s:113:\"WooCommerce Wishlists allows guests and customers to create and add products to an unlimited number of Wishlists.\";s:6:\"button\";s:9:\"From: $79\";s:6:\"plugin\";s:47:\"woocommerce-wishlists/woocommerce-wishlists.php\";}i:6;O:8:\"stdClass\":6:{s:4:\"href\";s:126:\"https://woocommerce.com/products/cart-notices/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:5:\"title\";s:12:\"Cart Notices\";s:5:\"image\";s:57:\"https://d3t0oesq8995hv.cloudfront.net/add-ons/generic.png\";s:11:\"description\";s:73:\"Display dynamic, actionable messages to your customers as they check out.\";s:6:\"button\";s:9:\"From: $49\";s:6:\"plugin\";s:53:\"woocommerce-cart-notices/woocommerce-cart-notices.php\";}i:7;O:8:\"stdClass\":6:{s:4:\"href\";s:126:\"https://woocommerce.com/products/cart-add-ons/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:5:\"title\";s:12:\"Cart Add-ons\";s:5:\"image\";s:57:\"https://d3t0oesq8995hv.cloudfront.net/add-ons/generic.png\";s:11:\"description\";s:109:\"A powerful tool for driving incremental and impulse purchases by customers once they are in the shopping cart\";s:6:\"button\";s:9:\"From: $29\";s:6:\"plugin\";s:53:\"woocommerce-cart-add-ons/woocommerce-cart-add-ons.php\";}i:8;O:8:\"stdClass\":6:{s:4:\"href\";s:134:\"https://woocommerce.com/products/woocommerce-waitlist/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:5:\"title\";s:20:\"WooCommerce Waitlist\";s:5:\"image\";s:57:\"https://d3t0oesq8995hv.cloudfront.net/add-ons/generic.png\";s:11:\"description\";s:117:\"With WooCommerce Waitlist customers can register for email notifications when out-of-stock products become available.\";s:6:\"button\";s:9:\"From: $49\";s:6:\"plugin\";s:45:\"woocommerce-waitlist/woocommerce-waitlist.php\";}}}i:4;O:8:\"stdClass\":5:{s:6:\"module\";s:17:\"small_light_block\";s:5:\"title\";s:34:\"Get the official WooCommerce theme\";s:11:\"description\";s:128:\"Storefront is the lean, flexible, and free theme, built by the people who make WooCommerce - everything you need to get started.\";s:5:\"image\";s:70:\"https://d3t0oesq8995hv.cloudfront.net/add-ons/storefront-screen@2x.png\";s:7:\"buttons\";a:2:{i:0;O:8:\"stdClass\":2:{s:4:\"href\";s:44:\"/wp-admin/theme-install.php?theme=storefront\";s:4:\"text\";s:7:\"Install\";}i:1;O:8:\"stdClass\":2:{s:4:\"href\";s:115:\"https://woocommerce.com/storefront/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:4:\"text\";s:9:\"Read More\";}}}i:5;O:8:\"stdClass\":1:{s:6:\"module\";s:10:\"column_end\";}i:6;O:8:\"stdClass\":1:{s:6:\"module\";s:12:\"column_start\";}i:7;O:8:\"stdClass\":4:{s:6:\"module\";s:16:\"small_dark_block\";s:5:\"title\";s:20:\"Square + WooCommerce\";s:11:\"description\";s:176:\"Keep your WooCommerce and brick-and-mortar stores in sync. Use Square to take payments both online and offline, keep inventory updated between the two and sync product changes.\";s:5:\"items\";a:1:{i:0;O:8:\"stdClass\":2:{s:4:\"href\";s:120:\"https://woocommerce.com/products/square/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:6:\"button\";s:5:\"Free!\";}}}i:8;O:8:\"stdClass\":4:{s:6:\"module\";s:12:\"column_block\";s:5:\"title\";s:19:\"Get deeper insights\";s:11:\"description\";s:58:\"Learn how your store is performing with enhanced reporting\";s:5:\"items\";a:8:{i:0;O:8:\"stdClass\":6:{s:4:\"href\";s:142:\"https://woocommerce.com/products/woocommerce-google-analytics/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:5:\"title\";s:16:\"Google Analytics\";s:5:\"image\";s:60:\"https://d3t0oesq8995hv.cloudfront.net/add-ons/ga-icon@2x.png\";s:11:\"description\";s:93:\"Understand your customers and increase revenue with the world’s leading analytics platform.\";s:6:\"button\";s:4:\"Free\";s:6:\"plugin\";s:85:\"woocommerce-google-analytics-integration/woocommerce-google-analytics-integration.php\";}i:1;O:8:\"stdClass\":6:{s:4:\"href\";s:138:\"https://woocommerce.com/products/woocommerce-cart-reports/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:5:\"title\";s:12:\"Cart reports\";s:5:\"image\";s:70:\"https://d3t0oesq8995hv.cloudfront.net/add-ons/cart-reports-icon@2x.png\";s:11:\"description\";s:66:\"Get real-time reports on what customers are leaving in their cart.\";s:6:\"button\";s:9:\"From: $79\";s:6:\"plugin\";s:53:\"woocommerce-cart-reports/woocommerce-cart-reports.php\";}i:2;O:8:\"stdClass\":6:{s:4:\"href\";s:139:\"https://woocommerce.com/products/woocommerce-cost-of-goods/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:5:\"title\";s:13:\"Cost of Goods\";s:5:\"image\";s:71:\"https://d3t0oesq8995hv.cloudfront.net/add-ons/cost-of-goods-icon@2x.png\";s:11:\"description\";s:64:\"Easily track profit by including  cost of goods in your reports.\";s:6:\"button\";s:9:\"From: $79\";s:6:\"plugin\";s:55:\"woocommerce-cost-of-goods/woocommerce-cost-of-goods.php\";}i:3;O:8:\"stdClass\":6:{s:4:\"href\";s:146:\"https://woocommerce.com/products/woocommerce-google-analytics-pro/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:5:\"title\";s:32:\"WooCommerce Google Analytics Pro\";s:5:\"image\";s:57:\"https://d3t0oesq8995hv.cloudfront.net/add-ons/generic.png\";s:11:\"description\";s:85:\"Add advanced event tracking and enhanced eCommerce tracking to your WooCommerce site.\";s:6:\"button\";s:9:\"From: $29\";s:6:\"plugin\";s:69:\"woocommerce-google-analytics-pro/woocommerce-google-analytics-pro.php\";}i:4;O:8:\"stdClass\":6:{s:4:\"href\";s:142:\"https://woocommerce.com/products/woocommerce-customer-history/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:5:\"title\";s:28:\"WooCommerce Customer History\";s:5:\"image\";s:57:\"https://d3t0oesq8995hv.cloudfront.net/add-ons/generic.png\";s:11:\"description\";s:125:\"Observe how your customers use your store, keep a full purchase history log, and calculate the total customer lifetime value.\";s:6:\"button\";s:9:\"From: $49\";s:6:\"plugin\";s:61:\"woocommerce-customer-history/woocommerce-customer-history.php\";}i:5;O:8:\"stdClass\":6:{s:4:\"href\";s:126:\"https://woocommerce.com/products/kiss-metrics/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:5:\"title\";s:11:\"Kissmetrics\";s:5:\"image\";s:57:\"https://d3t0oesq8995hv.cloudfront.net/add-ons/generic.png\";s:11:\"description\";s:79:\"Easily add Kissmetrics event tracking to your WooCommerce store with one click.\";s:6:\"button\";s:9:\"From: $79\";s:6:\"plugin\";s:52:\"woocommerce-kiss-metrics/woocommerce-kissmetrics.php\";}i:6;O:8:\"stdClass\":6:{s:4:\"href\";s:122:\"https://woocommerce.com/products/mixpanel/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:5:\"title\";s:8:\"Mixpanel\";s:5:\"image\";s:57:\"https://d3t0oesq8995hv.cloudfront.net/add-ons/generic.png\";s:11:\"description\";s:65:\"Add event tracking powered by Mixpanel to your WooCommerce store.\";s:6:\"button\";s:9:\"From: $79\";s:6:\"plugin\";s:45:\"woocommerce-mixpanel/woocommerce-mixpanel.php\";}i:7;O:8:\"stdClass\":6:{s:4:\"href\";s:144:\"https://woocommerce.com/products/woocommerce-sales-report-email/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:5:\"title\";s:30:\"WooCommerce Sales Report Email\";s:5:\"image\";s:57:\"https://d3t0oesq8995hv.cloudfront.net/add-ons/generic.png\";s:11:\"description\";s:107:\"Receive emails daily, weekly or monthly with meaningful information about how your products are performing.\";s:6:\"button\";s:9:\"From: $29\";s:6:\"plugin\";s:65:\"woocommerce-sales-report-email/woocommerce-sales-report-email.php\";}}}i:9;O:8:\"stdClass\":2:{s:6:\"module\";s:10:\"column_end\";s:9:\"container\";s:20:\"column_container_end\";}i:10;O:8:\"stdClass\":4:{s:6:\"module\";s:12:\"banner_block\";s:5:\"title\";s:40:\"Promote your products and increase sales\";s:11:\"description\";s:77:\"From coupons to emails, these extensions can power up your marketing efforts.\";s:5:\"items\";a:9:{i:0;O:8:\"stdClass\":6:{s:4:\"href\";s:127:\"https://woocommerce.com/products/smart-coupons/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:5:\"title\";s:13:\"Smart Coupons\";s:5:\"image\";s:71:\"https://d3t0oesq8995hv.cloudfront.net/add-ons/smart-coupons-icon@2x.png\";s:11:\"description\";s:106:\"Enhance your coupon options - create gift certificates, store credit, coupons based on purchases and more.\";s:6:\"button\";s:9:\"From: $99\";s:6:\"plugin\";s:55:\"woocommerce-smart-coupons/woocommerce-smart-coupons.php\";}i:1;O:8:\"stdClass\":6:{s:4:\"href\";s:130:\"https://woocommerce.com/products/follow-up-emails/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:5:\"title\";s:16:\"Follow Up Emails\";s:5:\"image\";s:74:\"https://d3t0oesq8995hv.cloudfront.net/add-ons/follow-up-emails-icon@2x.png\";s:11:\"description\";s:140:\"Automatically contact customers after purchase - be it everyone, your most loyal or your biggest spenders - and keep your store top-of-mind.\";s:6:\"button\";s:9:\"From: $99\";s:6:\"plugin\";s:61:\"woocommerce-follow-up-emails/woocommerce-follow-up-emails.php\";}i:2;O:8:\"stdClass\":6:{s:4:\"href\";s:133:\"https://woocommerce.com/products/google-product-feed/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:5:\"title\";s:19:\"Google Product Feed\";s:5:\"image\";s:77:\"https://d3t0oesq8995hv.cloudfront.net/add-ons/google-product-feed-icon@2x.png\";s:11:\"description\";s:61:\"Let customers find you when shopping for products via Google.\";s:6:\"button\";s:9:\"From: $79\";s:6:\"plugin\";s:45:\"woocommerce-product-feeds/woocommerce-gpf.php\";}i:3;O:8:\"stdClass\":6:{s:4:\"href\";s:129:\"https://woocommerce.com/products/dynamic-pricing/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:5:\"title\";s:15:\"Dynamic Pricing\";s:5:\"image\";s:57:\"https://d3t0oesq8995hv.cloudfront.net/add-ons/generic.png\";s:11:\"description\";s:48:\"Bulk discounts, role-based pricing and much more\";s:6:\"button\";s:10:\"From: $129\";s:6:\"plugin\";s:59:\"woocommerce-dynamic-pricing/woocommerce-dynamic-pricing.php\";}i:4;O:8:\"stdClass\":6:{s:4:\"href\";s:144:\"https://woocommerce.com/products/woocommerce-points-and-rewards/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:5:\"title\";s:30:\"WooCommerce Points and Rewards\";s:5:\"image\";s:57:\"https://d3t0oesq8995hv.cloudfront.net/add-ons/generic.png\";s:11:\"description\";s:102:\"Reward your customers for purchases and other actions with points which can be redeemed for discounts.\";s:6:\"button\";s:10:\"From: $129\";s:6:\"plugin\";s:65:\"woocommerce-points-and-rewards/woocommerce-points-and-rewards.php\";}i:5;O:8:\"stdClass\":6:{s:4:\"href\";s:126:\"https://woocommerce.com/products/store-credit/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:5:\"title\";s:24:\"WooCommerce Store Credit\";s:5:\"image\";s:57:\"https://d3t0oesq8995hv.cloudfront.net/add-ons/generic.png\";s:11:\"description\";s:152:\"Generate store credit coupons that enable customers to make multiple purchases until the total value specified is exhausted or the coupons life expires.\";s:6:\"button\";s:9:\"From: $29\";s:6:\"plugin\";s:53:\"woocommerce-store-credit/woocommerce-store-credit.php\";}i:6;O:8:\"stdClass\":6:{s:4:\"href\";s:122:\"https://woocommerce.com/products/facebook/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:5:\"title\";s:24:\"Facebook for WooCommerce\";s:5:\"image\";s:57:\"https://d3t0oesq8995hv.cloudfront.net/add-ons/generic.png\";s:11:\"description\";s:89:\"Get the Facebook for WooCommerce plugin for two powerful ways to help grow your business.\";s:6:\"button\";s:4:\"Free\";s:6:\"plugin\";s:53:\"facebook-for-woocommerce/facebook-for-woocommerce.php\";}i:7;O:8:\"stdClass\":6:{s:4:\"href\";s:137:\"https://woocommerce.com/products/newsletter-subscription/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:5:\"title\";s:23:\"Newsletter Subscription\";s:5:\"image\";s:57:\"https://d3t0oesq8995hv.cloudfront.net/add-ons/generic.png\";s:11:\"description\";s:127:\"Allow customers to subscribe to your MailChimp or CampaignMonitor mailing list(s) via a widget or by opting in during checkout.\";s:6:\"button\";s:9:\"From: $49\";s:6:\"plugin\";s:63:\"woocommerce-subscribe-to-newsletter/subscribe-to-newsletter.php\";}i:8;O:8:\"stdClass\":6:{s:4:\"href\";s:142:\"https://woocommerce.com/products/woocommerce-email-customizer/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons&utm_content=featured\";s:5:\"title\";s:28:\"WooCommerce Email Customizer\";s:5:\"image\";s:57:\"https://d3t0oesq8995hv.cloudfront.net/add-ons/generic.png\";s:11:\"description\";s:125:\"Connect with your customers with each email you send by visually modifying your email templates via the WordPress Customizer.\";s:6:\"button\";s:9:\"From: $79\";s:6:\"plugin\";s:61:\"woocommerce-email-customizer/woocommerce-email-customizer.php\";}}}}}', 'no'),
(481, 'widget_woocommercemyaccountwidget', 'a:2:{i:2;a:11:{s:16:\"logged_out_title\";s:14:\"Customer Login\";s:15:\"logged_in_title\";s:10:\"Welcome %s\";s:13:\"show_cartlink\";i:0;s:10:\"show_items\";i:0;s:11:\"show_upload\";i:0;s:15:\"show_upload_new\";i:0;s:11:\"show_unpaid\";i:0;s:12:\"show_pending\";i:0;s:16:\"show_logout_link\";i:0;s:16:\"login_with_email\";i:1;s:12:\"wma_redirect\";s:2:\"46\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(482, 'wma_login_with_email', 'on', 'yes'),
(502, 'jetpack_sync_settings_disable', '0', 'yes'),
(505, 'jpsq_sync-1534250818.920353-284599-1', 'a:6:{i:0;s:18:\"deactivated_plugin\";i:1;a:2:{i:0;s:19:\"jetpack/jetpack.php\";i:1;b:0;}i:2;i:1;i:3;d:1534250818.920293;i:4;b:0;i:5;a:13:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:5:\"admin\";s:10:\"user_email\";s:20:\"tudor@contursoft.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;s:6:\"is_cli\";b:0;}}', 'no'),
(506, 'jpsq_sync-1534250818.921570-284599-2', 'a:6:{i:0;s:14:\"updated_option\";i:1;a:3:{i:0;s:14:\"active_plugins\";i:1;a:7:{i:0;s:19:\"jetpack/jetpack.php\";i:1;s:51:\"mailchimp-for-woocommerce/mailchimp-woocommerce.php\";i:2;s:91:\"woocommerce-gateway-paypal-express-checkout/woocommerce-gateway-paypal-express-checkout.php\";i:3;s:57:\"woocommerce-gateway-stripe/woocommerce-gateway-stripe.php\";i:4;s:63:\"woocommerce-my-account-widget/woocommerce-my-account-widget.php\";i:5;s:45:\"woocommerce-services/woocommerce-services.php\";i:6;s:27:\"woocommerce/woocommerce.php\";}i:2;a:6:{i:1;s:51:\"mailchimp-for-woocommerce/mailchimp-woocommerce.php\";i:2;s:91:\"woocommerce-gateway-paypal-express-checkout/woocommerce-gateway-paypal-express-checkout.php\";i:3;s:57:\"woocommerce-gateway-stripe/woocommerce-gateway-stripe.php\";i:4;s:63:\"woocommerce-my-account-widget/woocommerce-my-account-widget.php\";i:5;s:45:\"woocommerce-services/woocommerce-services.php\";i:6;s:27:\"woocommerce/woocommerce.php\";}}i:2;i:1;i:3;d:1534250818.92154;i:4;b:0;i:5;a:13:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:5:\"admin\";s:10:\"user_email\";s:20:\"tudor@contursoft.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;s:6:\"is_cli\";b:0;}}', 'no'),
(509, 'jpsq_sync-1534250818.924101-284599-3', 'a:6:{i:0;s:21:\"jetpack_sync_constant\";i:1;a:2:{i:0;s:16:\"EMPTY_TRASH_DAYS\";i:1;i:30;}i:2;i:1;i:3;d:1534250818.924062;i:4;b:0;i:5;a:13:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:5:\"admin\";s:10:\"user_email\";s:20:\"tudor@contursoft.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;s:6:\"is_cli\";b:0;}}', 'no'),
(510, 'jpsq_sync-1534250818.924394-284599-4', 'a:6:{i:0;s:21:\"jetpack_sync_constant\";i:1;a:2:{i:0;s:17:\"WP_POST_REVISIONS\";i:1;b:1;}i:2;i:1;i:3;d:1534250818.924366;i:4;b:0;i:5;a:13:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:5:\"admin\";s:10:\"user_email\";s:20:\"tudor@contursoft.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;s:6:\"is_cli\";b:0;}}', 'no'),
(511, 'jpsq_sync-1534250818.924675-284599-5', 'a:6:{i:0;s:21:\"jetpack_sync_constant\";i:1;a:2:{i:0;s:7:\"ABSPATH\";i:1;s:64:\"/var/www/vhosts/dev.contursoft.ro/cndeveloper.loc/\";}i:2;i:1;i:3;d:1534250818.924647;i:4;b:0;i:5;a:13:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:5:\"admin\";s:10:\"user_email\";s:20:\"tudor@contursoft.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;s:6:\"is_cli\";b:0;}}', 'no'),
(512, 'jpsq_sync-1534250818.924979-284599-6', 'a:6:{i:0;s:21:\"jetpack_sync_constant\";i:1;a:2:{i:0;s:14:\"WP_CONTENT_DIR\";i:1;s:74:\"/var/www/vhosts/dev.contursoft.ro/cndeveloper.loc/wp-content\";}i:2;i:1;i:3;d:1534250818.924943;i:4;b:0;i:5;a:13:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:5:\"admin\";s:10:\"user_email\";s:20:\"tudor@contursoft.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;s:6:\"is_cli\";b:0;}}', 'no'),
(513, 'jpsq_sync-1534250818.925293-284599-7', 'a:6:{i:0;s:21:\"jetpack_sync_constant\";i:1;a:2:{i:0;s:19:\"WP_AUTO_UPDATE_CORE\";i:1;b:0;}i:2;i:1;i:3;d:1534250818.925262;i:4;b:0;i:5;a:13:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:5:\"admin\";s:10:\"user_email\";s:20:\"tudor@contursoft.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;s:6:\"is_cli\";b:0;}}', 'no'),
(514, 'jpsq_sync-1534250818.925571-284599-8', 'a:6:{i:0;s:21:\"jetpack_sync_constant\";i:1;a:2:{i:0;s:16:\"JETPACK__VERSION\";i:1;s:5:\"6.4.2\";}i:2;i:1;i:3;d:1534250818.925545;i:4;b:0;i:5;a:13:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:5:\"admin\";s:10:\"user_email\";s:20:\"tudor@contursoft.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;s:6:\"is_cli\";b:0;}}', 'no'),
(515, 'jpsq_sync-1534250818.925846-284599-9', 'a:6:{i:0;s:21:\"jetpack_sync_constant\";i:1;a:2:{i:0;s:20:\"WP_CRON_LOCK_TIMEOUT\";i:1;i:60;}i:2;i:1;i:3;d:1534250818.92582;i:4;b:0;i:5;a:13:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:5:\"admin\";s:10:\"user_email\";s:20:\"tudor@contursoft.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;s:6:\"is_cli\";b:0;}}', 'no'),
(516, 'jpsq_sync-1534250818.926130-284599-10', 'a:6:{i:0;s:21:\"jetpack_sync_constant\";i:1;a:2:{i:0;s:11:\"PHP_VERSION\";i:1;s:6:\"7.1.18\";}i:2;i:1;i:3;d:1534250818.926101;i:4;b:0;i:5;a:13:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:5:\"admin\";s:10:\"user_email\";s:20:\"tudor@contursoft.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;s:6:\"is_cli\";b:0;}}', 'no'),
(517, 'jpsq_sync-1534250818.926483-284599-11', 'a:6:{i:0;s:21:\"jetpack_sync_constant\";i:1;a:2:{i:0;s:15:\"WP_MEMORY_LIMIT\";i:1;s:3:\"40M\";}i:2;i:1;i:3;d:1534250818.926454;i:4;b:0;i:5;a:13:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:5:\"admin\";s:10:\"user_email\";s:20:\"tudor@contursoft.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;s:6:\"is_cli\";b:0;}}', 'no'),
(518, 'jpsq_sync-1534250818.926768-284599-12', 'a:6:{i:0;s:21:\"jetpack_sync_constant\";i:1;a:2:{i:0;s:19:\"WP_MAX_MEMORY_LIMIT\";i:1;s:4:\"512M\";}i:2;i:1;i:3;d:1534250818.92674;i:4;b:0;i:5;a:13:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:5:\"admin\";s:10:\"user_email\";s:20:\"tudor@contursoft.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;s:6:\"is_cli\";b:0;}}', 'no'),
(519, 'jpsq_sync-1534250818.927080-284599-13', 'a:6:{i:0;s:21:\"jetpack_sync_constant\";i:1;a:2:{i:0;s:14:\"WC_PLUGIN_FILE\";i:1;s:110:\"/var/www/vhosts/dev.contursoft.ro/cndeveloper.loc/wp-content/plugins/woocommerce/woocommerce.php\";}i:2;i:1;i:3;d:1534250818.927053;i:4;b:0;i:5;a:13:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:5:\"admin\";s:10:\"user_email\";s:20:\"tudor@contursoft.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;s:6:\"is_cli\";b:0;}}', 'no'),
(520, 'jpsq_sync-1534250818.927376-284599-14', 'a:6:{i:0;s:21:\"jetpack_sync_constant\";i:1;a:2:{i:0;s:10:\"WC_ABSPATH\";i:1;s:95:\"/var/www/vhosts/dev.contursoft.ro/cndeveloper.loc/wp-content/plugins/woocommerce/\";}i:2;i:1;i:3;d:1534250818.927349;i:4;b:0;i:5;a:13:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:5:\"admin\";s:10:\"user_email\";s:20:\"tudor@contursoft.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;s:6:\"is_cli\";b:0;}}', 'no'),
(521, 'jpsq_sync-1534250818.927643-284599-15', 'a:6:{i:0;s:21:\"jetpack_sync_constant\";i:1;a:2:{i:0;s:18:\"WC_PLUGIN_BASENAME\";i:1;s:27:\"woocommerce/woocommerce.php\";}i:2;i:1;i:3;d:1534250818.927617;i:4;b:0;i:5;a:13:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:5:\"admin\";s:10:\"user_email\";s:20:\"tudor@contursoft.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;s:6:\"is_cli\";b:0;}}', 'no'),
(522, 'jpsq_sync-1534250818.928128-284599-16', 'a:6:{i:0;s:21:\"jetpack_sync_constant\";i:1;a:2:{i:0;s:10:\"WC_VERSION\";i:1;s:5:\"3.4.4\";}i:2;i:1;i:3;d:1534250818.928101;i:4;b:0;i:5;a:13:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:5:\"admin\";s:10:\"user_email\";s:20:\"tudor@contursoft.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;s:6:\"is_cli\";b:0;}}', 'no'),
(523, 'jpsq_sync-1534250818.928409-284599-17', 'a:6:{i:0;s:21:\"jetpack_sync_constant\";i:1;a:2:{i:0;s:19:\"WOOCOMMERCE_VERSION\";i:1;s:5:\"3.4.4\";}i:2;i:1;i:3;d:1534250818.928382;i:4;b:0;i:5;a:13:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:5:\"admin\";s:10:\"user_email\";s:20:\"tudor@contursoft.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;s:6:\"is_cli\";b:0;}}', 'no'),
(524, 'jpsq_sync-1534250818.928684-284599-18', 'a:6:{i:0;s:21:\"jetpack_sync_constant\";i:1;a:2:{i:0;s:21:\"WC_ROUNDING_PRECISION\";i:1;i:6;}i:2;i:1;i:3;d:1534250818.928657;i:4;b:0;i:5;a:13:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:5:\"admin\";s:10:\"user_email\";s:20:\"tudor@contursoft.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;s:6:\"is_cli\";b:0;}}', 'no'),
(525, 'jpsq_sync-1534250818.928975-284599-19', 'a:6:{i:0;s:21:\"jetpack_sync_constant\";i:1;a:2:{i:0;s:25:\"WC_DISCOUNT_ROUNDING_MODE\";i:1;i:2;}i:2;i:1;i:3;d:1534250818.928941;i:4;b:0;i:5;a:13:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:5:\"admin\";s:10:\"user_email\";s:20:\"tudor@contursoft.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;s:6:\"is_cli\";b:0;}}', 'no'),
(526, 'jpsq_sync-1534250818.929268-284599-20', 'a:6:{i:0;s:21:\"jetpack_sync_constant\";i:1;a:2:{i:0;s:20:\"WC_TAX_ROUNDING_MODE\";i:1;i:1;}i:2;i:1;i:3;d:1534250818.929242;i:4;b:0;i:5;a:13:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:5:\"admin\";s:10:\"user_email\";s:20:\"tudor@contursoft.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;s:6:\"is_cli\";b:0;}}', 'no'),
(527, 'jpsq_sync-1534250818.929620-284599-21', 'a:6:{i:0;s:21:\"jetpack_sync_constant\";i:1;a:2:{i:0;s:12:\"WC_DELIMITER\";i:1;s:1:\"|\";}i:2;i:1;i:3;d:1534250818.92958;i:4;b:0;i:5;a:13:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:5:\"admin\";s:10:\"user_email\";s:20:\"tudor@contursoft.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;s:6:\"is_cli\";b:0;}}', 'no'),
(528, 'jpsq_sync-1534250818.929906-284599-22', 'a:6:{i:0;s:21:\"jetpack_sync_constant\";i:1;a:2:{i:0;s:10:\"WC_LOG_DIR\";i:1;s:91:\"/var/www/vhosts/dev.contursoft.ro/cndeveloper.loc/wp-content/uploads/wc-logs/\";}i:2;i:1;i:3;d:1534250818.929875;i:4;b:0;i:5;a:13:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:5:\"admin\";s:10:\"user_email\";s:20:\"tudor@contursoft.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;s:6:\"is_cli\";b:0;}}', 'no'),
(529, 'jpsq_sync-1534250818.930194-284599-23', 'a:6:{i:0;s:21:\"jetpack_sync_constant\";i:1;a:2:{i:0;s:22:\"WC_SESSION_CACHE_GROUP\";i:1;s:13:\"wc_session_id\";}i:2;i:1;i:3;d:1534250818.930167;i:4;b:0;i:5;a:13:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:5:\"admin\";s:10:\"user_email\";s:20:\"tudor@contursoft.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;s:6:\"is_cli\";b:0;}}', 'no'),
(530, 'jpsq_sync-1534250818.930456-284599-24', 'a:6:{i:0;s:21:\"jetpack_sync_constant\";i:1;a:2:{i:0;s:22:\"WC_TEMPLATE_DEBUG_MODE\";i:1;b:0;}i:2;i:1;i:3;d:1534250818.93043;i:4;b:0;i:5;a:13:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:5:\"admin\";s:10:\"user_email\";s:20:\"tudor@contursoft.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;s:6:\"is_cli\";b:0;}}', 'no'),
(531, 'jetpack_constants_sync_checksum', 'a:31:{s:16:\"EMPTY_TRASH_DAYS\";i:2473281379;s:17:\"WP_POST_REVISIONS\";i:4261170317;s:26:\"AUTOMATIC_UPDATER_DISABLED\";i:634125391;s:7:\"ABSPATH\";i:3699569375;s:14:\"WP_CONTENT_DIR\";i:3376906830;s:9:\"FS_METHOD\";i:634125391;s:18:\"DISALLOW_FILE_EDIT\";i:634125391;s:18:\"DISALLOW_FILE_MODS\";i:634125391;s:19:\"WP_AUTO_UPDATE_CORE\";i:734881840;s:22:\"WP_HTTP_BLOCK_EXTERNAL\";i:634125391;s:19:\"WP_ACCESSIBLE_HOSTS\";i:634125391;s:16:\"JETPACK__VERSION\";i:471241623;s:12:\"IS_PRESSABLE\";i:634125391;s:15:\"DISABLE_WP_CRON\";i:634125391;s:17:\"ALTERNATE_WP_CRON\";i:634125391;s:20:\"WP_CRON_LOCK_TIMEOUT\";i:3994858278;s:11:\"PHP_VERSION\";i:3051978331;s:15:\"WP_MEMORY_LIMIT\";i:3065409971;s:19:\"WP_MAX_MEMORY_LIMIT\";i:1839787262;s:14:\"WC_PLUGIN_FILE\";i:933524957;s:10:\"WC_ABSPATH\";i:3256836219;s:18:\"WC_PLUGIN_BASENAME\";i:1149093810;s:10:\"WC_VERSION\";i:444704162;s:19:\"WOOCOMMERCE_VERSION\";i:444704162;s:21:\"WC_ROUNDING_PRECISION\";i:498629140;s:25:\"WC_DISCOUNT_ROUNDING_MODE\";i:450215437;s:20:\"WC_TAX_ROUNDING_MODE\";i:2212294583;s:12:\"WC_DELIMITER\";i:2455911554;s:10:\"WC_LOG_DIR\";i:1087758949;s:22:\"WC_SESSION_CACHE_GROUP\";i:4278978988;s:22:\"WC_TEMPLATE_DEBUG_MODE\";i:734881840;}', 'yes'),
(534, 'jetpack_sync_https_history_main_network_site_url', 'a:1:{i:0;s:5:\"https\";}', 'yes'),
(535, 'jetpack_sync_https_history_site_url', 'a:2:{i:0;s:5:\"https\";i:1;s:5:\"https\";}', 'yes'),
(536, 'jetpack_sync_https_history_home_url', 'a:2:{i:0;s:5:\"https\";i:1;s:5:\"https\";}', 'yes'),
(539, 'jpsq_sync-1534250818.950661-284599-25', 'a:6:{i:0;s:21:\"jetpack_sync_callable\";i:1;a:2:{i:0;s:18:\"wp_max_upload_size\";i:1;i:8388608;}i:2;i:1;i:3;d:1534250818.950616;i:4;b:0;i:5;a:13:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:5:\"admin\";s:10:\"user_email\";s:20:\"tudor@contursoft.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;s:6:\"is_cli\";b:0;}}', 'no'),
(540, 'jpsq_sync-1534250818.951056-284599-26', 'a:6:{i:0;s:21:\"jetpack_sync_callable\";i:1;a:2:{i:0;s:15:\"is_main_network\";i:1;b:0;}i:2;i:1;i:3;d:1534250818.951023;i:4;b:0;i:5;a:13:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:5:\"admin\";s:10:\"user_email\";s:20:\"tudor@contursoft.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;s:6:\"is_cli\";b:0;}}', 'no'),
(541, 'jpsq_sync-1534250818.951362-284599-27', 'a:6:{i:0;s:21:\"jetpack_sync_callable\";i:1;a:2:{i:0;s:13:\"is_multi_site\";i:1;b:0;}i:2;i:1;i:3;d:1534250818.951334;i:4;b:0;i:5;a:13:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:5:\"admin\";s:10:\"user_email\";s:20:\"tudor@contursoft.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;s:6:\"is_cli\";b:0;}}', 'no'),
(542, 'jpsq_sync-1534250818.951827-284599-28', 'a:6:{i:0;s:21:\"jetpack_sync_callable\";i:1;a:2:{i:0;s:17:\"main_network_site\";i:1;s:37:\"https://cndeveloper.loc\";}i:2;i:1;i:3;d:1534250818.9518;i:4;b:0;i:5;a:13:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:5:\"admin\";s:10:\"user_email\";s:20:\"tudor@contursoft.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;s:6:\"is_cli\";b:0;}}', 'no'),
(543, 'jpsq_sync-1534250818.952205-284599-29', 'a:6:{i:0;s:21:\"jetpack_sync_callable\";i:1;a:2:{i:0;s:8:\"site_url\";i:1;s:37:\"https://cndeveloper.loc\";}i:2;i:1;i:3;d:1534250818.952104;i:4;b:0;i:5;a:13:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:5:\"admin\";s:10:\"user_email\";s:20:\"tudor@contursoft.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;s:6:\"is_cli\";b:0;}}', 'no'),
(544, 'jpsq_sync-1534250818.952537-284599-30', 'a:6:{i:0;s:21:\"jetpack_sync_callable\";i:1;a:2:{i:0;s:8:\"home_url\";i:1;s:37:\"https://cndeveloper.loc\";}i:2;i:1;i:3;d:1534250818.952506;i:4;b:0;i:5;a:13:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:5:\"admin\";s:10:\"user_email\";s:20:\"tudor@contursoft.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;s:6:\"is_cli\";b:0;}}', 'no'),
(545, 'jpsq_sync-1534250818.952839-284599-31', 'a:6:{i:0;s:21:\"jetpack_sync_callable\";i:1;a:2:{i:0;s:16:\"single_user_site\";i:1;b:0;}i:2;i:1;i:3;d:1534250818.952807;i:4;b:0;i:5;a:13:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:5:\"admin\";s:10:\"user_email\";s:20:\"tudor@contursoft.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;s:6:\"is_cli\";b:0;}}', 'no'),
(546, 'jpsq_sync-1534250818.953162-284599-32', 'a:6:{i:0;s:21:\"jetpack_sync_callable\";i:1;a:2:{i:0;s:7:\"updates\";i:1;a:5:{s:7:\"plugins\";i:0;s:6:\"themes\";i:0;s:9:\"wordpress\";i:0;s:12:\"translations\";i:0;s:5:\"total\";i:0;}}i:2;i:1;i:3;d:1534250818.953131;i:4;b:0;i:5;a:13:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:5:\"admin\";s:10:\"user_email\";s:20:\"tudor@contursoft.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;s:6:\"is_cli\";b:0;}}', 'no'),
(547, 'jpsq_sync-1534250818.953463-284599-33', 'a:6:{i:0;s:21:\"jetpack_sync_callable\";i:1;a:2:{i:0;s:28:\"has_file_system_write_access\";i:1;b:1;}i:2;i:1;i:3;d:1534250818.95343;i:4;b:0;i:5;a:13:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:5:\"admin\";s:10:\"user_email\";s:20:\"tudor@contursoft.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;s:6:\"is_cli\";b:0;}}', 'no'),
(548, 'jpsq_sync-1534250818.953757-284599-34', 'a:6:{i:0;s:21:\"jetpack_sync_callable\";i:1;a:2:{i:0;s:21:\"is_version_controlled\";i:1;b:0;}i:2;i:1;i:3;d:1534250818.953723;i:4;b:0;i:5;a:13:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:5:\"admin\";s:10:\"user_email\";s:20:\"tudor@contursoft.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;s:6:\"is_cli\";b:0;}}', 'no');
INSERT INTO `r8ISTpM09o_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(549, 'jpsq_sync-1534250818.954328-284599-35', 'a:6:{i:0;s:21:\"jetpack_sync_callable\";i:1;a:2:{i:0;s:10:\"taxonomies\";i:1;a:10:{s:8:\"category\";O:8:\"stdClass\":23:{s:4:\"name\";s:8:\"category\";s:5:\"label\";s:10:\"Categories\";s:6:\"labels\";O:8:\"stdClass\":23:{s:4:\"name\";s:10:\"Categories\";s:13:\"singular_name\";s:8:\"Category\";s:12:\"search_items\";s:17:\"Search Categories\";s:13:\"popular_items\";N;s:9:\"all_items\";s:14:\"All Categories\";s:11:\"parent_item\";s:15:\"Parent Category\";s:17:\"parent_item_colon\";s:16:\"Parent Category:\";s:9:\"edit_item\";s:13:\"Edit Category\";s:9:\"view_item\";s:13:\"View Category\";s:11:\"update_item\";s:15:\"Update Category\";s:12:\"add_new_item\";s:16:\"Add New Category\";s:13:\"new_item_name\";s:17:\"New Category Name\";s:26:\"separate_items_with_commas\";N;s:19:\"add_or_remove_items\";N;s:21:\"choose_from_most_used\";N;s:9:\"not_found\";s:20:\"No categories found.\";s:8:\"no_terms\";s:13:\"No categories\";s:21:\"items_list_navigation\";s:26:\"Categories list navigation\";s:10:\"items_list\";s:15:\"Categories list\";s:9:\"most_used\";s:9:\"Most Used\";s:13:\"back_to_items\";s:25:\"&larr; Back to Categories\";s:9:\"menu_name\";s:10:\"Categories\";s:14:\"name_admin_bar\";s:8:\"category\";}s:11:\"description\";s:0:\"\";s:6:\"public\";b:1;s:18:\"publicly_queryable\";b:1;s:12:\"hierarchical\";b:1;s:7:\"show_ui\";b:1;s:12:\"show_in_menu\";b:1;s:17:\"show_in_nav_menus\";b:1;s:13:\"show_tagcloud\";b:1;s:18:\"show_in_quick_edit\";b:1;s:17:\"show_admin_column\";b:1;s:11:\"meta_box_cb\";s:24:\"post_categories_meta_box\";s:11:\"object_type\";a:1:{i:0;s:4:\"post\";}s:3:\"cap\";O:8:\"stdClass\":4:{s:12:\"manage_terms\";s:17:\"manage_categories\";s:10:\"edit_terms\";s:15:\"edit_categories\";s:12:\"delete_terms\";s:17:\"delete_categories\";s:12:\"assign_terms\";s:17:\"assign_categories\";}s:7:\"rewrite\";O:8:\"stdClass\":4:{s:10:\"with_front\";b:1;s:12:\"hierarchical\";b:1;s:7:\"ep_mask\";i:512;s:4:\"slug\";s:8:\"category\";}s:9:\"query_var\";s:13:\"category_name\";s:21:\"update_count_callback\";N;s:12:\"show_in_rest\";b:1;s:9:\"rest_base\";s:10:\"categories\";s:21:\"rest_controller_class\";s:24:\"WP_REST_Terms_Controller\";s:8:\"_builtin\";b:1;}s:8:\"post_tag\";O:8:\"stdClass\":23:{s:4:\"name\";s:8:\"post_tag\";s:5:\"label\";s:4:\"Tags\";s:6:\"labels\";O:8:\"stdClass\":23:{s:4:\"name\";s:4:\"Tags\";s:13:\"singular_name\";s:3:\"Tag\";s:12:\"search_items\";s:11:\"Search Tags\";s:13:\"popular_items\";s:12:\"Popular Tags\";s:9:\"all_items\";s:8:\"All Tags\";s:11:\"parent_item\";N;s:17:\"parent_item_colon\";N;s:9:\"edit_item\";s:8:\"Edit Tag\";s:9:\"view_item\";s:8:\"View Tag\";s:11:\"update_item\";s:10:\"Update Tag\";s:12:\"add_new_item\";s:11:\"Add New Tag\";s:13:\"new_item_name\";s:12:\"New Tag Name\";s:26:\"separate_items_with_commas\";s:25:\"Separate tags with commas\";s:19:\"add_or_remove_items\";s:18:\"Add or remove tags\";s:21:\"choose_from_most_used\";s:30:\"Choose from the most used tags\";s:9:\"not_found\";s:14:\"No tags found.\";s:8:\"no_terms\";s:7:\"No tags\";s:21:\"items_list_navigation\";s:20:\"Tags list navigation\";s:10:\"items_list\";s:9:\"Tags list\";s:9:\"most_used\";s:9:\"Most Used\";s:13:\"back_to_items\";s:19:\"&larr; Back to Tags\";s:9:\"menu_name\";s:4:\"Tags\";s:14:\"name_admin_bar\";s:8:\"post_tag\";}s:11:\"description\";s:0:\"\";s:6:\"public\";b:1;s:18:\"publicly_queryable\";b:1;s:12:\"hierarchical\";b:0;s:7:\"show_ui\";b:1;s:12:\"show_in_menu\";b:1;s:17:\"show_in_nav_menus\";b:1;s:13:\"show_tagcloud\";b:1;s:18:\"show_in_quick_edit\";b:1;s:17:\"show_admin_column\";b:1;s:11:\"meta_box_cb\";s:18:\"post_tags_meta_box\";s:11:\"object_type\";a:1:{i:0;s:4:\"post\";}s:3:\"cap\";O:8:\"stdClass\":4:{s:12:\"manage_terms\";s:16:\"manage_post_tags\";s:10:\"edit_terms\";s:14:\"edit_post_tags\";s:12:\"delete_terms\";s:16:\"delete_post_tags\";s:12:\"assign_terms\";s:16:\"assign_post_tags\";}s:7:\"rewrite\";O:8:\"stdClass\":4:{s:10:\"with_front\";b:1;s:12:\"hierarchical\";b:0;s:7:\"ep_mask\";i:1024;s:4:\"slug\";s:3:\"tag\";}s:9:\"query_var\";s:3:\"tag\";s:21:\"update_count_callback\";N;s:12:\"show_in_rest\";b:1;s:9:\"rest_base\";s:4:\"tags\";s:21:\"rest_controller_class\";s:24:\"WP_REST_Terms_Controller\";s:8:\"_builtin\";b:1;}s:8:\"nav_menu\";O:8:\"stdClass\":23:{s:4:\"name\";s:8:\"nav_menu\";s:5:\"label\";s:16:\"Navigation Menus\";s:6:\"labels\";O:8:\"stdClass\":24:{s:4:\"name\";s:16:\"Navigation Menus\";s:13:\"singular_name\";s:15:\"Navigation Menu\";s:12:\"search_items\";s:11:\"Search Tags\";s:13:\"popular_items\";s:12:\"Popular Tags\";s:9:\"all_items\";s:16:\"Navigation Menus\";s:11:\"parent_item\";N;s:17:\"parent_item_colon\";N;s:9:\"edit_item\";s:8:\"Edit Tag\";s:9:\"view_item\";s:8:\"View Tag\";s:11:\"update_item\";s:10:\"Update Tag\";s:12:\"add_new_item\";s:11:\"Add New Tag\";s:13:\"new_item_name\";s:12:\"New Tag Name\";s:26:\"separate_items_with_commas\";s:25:\"Separate tags with commas\";s:19:\"add_or_remove_items\";s:18:\"Add or remove tags\";s:21:\"choose_from_most_used\";s:30:\"Choose from the most used tags\";s:9:\"not_found\";s:14:\"No tags found.\";s:8:\"no_terms\";s:7:\"No tags\";s:21:\"items_list_navigation\";s:20:\"Tags list navigation\";s:10:\"items_list\";s:9:\"Tags list\";s:9:\"most_used\";s:9:\"Most Used\";s:13:\"back_to_items\";s:19:\"&larr; Back to Tags\";s:9:\"menu_name\";s:16:\"Navigation Menus\";s:14:\"name_admin_bar\";s:15:\"Navigation Menu\";s:8:\"archives\";s:16:\"Navigation Menus\";}s:11:\"description\";s:0:\"\";s:6:\"public\";b:0;s:18:\"publicly_queryable\";b:0;s:12:\"hierarchical\";b:0;s:7:\"show_ui\";b:0;s:12:\"show_in_menu\";b:0;s:17:\"show_in_nav_menus\";b:0;s:13:\"show_tagcloud\";b:0;s:18:\"show_in_quick_edit\";b:0;s:17:\"show_admin_column\";b:0;s:11:\"meta_box_cb\";s:18:\"post_tags_meta_box\";s:11:\"object_type\";a:1:{i:0;s:13:\"nav_menu_item\";}s:3:\"cap\";O:8:\"stdClass\":4:{s:12:\"manage_terms\";s:17:\"manage_categories\";s:10:\"edit_terms\";s:17:\"manage_categories\";s:12:\"delete_terms\";s:17:\"manage_categories\";s:12:\"assign_terms\";s:10:\"edit_posts\";}s:7:\"rewrite\";b:0;s:9:\"query_var\";b:0;s:21:\"update_count_callback\";N;s:12:\"show_in_rest\";b:0;s:9:\"rest_base\";b:0;s:21:\"rest_controller_class\";N;s:8:\"_builtin\";b:1;}s:13:\"link_category\";O:8:\"stdClass\":23:{s:4:\"name\";s:13:\"link_category\";s:5:\"label\";s:15:\"Link Categories\";s:6:\"labels\";O:8:\"stdClass\":24:{s:4:\"name\";s:15:\"Link Categories\";s:13:\"singular_name\";s:13:\"Link Category\";s:12:\"search_items\";s:22:\"Search Link Categories\";s:13:\"popular_items\";N;s:9:\"all_items\";s:19:\"All Link Categories\";s:11:\"parent_item\";N;s:17:\"parent_item_colon\";N;s:9:\"edit_item\";s:18:\"Edit Link Category\";s:9:\"view_item\";s:8:\"View Tag\";s:11:\"update_item\";s:20:\"Update Link Category\";s:12:\"add_new_item\";s:21:\"Add New Link Category\";s:13:\"new_item_name\";s:22:\"New Link Category Name\";s:26:\"separate_items_with_commas\";N;s:19:\"add_or_remove_items\";N;s:21:\"choose_from_most_used\";N;s:9:\"not_found\";s:14:\"No tags found.\";s:8:\"no_terms\";s:7:\"No tags\";s:21:\"items_list_navigation\";s:20:\"Tags list navigation\";s:10:\"items_list\";s:9:\"Tags list\";s:9:\"most_used\";s:9:\"Most Used\";s:13:\"back_to_items\";s:30:\"&larr; Back to Link Categories\";s:9:\"menu_name\";s:15:\"Link Categories\";s:14:\"name_admin_bar\";s:13:\"Link Category\";s:8:\"archives\";s:19:\"All Link Categories\";}s:11:\"description\";s:0:\"\";s:6:\"public\";b:0;s:18:\"publicly_queryable\";b:0;s:12:\"hierarchical\";b:0;s:7:\"show_ui\";b:1;s:12:\"show_in_menu\";b:1;s:17:\"show_in_nav_menus\";b:0;s:13:\"show_tagcloud\";b:1;s:18:\"show_in_quick_edit\";b:1;s:17:\"show_admin_column\";b:0;s:11:\"meta_box_cb\";s:18:\"post_tags_meta_box\";s:11:\"object_type\";a:1:{i:0;s:4:\"link\";}s:3:\"cap\";O:8:\"stdClass\":4:{s:12:\"manage_terms\";s:12:\"manage_links\";s:10:\"edit_terms\";s:12:\"manage_links\";s:12:\"delete_terms\";s:12:\"manage_links\";s:12:\"assign_terms\";s:12:\"manage_links\";}s:7:\"rewrite\";b:0;s:9:\"query_var\";b:0;s:21:\"update_count_callback\";N;s:12:\"show_in_rest\";b:0;s:9:\"rest_base\";b:0;s:21:\"rest_controller_class\";N;s:8:\"_builtin\";b:1;}s:11:\"post_format\";O:8:\"stdClass\":23:{s:4:\"name\";s:11:\"post_format\";s:5:\"label\";s:6:\"Format\";s:6:\"labels\";O:8:\"stdClass\":24:{s:4:\"name\";s:6:\"Format\";s:13:\"singular_name\";s:6:\"Format\";s:12:\"search_items\";s:11:\"Search Tags\";s:13:\"popular_items\";s:12:\"Popular Tags\";s:9:\"all_items\";s:6:\"Format\";s:11:\"parent_item\";N;s:17:\"parent_item_colon\";N;s:9:\"edit_item\";s:8:\"Edit Tag\";s:9:\"view_item\";s:8:\"View Tag\";s:11:\"update_item\";s:10:\"Update Tag\";s:12:\"add_new_item\";s:11:\"Add New Tag\";s:13:\"new_item_name\";s:12:\"New Tag Name\";s:26:\"separate_items_with_commas\";s:25:\"Separate tags with commas\";s:19:\"add_or_remove_items\";s:18:\"Add or remove tags\";s:21:\"choose_from_most_used\";s:30:\"Choose from the most used tags\";s:9:\"not_found\";s:14:\"No tags found.\";s:8:\"no_terms\";s:7:\"No tags\";s:21:\"items_list_navigation\";s:20:\"Tags list navigation\";s:10:\"items_list\";s:9:\"Tags list\";s:9:\"most_used\";s:9:\"Most Used\";s:13:\"back_to_items\";s:19:\"&larr; Back to Tags\";s:9:\"menu_name\";s:6:\"Format\";s:14:\"name_admin_bar\";s:6:\"Format\";s:8:\"archives\";s:6:\"Format\";}s:11:\"description\";s:0:\"\";s:6:\"public\";b:1;s:18:\"publicly_queryable\";b:1;s:12:\"hierarchical\";b:0;s:7:\"show_ui\";b:0;s:12:\"show_in_menu\";b:0;s:17:\"show_in_nav_menus\";b:1;s:13:\"show_tagcloud\";b:0;s:18:\"show_in_quick_edit\";b:0;s:17:\"show_admin_column\";b:0;s:11:\"meta_box_cb\";s:18:\"post_tags_meta_box\";s:11:\"object_type\";a:1:{i:0;s:4:\"post\";}s:3:\"cap\";O:8:\"stdClass\":4:{s:12:\"manage_terms\";s:17:\"manage_categories\";s:10:\"edit_terms\";s:17:\"manage_categories\";s:12:\"delete_terms\";s:17:\"manage_categories\";s:12:\"assign_terms\";s:10:\"edit_posts\";}s:7:\"rewrite\";O:8:\"stdClass\":4:{s:10:\"with_front\";b:1;s:12:\"hierarchical\";b:0;s:7:\"ep_mask\";i:0;s:4:\"slug\";s:4:\"type\";}s:9:\"query_var\";s:11:\"post_format\";s:21:\"update_count_callback\";N;s:12:\"show_in_rest\";b:0;s:9:\"rest_base\";b:0;s:21:\"rest_controller_class\";N;s:8:\"_builtin\";b:1;}s:12:\"product_type\";O:8:\"stdClass\":23:{s:4:\"name\";s:12:\"product_type\";s:5:\"label\";s:4:\"Tags\";s:6:\"labels\";O:8:\"stdClass\":23:{s:4:\"name\";s:4:\"Tags\";s:13:\"singular_name\";s:3:\"Tag\";s:12:\"search_items\";s:11:\"Search Tags\";s:13:\"popular_items\";s:12:\"Popular Tags\";s:9:\"all_items\";s:8:\"All Tags\";s:11:\"parent_item\";N;s:17:\"parent_item_colon\";N;s:9:\"edit_item\";s:8:\"Edit Tag\";s:9:\"view_item\";s:8:\"View Tag\";s:11:\"update_item\";s:10:\"Update Tag\";s:12:\"add_new_item\";s:11:\"Add New Tag\";s:13:\"new_item_name\";s:12:\"New Tag Name\";s:26:\"separate_items_with_commas\";s:25:\"Separate tags with commas\";s:19:\"add_or_remove_items\";s:18:\"Add or remove tags\";s:21:\"choose_from_most_used\";s:30:\"Choose from the most used tags\";s:9:\"not_found\";s:14:\"No tags found.\";s:8:\"no_terms\";s:7:\"No tags\";s:21:\"items_list_navigation\";s:20:\"Tags list navigation\";s:10:\"items_list\";s:9:\"Tags list\";s:9:\"most_used\";s:9:\"Most Used\";s:13:\"back_to_items\";s:19:\"&larr; Back to Tags\";s:9:\"menu_name\";s:4:\"Tags\";s:14:\"name_admin_bar\";s:12:\"product_type\";}s:11:\"description\";s:0:\"\";s:6:\"public\";b:0;s:18:\"publicly_queryable\";b:0;s:12:\"hierarchical\";b:0;s:7:\"show_ui\";b:0;s:12:\"show_in_menu\";b:0;s:17:\"show_in_nav_menus\";b:0;s:13:\"show_tagcloud\";b:0;s:18:\"show_in_quick_edit\";b:0;s:17:\"show_admin_column\";b:0;s:11:\"meta_box_cb\";s:18:\"post_tags_meta_box\";s:11:\"object_type\";a:1:{i:0;s:7:\"product\";}s:3:\"cap\";O:8:\"stdClass\":4:{s:12:\"manage_terms\";s:17:\"manage_categories\";s:10:\"edit_terms\";s:17:\"manage_categories\";s:12:\"delete_terms\";s:17:\"manage_categories\";s:12:\"assign_terms\";s:10:\"edit_posts\";}s:7:\"rewrite\";b:0;s:9:\"query_var\";s:12:\"product_type\";s:21:\"update_count_callback\";N;s:12:\"show_in_rest\";b:0;s:9:\"rest_base\";b:0;s:21:\"rest_controller_class\";N;s:8:\"_builtin\";b:0;}s:18:\"product_visibility\";O:8:\"stdClass\":23:{s:4:\"name\";s:18:\"product_visibility\";s:5:\"label\";s:4:\"Tags\";s:6:\"labels\";O:8:\"stdClass\":23:{s:4:\"name\";s:4:\"Tags\";s:13:\"singular_name\";s:3:\"Tag\";s:12:\"search_items\";s:11:\"Search Tags\";s:13:\"popular_items\";s:12:\"Popular Tags\";s:9:\"all_items\";s:8:\"All Tags\";s:11:\"parent_item\";N;s:17:\"parent_item_colon\";N;s:9:\"edit_item\";s:8:\"Edit Tag\";s:9:\"view_item\";s:8:\"View Tag\";s:11:\"update_item\";s:10:\"Update Tag\";s:12:\"add_new_item\";s:11:\"Add New Tag\";s:13:\"new_item_name\";s:12:\"New Tag Name\";s:26:\"separate_items_with_commas\";s:25:\"Separate tags with commas\";s:19:\"add_or_remove_items\";s:18:\"Add or remove tags\";s:21:\"choose_from_most_used\";s:30:\"Choose from the most used tags\";s:9:\"not_found\";s:14:\"No tags found.\";s:8:\"no_terms\";s:7:\"No tags\";s:21:\"items_list_navigation\";s:20:\"Tags list navigation\";s:10:\"items_list\";s:9:\"Tags list\";s:9:\"most_used\";s:9:\"Most Used\";s:13:\"back_to_items\";s:19:\"&larr; Back to Tags\";s:9:\"menu_name\";s:4:\"Tags\";s:14:\"name_admin_bar\";s:18:\"product_visibility\";}s:11:\"description\";s:0:\"\";s:6:\"public\";b:0;s:18:\"publicly_queryable\";b:0;s:12:\"hierarchical\";b:0;s:7:\"show_ui\";b:0;s:12:\"show_in_menu\";b:0;s:17:\"show_in_nav_menus\";b:0;s:13:\"show_tagcloud\";b:0;s:18:\"show_in_quick_edit\";b:0;s:17:\"show_admin_column\";b:0;s:11:\"meta_box_cb\";s:18:\"post_tags_meta_box\";s:11:\"object_type\";a:2:{i:0;s:7:\"product\";i:1;s:17:\"product_variation\";}s:3:\"cap\";O:8:\"stdClass\":4:{s:12:\"manage_terms\";s:17:\"manage_categories\";s:10:\"edit_terms\";s:17:\"manage_categories\";s:12:\"delete_terms\";s:17:\"manage_categories\";s:12:\"assign_terms\";s:10:\"edit_posts\";}s:7:\"rewrite\";b:0;s:9:\"query_var\";s:18:\"product_visibility\";s:21:\"update_count_callback\";N;s:12:\"show_in_rest\";b:0;s:9:\"rest_base\";b:0;s:21:\"rest_controller_class\";N;s:8:\"_builtin\";b:0;}s:11:\"product_cat\";O:8:\"stdClass\":23:{s:4:\"name\";s:11:\"product_cat\";s:5:\"label\";s:18:\"Product categories\";s:6:\"labels\";O:8:\"stdClass\":24:{s:4:\"name\";s:18:\"Product categories\";s:13:\"singular_name\";s:8:\"Category\";s:12:\"search_items\";s:17:\"Search categories\";s:13:\"popular_items\";N;s:9:\"all_items\";s:14:\"All categories\";s:11:\"parent_item\";s:15:\"Parent category\";s:17:\"parent_item_colon\";s:16:\"Parent category:\";s:9:\"edit_item\";s:13:\"Edit category\";s:9:\"view_item\";s:13:\"View Category\";s:11:\"update_item\";s:15:\"Update category\";s:12:\"add_new_item\";s:16:\"Add new category\";s:13:\"new_item_name\";s:17:\"New category name\";s:26:\"separate_items_with_commas\";N;s:19:\"add_or_remove_items\";N;s:21:\"choose_from_most_used\";N;s:9:\"not_found\";s:19:\"No categories found\";s:8:\"no_terms\";s:13:\"No categories\";s:21:\"items_list_navigation\";s:26:\"Categories list navigation\";s:10:\"items_list\";s:15:\"Categories list\";s:9:\"most_used\";s:9:\"Most Used\";s:13:\"back_to_items\";s:25:\"&larr; Back to Categories\";s:9:\"menu_name\";s:10:\"Categories\";s:14:\"name_admin_bar\";s:8:\"Category\";s:8:\"archives\";s:14:\"All categories\";}s:11:\"description\";s:0:\"\";s:6:\"public\";b:1;s:18:\"publicly_queryable\";b:1;s:12:\"hierarchical\";b:1;s:7:\"show_ui\";b:1;s:12:\"show_in_menu\";b:1;s:17:\"show_in_nav_menus\";b:1;s:13:\"show_tagcloud\";b:1;s:18:\"show_in_quick_edit\";b:1;s:17:\"show_admin_column\";b:0;s:11:\"meta_box_cb\";s:24:\"post_categories_meta_box\";s:11:\"object_type\";a:1:{i:0;s:7:\"product\";}s:3:\"cap\";O:8:\"stdClass\":4:{s:12:\"manage_terms\";s:20:\"manage_product_terms\";s:10:\"edit_terms\";s:18:\"edit_product_terms\";s:12:\"delete_terms\";s:20:\"delete_product_terms\";s:12:\"assign_terms\";s:20:\"assign_product_terms\";}s:7:\"rewrite\";O:8:\"stdClass\":4:{s:10:\"with_front\";b:0;s:12:\"hierarchical\";b:1;s:7:\"ep_mask\";i:0;s:4:\"slug\";s:16:\"product-category\";}s:9:\"query_var\";s:11:\"product_cat\";s:21:\"update_count_callback\";N;s:12:\"show_in_rest\";b:0;s:9:\"rest_base\";b:0;s:21:\"rest_controller_class\";N;s:8:\"_builtin\";b:0;}s:11:\"product_tag\";O:8:\"stdClass\":23:{s:4:\"name\";s:11:\"product_tag\";s:5:\"label\";s:12:\"Product tags\";s:6:\"labels\";O:8:\"stdClass\":24:{s:4:\"name\";s:12:\"Product tags\";s:13:\"singular_name\";s:3:\"Tag\";s:12:\"search_items\";s:11:\"Search tags\";s:13:\"popular_items\";s:12:\"Popular tags\";s:9:\"all_items\";s:8:\"All tags\";s:11:\"parent_item\";N;s:17:\"parent_item_colon\";N;s:9:\"edit_item\";s:8:\"Edit tag\";s:9:\"view_item\";s:8:\"View Tag\";s:11:\"update_item\";s:10:\"Update tag\";s:12:\"add_new_item\";s:11:\"Add new tag\";s:13:\"new_item_name\";s:12:\"New tag name\";s:26:\"separate_items_with_commas\";s:25:\"Separate tags with commas\";s:19:\"add_or_remove_items\";s:18:\"Add or remove tags\";s:21:\"choose_from_most_used\";s:30:\"Choose from the most used tags\";s:9:\"not_found\";s:13:\"No tags found\";s:8:\"no_terms\";s:7:\"No tags\";s:21:\"items_list_navigation\";s:20:\"Tags list navigation\";s:10:\"items_list\";s:9:\"Tags list\";s:9:\"most_used\";s:9:\"Most Used\";s:13:\"back_to_items\";s:19:\"&larr; Back to Tags\";s:9:\"menu_name\";s:4:\"Tags\";s:14:\"name_admin_bar\";s:3:\"Tag\";s:8:\"archives\";s:8:\"All tags\";}s:11:\"description\";s:0:\"\";s:6:\"public\";b:1;s:18:\"publicly_queryable\";b:1;s:12:\"hierarchical\";b:0;s:7:\"show_ui\";b:1;s:12:\"show_in_menu\";b:1;s:17:\"show_in_nav_menus\";b:1;s:13:\"show_tagcloud\";b:1;s:18:\"show_in_quick_edit\";b:1;s:17:\"show_admin_column\";b:0;s:11:\"meta_box_cb\";s:18:\"post_tags_meta_box\";s:11:\"object_type\";a:1:{i:0;s:7:\"product\";}s:3:\"cap\";O:8:\"stdClass\":4:{s:12:\"manage_terms\";s:20:\"manage_product_terms\";s:10:\"edit_terms\";s:18:\"edit_product_terms\";s:12:\"delete_terms\";s:20:\"delete_product_terms\";s:12:\"assign_terms\";s:20:\"assign_product_terms\";}s:7:\"rewrite\";O:8:\"stdClass\":4:{s:10:\"with_front\";b:0;s:12:\"hierarchical\";b:0;s:7:\"ep_mask\";i:0;s:4:\"slug\";s:11:\"product-tag\";}s:9:\"query_var\";s:11:\"product_tag\";s:21:\"update_count_callback\";N;s:12:\"show_in_rest\";b:0;s:9:\"rest_base\";b:0;s:21:\"rest_controller_class\";N;s:8:\"_builtin\";b:0;}s:22:\"product_shipping_class\";O:8:\"stdClass\":23:{s:4:\"name\";s:22:\"product_shipping_class\";s:5:\"label\";s:24:\"Product shipping classes\";s:6:\"labels\";O:8:\"stdClass\":24:{s:4:\"name\";s:24:\"Product shipping classes\";s:13:\"singular_name\";s:14:\"Shipping class\";s:12:\"search_items\";s:23:\"Search shipping classes\";s:13:\"popular_items\";s:12:\"Popular Tags\";s:9:\"all_items\";s:20:\"All shipping classes\";s:11:\"parent_item\";s:21:\"Parent shipping class\";s:17:\"parent_item_colon\";s:22:\"Parent shipping class:\";s:9:\"edit_item\";s:19:\"Edit shipping class\";s:9:\"view_item\";s:8:\"View Tag\";s:11:\"update_item\";s:21:\"Update shipping class\";s:12:\"add_new_item\";s:22:\"Add new shipping class\";s:13:\"new_item_name\";s:23:\"New shipping class Name\";s:26:\"separate_items_with_commas\";s:25:\"Separate tags with commas\";s:19:\"add_or_remove_items\";s:18:\"Add or remove tags\";s:21:\"choose_from_most_used\";s:30:\"Choose from the most used tags\";s:9:\"not_found\";s:14:\"No tags found.\";s:8:\"no_terms\";s:7:\"No tags\";s:21:\"items_list_navigation\";s:20:\"Tags list navigation\";s:10:\"items_list\";s:9:\"Tags list\";s:9:\"most_used\";s:9:\"Most Used\";s:13:\"back_to_items\";s:19:\"&larr; Back to Tags\";s:9:\"menu_name\";s:16:\"Shipping classes\";s:14:\"name_admin_bar\";s:14:\"Shipping class\";s:8:\"archives\";s:20:\"All shipping classes\";}s:11:\"description\";s:0:\"\";s:6:\"public\";b:1;s:18:\"publicly_queryable\";b:1;s:12:\"hierarchical\";b:0;s:7:\"show_ui\";b:0;s:12:\"show_in_menu\";b:0;s:17:\"show_in_nav_menus\";b:0;s:13:\"show_tagcloud\";b:0;s:18:\"show_in_quick_edit\";b:0;s:17:\"show_admin_column\";b:0;s:11:\"meta_box_cb\";s:18:\"post_tags_meta_box\";s:11:\"object_type\";a:2:{i:0;s:7:\"product\";i:1;s:17:\"product_variation\";}s:3:\"cap\";O:8:\"stdClass\":4:{s:12:\"manage_terms\";s:20:\"manage_product_terms\";s:10:\"edit_terms\";s:18:\"edit_product_terms\";s:12:\"delete_terms\";s:20:\"delete_product_terms\";s:12:\"assign_terms\";s:20:\"assign_product_terms\";}s:7:\"rewrite\";b:0;s:9:\"query_var\";s:22:\"product_shipping_class\";s:21:\"update_count_callback\";N;s:12:\"show_in_rest\";b:0;s:9:\"rest_base\";b:0;s:21:\"rest_controller_class\";N;s:8:\"_builtin\";b:0;}}}i:2;i:1;i:3;d:1534250818.954298;i:4;b:0;i:5;a:13:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:5:\"admin\";s:10:\"user_email\";s:20:\"tudor@contursoft.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;s:6:\"is_cli\";b:0;}}', 'no');
INSERT INTO `r8ISTpM09o_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(550, 'jpsq_sync-1534250818.955930-284599-36', 'a:6:{i:0;s:21:\"jetpack_sync_callable\";i:1;a:2:{i:0;s:10:\"post_types\";i:1;a:14:{s:4:\"post\";O:8:\"stdClass\":26:{s:4:\"name\";s:4:\"post\";s:5:\"label\";s:5:\"Posts\";s:6:\"labels\";O:8:\"stdClass\":26:{s:4:\"name\";s:5:\"Posts\";s:13:\"singular_name\";s:4:\"Post\";s:7:\"add_new\";s:7:\"Add New\";s:12:\"add_new_item\";s:12:\"Add New Post\";s:9:\"edit_item\";s:9:\"Edit Post\";s:8:\"new_item\";s:8:\"New Post\";s:9:\"view_item\";s:9:\"View Post\";s:10:\"view_items\";s:10:\"View Posts\";s:12:\"search_items\";s:12:\"Search Posts\";s:9:\"not_found\";s:15:\"No posts found.\";s:18:\"not_found_in_trash\";s:24:\"No posts found in Trash.\";s:17:\"parent_item_colon\";N;s:9:\"all_items\";s:9:\"All Posts\";s:8:\"archives\";s:13:\"Post Archives\";s:10:\"attributes\";s:15:\"Post Attributes\";s:16:\"insert_into_item\";s:16:\"Insert into post\";s:21:\"uploaded_to_this_item\";s:21:\"Uploaded to this post\";s:14:\"featured_image\";s:14:\"Featured Image\";s:18:\"set_featured_image\";s:18:\"Set featured image\";s:21:\"remove_featured_image\";s:21:\"Remove featured image\";s:18:\"use_featured_image\";s:21:\"Use as featured image\";s:17:\"filter_items_list\";s:17:\"Filter posts list\";s:21:\"items_list_navigation\";s:21:\"Posts list navigation\";s:10:\"items_list\";s:10:\"Posts list\";s:9:\"menu_name\";s:5:\"Posts\";s:14:\"name_admin_bar\";s:4:\"Post\";}s:11:\"description\";s:0:\"\";s:6:\"public\";b:1;s:12:\"hierarchical\";b:0;s:19:\"exclude_from_search\";b:0;s:18:\"publicly_queryable\";b:1;s:7:\"show_ui\";b:1;s:12:\"show_in_menu\";b:1;s:17:\"show_in_nav_menus\";b:1;s:17:\"show_in_admin_bar\";b:1;s:13:\"menu_position\";i:5;s:15:\"capability_type\";s:4:\"post\";s:3:\"cap\";O:8:\"stdClass\":15:{s:9:\"edit_post\";s:9:\"edit_post\";s:9:\"read_post\";s:9:\"read_post\";s:11:\"delete_post\";s:11:\"delete_post\";s:10:\"edit_posts\";s:10:\"edit_posts\";s:17:\"edit_others_posts\";s:17:\"edit_others_posts\";s:13:\"publish_posts\";s:13:\"publish_posts\";s:18:\"read_private_posts\";s:18:\"read_private_posts\";s:4:\"read\";s:4:\"read\";s:12:\"delete_posts\";s:12:\"delete_posts\";s:20:\"delete_private_posts\";s:20:\"delete_private_posts\";s:22:\"delete_published_posts\";s:22:\"delete_published_posts\";s:19:\"delete_others_posts\";s:19:\"delete_others_posts\";s:18:\"edit_private_posts\";s:18:\"edit_private_posts\";s:20:\"edit_published_posts\";s:20:\"edit_published_posts\";s:12:\"create_posts\";s:10:\"edit_posts\";}s:12:\"map_meta_cap\";b:1;s:10:\"taxonomies\";a:0:{}s:11:\"has_archive\";b:0;s:7:\"rewrite\";b:0;s:9:\"query_var\";b:0;s:10:\"can_export\";b:1;s:16:\"delete_with_user\";b:1;s:12:\"show_in_rest\";b:1;s:9:\"rest_base\";s:5:\"posts\";s:8:\"_builtin\";b:1;s:10:\"_edit_link\";s:16:\"post.php?post=%d\";}s:4:\"page\";O:8:\"stdClass\":26:{s:4:\"name\";s:4:\"page\";s:5:\"label\";s:5:\"Pages\";s:6:\"labels\";O:8:\"stdClass\":26:{s:4:\"name\";s:5:\"Pages\";s:13:\"singular_name\";s:4:\"Page\";s:7:\"add_new\";s:7:\"Add New\";s:12:\"add_new_item\";s:12:\"Add New Page\";s:9:\"edit_item\";s:9:\"Edit Page\";s:8:\"new_item\";s:8:\"New Page\";s:9:\"view_item\";s:9:\"View Page\";s:10:\"view_items\";s:10:\"View Pages\";s:12:\"search_items\";s:12:\"Search Pages\";s:9:\"not_found\";s:15:\"No pages found.\";s:18:\"not_found_in_trash\";s:24:\"No pages found in Trash.\";s:17:\"parent_item_colon\";s:12:\"Parent Page:\";s:9:\"all_items\";s:9:\"All Pages\";s:8:\"archives\";s:13:\"Page Archives\";s:10:\"attributes\";s:15:\"Page Attributes\";s:16:\"insert_into_item\";s:16:\"Insert into page\";s:21:\"uploaded_to_this_item\";s:21:\"Uploaded to this page\";s:14:\"featured_image\";s:14:\"Featured Image\";s:18:\"set_featured_image\";s:18:\"Set featured image\";s:21:\"remove_featured_image\";s:21:\"Remove featured image\";s:18:\"use_featured_image\";s:21:\"Use as featured image\";s:17:\"filter_items_list\";s:17:\"Filter pages list\";s:21:\"items_list_navigation\";s:21:\"Pages list navigation\";s:10:\"items_list\";s:10:\"Pages list\";s:9:\"menu_name\";s:5:\"Pages\";s:14:\"name_admin_bar\";s:4:\"Page\";}s:11:\"description\";s:0:\"\";s:6:\"public\";b:1;s:12:\"hierarchical\";b:1;s:19:\"exclude_from_search\";b:0;s:18:\"publicly_queryable\";b:0;s:7:\"show_ui\";b:1;s:12:\"show_in_menu\";b:1;s:17:\"show_in_nav_menus\";b:1;s:17:\"show_in_admin_bar\";b:1;s:13:\"menu_position\";i:20;s:15:\"capability_type\";s:4:\"page\";s:3:\"cap\";O:8:\"stdClass\":15:{s:9:\"edit_post\";s:9:\"edit_page\";s:9:\"read_post\";s:9:\"read_page\";s:11:\"delete_post\";s:11:\"delete_page\";s:10:\"edit_posts\";s:10:\"edit_pages\";s:17:\"edit_others_posts\";s:17:\"edit_others_pages\";s:13:\"publish_posts\";s:13:\"publish_pages\";s:18:\"read_private_posts\";s:18:\"read_private_pages\";s:4:\"read\";s:4:\"read\";s:12:\"delete_posts\";s:12:\"delete_pages\";s:20:\"delete_private_posts\";s:20:\"delete_private_pages\";s:22:\"delete_published_posts\";s:22:\"delete_published_pages\";s:19:\"delete_others_posts\";s:19:\"delete_others_pages\";s:18:\"edit_private_posts\";s:18:\"edit_private_pages\";s:20:\"edit_published_posts\";s:20:\"edit_published_pages\";s:12:\"create_posts\";s:10:\"edit_pages\";}s:12:\"map_meta_cap\";b:1;s:10:\"taxonomies\";a:0:{}s:11:\"has_archive\";b:0;s:7:\"rewrite\";b:0;s:9:\"query_var\";b:0;s:10:\"can_export\";b:1;s:16:\"delete_with_user\";b:1;s:12:\"show_in_rest\";b:1;s:9:\"rest_base\";s:5:\"pages\";s:8:\"_builtin\";b:1;s:10:\"_edit_link\";s:16:\"post.php?post=%d\";}s:10:\"attachment\";O:8:\"stdClass\":25:{s:4:\"name\";s:10:\"attachment\";s:5:\"label\";s:5:\"Media\";s:6:\"labels\";O:8:\"stdClass\":26:{s:4:\"name\";s:5:\"Media\";s:13:\"singular_name\";s:5:\"Media\";s:7:\"add_new\";s:7:\"Add New\";s:12:\"add_new_item\";s:12:\"Add New Post\";s:9:\"edit_item\";s:10:\"Edit Media\";s:8:\"new_item\";s:8:\"New Post\";s:9:\"view_item\";s:20:\"View Attachment Page\";s:10:\"view_items\";s:10:\"View Posts\";s:12:\"search_items\";s:12:\"Search Posts\";s:9:\"not_found\";s:15:\"No posts found.\";s:18:\"not_found_in_trash\";s:24:\"No posts found in Trash.\";s:17:\"parent_item_colon\";N;s:9:\"all_items\";s:5:\"Media\";s:8:\"archives\";s:5:\"Media\";s:10:\"attributes\";s:21:\"Attachment Attributes\";s:16:\"insert_into_item\";s:16:\"Insert into post\";s:21:\"uploaded_to_this_item\";s:21:\"Uploaded to this post\";s:14:\"featured_image\";s:14:\"Featured Image\";s:18:\"set_featured_image\";s:18:\"Set featured image\";s:21:\"remove_featured_image\";s:21:\"Remove featured image\";s:18:\"use_featured_image\";s:21:\"Use as featured image\";s:17:\"filter_items_list\";s:17:\"Filter posts list\";s:21:\"items_list_navigation\";s:21:\"Posts list navigation\";s:10:\"items_list\";s:10:\"Posts list\";s:9:\"menu_name\";s:5:\"Media\";s:14:\"name_admin_bar\";s:5:\"Media\";}s:11:\"description\";s:0:\"\";s:6:\"public\";b:1;s:12:\"hierarchical\";b:0;s:19:\"exclude_from_search\";b:0;s:18:\"publicly_queryable\";b:1;s:7:\"show_ui\";b:1;s:12:\"show_in_menu\";b:1;s:17:\"show_in_nav_menus\";b:0;s:17:\"show_in_admin_bar\";b:1;s:15:\"capability_type\";s:4:\"post\";s:3:\"cap\";O:8:\"stdClass\":15:{s:9:\"edit_post\";s:9:\"edit_post\";s:9:\"read_post\";s:9:\"read_post\";s:11:\"delete_post\";s:11:\"delete_post\";s:10:\"edit_posts\";s:10:\"edit_posts\";s:17:\"edit_others_posts\";s:17:\"edit_others_posts\";s:13:\"publish_posts\";s:13:\"publish_posts\";s:18:\"read_private_posts\";s:18:\"read_private_posts\";s:4:\"read\";s:4:\"read\";s:12:\"delete_posts\";s:12:\"delete_posts\";s:20:\"delete_private_posts\";s:20:\"delete_private_posts\";s:22:\"delete_published_posts\";s:22:\"delete_published_posts\";s:19:\"delete_others_posts\";s:19:\"delete_others_posts\";s:18:\"edit_private_posts\";s:18:\"edit_private_posts\";s:20:\"edit_published_posts\";s:20:\"edit_published_posts\";s:12:\"create_posts\";s:12:\"upload_files\";}s:12:\"map_meta_cap\";b:1;s:10:\"taxonomies\";a:0:{}s:11:\"has_archive\";b:0;s:7:\"rewrite\";b:0;s:9:\"query_var\";b:0;s:10:\"can_export\";b:1;s:16:\"delete_with_user\";b:1;s:12:\"show_in_rest\";b:1;s:9:\"rest_base\";s:5:\"media\";s:8:\"_builtin\";b:1;s:10:\"_edit_link\";s:16:\"post.php?post=%d\";}s:8:\"revision\";O:8:\"stdClass\":25:{s:4:\"name\";s:8:\"revision\";s:5:\"label\";s:9:\"Revisions\";s:6:\"labels\";O:8:\"stdClass\":26:{s:4:\"name\";s:9:\"Revisions\";s:13:\"singular_name\";s:8:\"Revision\";s:7:\"add_new\";s:7:\"Add New\";s:12:\"add_new_item\";s:12:\"Add New Post\";s:9:\"edit_item\";s:9:\"Edit Post\";s:8:\"new_item\";s:8:\"New Post\";s:9:\"view_item\";s:9:\"View Post\";s:10:\"view_items\";s:10:\"View Posts\";s:12:\"search_items\";s:12:\"Search Posts\";s:9:\"not_found\";s:15:\"No posts found.\";s:18:\"not_found_in_trash\";s:24:\"No posts found in Trash.\";s:17:\"parent_item_colon\";N;s:9:\"all_items\";s:9:\"Revisions\";s:8:\"archives\";s:9:\"Revisions\";s:10:\"attributes\";s:15:\"Post Attributes\";s:16:\"insert_into_item\";s:16:\"Insert into post\";s:21:\"uploaded_to_this_item\";s:21:\"Uploaded to this post\";s:14:\"featured_image\";s:14:\"Featured Image\";s:18:\"set_featured_image\";s:18:\"Set featured image\";s:21:\"remove_featured_image\";s:21:\"Remove featured image\";s:18:\"use_featured_image\";s:21:\"Use as featured image\";s:17:\"filter_items_list\";s:17:\"Filter posts list\";s:21:\"items_list_navigation\";s:21:\"Posts list navigation\";s:10:\"items_list\";s:10:\"Posts list\";s:9:\"menu_name\";s:9:\"Revisions\";s:14:\"name_admin_bar\";s:8:\"Revision\";}s:11:\"description\";s:0:\"\";s:6:\"public\";b:0;s:12:\"hierarchical\";b:0;s:19:\"exclude_from_search\";b:1;s:18:\"publicly_queryable\";b:0;s:7:\"show_ui\";b:0;s:12:\"show_in_menu\";b:0;s:17:\"show_in_nav_menus\";b:0;s:17:\"show_in_admin_bar\";b:0;s:15:\"capability_type\";s:4:\"post\";s:3:\"cap\";O:8:\"stdClass\":15:{s:9:\"edit_post\";s:9:\"edit_post\";s:9:\"read_post\";s:9:\"read_post\";s:11:\"delete_post\";s:11:\"delete_post\";s:10:\"edit_posts\";s:10:\"edit_posts\";s:17:\"edit_others_posts\";s:17:\"edit_others_posts\";s:13:\"publish_posts\";s:13:\"publish_posts\";s:18:\"read_private_posts\";s:18:\"read_private_posts\";s:4:\"read\";s:4:\"read\";s:12:\"delete_posts\";s:12:\"delete_posts\";s:20:\"delete_private_posts\";s:20:\"delete_private_posts\";s:22:\"delete_published_posts\";s:22:\"delete_published_posts\";s:19:\"delete_others_posts\";s:19:\"delete_others_posts\";s:18:\"edit_private_posts\";s:18:\"edit_private_posts\";s:20:\"edit_published_posts\";s:20:\"edit_published_posts\";s:12:\"create_posts\";s:10:\"edit_posts\";}s:12:\"map_meta_cap\";b:1;s:10:\"taxonomies\";a:0:{}s:11:\"has_archive\";b:0;s:7:\"rewrite\";b:0;s:9:\"query_var\";b:0;s:10:\"can_export\";b:0;s:16:\"delete_with_user\";b:1;s:12:\"show_in_rest\";b:0;s:9:\"rest_base\";b:0;s:8:\"_builtin\";b:1;s:10:\"_edit_link\";s:24:\"revision.php?revision=%d\";}s:13:\"nav_menu_item\";O:8:\"stdClass\":26:{s:4:\"name\";s:13:\"nav_menu_item\";s:5:\"label\";s:21:\"Navigation Menu Items\";s:6:\"labels\";O:8:\"stdClass\":26:{s:4:\"name\";s:21:\"Navigation Menu Items\";s:13:\"singular_name\";s:20:\"Navigation Menu Item\";s:7:\"add_new\";s:7:\"Add New\";s:12:\"add_new_item\";s:12:\"Add New Post\";s:9:\"edit_item\";s:9:\"Edit Post\";s:8:\"new_item\";s:8:\"New Post\";s:9:\"view_item\";s:9:\"View Post\";s:10:\"view_items\";s:10:\"View Posts\";s:12:\"search_items\";s:12:\"Search Posts\";s:9:\"not_found\";s:15:\"No posts found.\";s:18:\"not_found_in_trash\";s:24:\"No posts found in Trash.\";s:17:\"parent_item_colon\";N;s:9:\"all_items\";s:21:\"Navigation Menu Items\";s:8:\"archives\";s:21:\"Navigation Menu Items\";s:10:\"attributes\";s:15:\"Post Attributes\";s:16:\"insert_into_item\";s:16:\"Insert into post\";s:21:\"uploaded_to_this_item\";s:21:\"Uploaded to this post\";s:14:\"featured_image\";s:14:\"Featured Image\";s:18:\"set_featured_image\";s:18:\"Set featured image\";s:21:\"remove_featured_image\";s:21:\"Remove featured image\";s:18:\"use_featured_image\";s:21:\"Use as featured image\";s:17:\"filter_items_list\";s:17:\"Filter posts list\";s:21:\"items_list_navigation\";s:21:\"Posts list navigation\";s:10:\"items_list\";s:10:\"Posts list\";s:9:\"menu_name\";s:21:\"Navigation Menu Items\";s:14:\"name_admin_bar\";s:20:\"Navigation Menu Item\";}s:11:\"description\";s:0:\"\";s:6:\"public\";b:0;s:12:\"hierarchical\";b:0;s:19:\"exclude_from_search\";b:1;s:18:\"publicly_queryable\";b:0;s:7:\"show_ui\";b:0;s:12:\"show_in_menu\";b:0;s:17:\"show_in_nav_menus\";b:0;s:17:\"show_in_admin_bar\";b:0;s:8:\"supports\";a:0:{}s:15:\"capability_type\";s:4:\"post\";s:3:\"cap\";O:8:\"stdClass\":15:{s:9:\"edit_post\";s:9:\"edit_post\";s:9:\"read_post\";s:9:\"read_post\";s:11:\"delete_post\";s:11:\"delete_post\";s:10:\"edit_posts\";s:10:\"edit_posts\";s:17:\"edit_others_posts\";s:17:\"edit_others_posts\";s:13:\"publish_posts\";s:13:\"publish_posts\";s:18:\"read_private_posts\";s:18:\"read_private_posts\";s:4:\"read\";s:4:\"read\";s:12:\"delete_posts\";s:12:\"delete_posts\";s:20:\"delete_private_posts\";s:20:\"delete_private_posts\";s:22:\"delete_published_posts\";s:22:\"delete_published_posts\";s:19:\"delete_others_posts\";s:19:\"delete_others_posts\";s:18:\"edit_private_posts\";s:18:\"edit_private_posts\";s:20:\"edit_published_posts\";s:20:\"edit_published_posts\";s:12:\"create_posts\";s:10:\"edit_posts\";}s:12:\"map_meta_cap\";b:1;s:10:\"taxonomies\";a:0:{}s:11:\"has_archive\";b:0;s:7:\"rewrite\";b:0;s:9:\"query_var\";b:0;s:10:\"can_export\";b:1;s:16:\"delete_with_user\";b:0;s:12:\"show_in_rest\";b:0;s:9:\"rest_base\";b:0;s:8:\"_builtin\";b:1;s:10:\"_edit_link\";s:0:\"\";}s:10:\"custom_css\";O:8:\"stdClass\":25:{s:4:\"name\";s:10:\"custom_css\";s:5:\"label\";s:10:\"Custom CSS\";s:6:\"labels\";O:8:\"stdClass\":26:{s:4:\"name\";s:10:\"Custom CSS\";s:13:\"singular_name\";s:10:\"Custom CSS\";s:7:\"add_new\";s:7:\"Add New\";s:12:\"add_new_item\";s:12:\"Add New Post\";s:9:\"edit_item\";s:9:\"Edit Post\";s:8:\"new_item\";s:8:\"New Post\";s:9:\"view_item\";s:9:\"View Post\";s:10:\"view_items\";s:10:\"View Posts\";s:12:\"search_items\";s:12:\"Search Posts\";s:9:\"not_found\";s:15:\"No posts found.\";s:18:\"not_found_in_trash\";s:24:\"No posts found in Trash.\";s:17:\"parent_item_colon\";N;s:9:\"all_items\";s:10:\"Custom CSS\";s:8:\"archives\";s:10:\"Custom CSS\";s:10:\"attributes\";s:15:\"Post Attributes\";s:16:\"insert_into_item\";s:16:\"Insert into post\";s:21:\"uploaded_to_this_item\";s:21:\"Uploaded to this post\";s:14:\"featured_image\";s:14:\"Featured Image\";s:18:\"set_featured_image\";s:18:\"Set featured image\";s:21:\"remove_featured_image\";s:21:\"Remove featured image\";s:18:\"use_featured_image\";s:21:\"Use as featured image\";s:17:\"filter_items_list\";s:17:\"Filter posts list\";s:21:\"items_list_navigation\";s:21:\"Posts list navigation\";s:10:\"items_list\";s:10:\"Posts list\";s:9:\"menu_name\";s:10:\"Custom CSS\";s:14:\"name_admin_bar\";s:10:\"Custom CSS\";}s:11:\"description\";s:0:\"\";s:6:\"public\";b:0;s:12:\"hierarchical\";b:0;s:19:\"exclude_from_search\";b:1;s:18:\"publicly_queryable\";b:0;s:7:\"show_ui\";b:0;s:12:\"show_in_menu\";b:0;s:17:\"show_in_nav_menus\";b:0;s:17:\"show_in_admin_bar\";b:0;s:15:\"capability_type\";s:4:\"post\";s:3:\"cap\";O:8:\"stdClass\":13:{s:9:\"edit_post\";s:8:\"edit_css\";s:9:\"read_post\";s:4:\"read\";s:11:\"delete_post\";s:18:\"edit_theme_options\";s:10:\"edit_posts\";s:8:\"edit_css\";s:17:\"edit_others_posts\";s:8:\"edit_css\";s:13:\"publish_posts\";s:18:\"edit_theme_options\";s:18:\"read_private_posts\";s:4:\"read\";s:12:\"delete_posts\";s:18:\"edit_theme_options\";s:22:\"delete_published_posts\";s:18:\"edit_theme_options\";s:20:\"delete_private_posts\";s:18:\"edit_theme_options\";s:19:\"delete_others_posts\";s:18:\"edit_theme_options\";s:20:\"edit_published_posts\";s:8:\"edit_css\";s:12:\"create_posts\";s:8:\"edit_css\";}s:12:\"map_meta_cap\";b:0;s:10:\"taxonomies\";a:0:{}s:11:\"has_archive\";b:0;s:7:\"rewrite\";b:0;s:9:\"query_var\";b:0;s:10:\"can_export\";b:1;s:16:\"delete_with_user\";b:0;s:12:\"show_in_rest\";b:0;s:9:\"rest_base\";b:0;s:8:\"_builtin\";b:1;s:10:\"_edit_link\";s:0:\"\";}s:19:\"customize_changeset\";O:8:\"stdClass\":25:{s:4:\"name\";s:19:\"customize_changeset\";s:5:\"label\";s:10:\"Changesets\";s:6:\"labels\";O:8:\"stdClass\":26:{s:4:\"name\";s:10:\"Changesets\";s:13:\"singular_name\";s:9:\"Changeset\";s:7:\"add_new\";s:7:\"Add New\";s:12:\"add_new_item\";s:17:\"Add New Changeset\";s:9:\"edit_item\";s:14:\"Edit Changeset\";s:8:\"new_item\";s:13:\"New Changeset\";s:9:\"view_item\";s:14:\"View Changeset\";s:10:\"view_items\";s:10:\"View Posts\";s:12:\"search_items\";s:17:\"Search Changesets\";s:9:\"not_found\";s:20:\"No changesets found.\";s:18:\"not_found_in_trash\";s:29:\"No changesets found in Trash.\";s:17:\"parent_item_colon\";N;s:9:\"all_items\";s:14:\"All Changesets\";s:8:\"archives\";s:14:\"All Changesets\";s:10:\"attributes\";s:15:\"Post Attributes\";s:16:\"insert_into_item\";s:16:\"Insert into post\";s:21:\"uploaded_to_this_item\";s:21:\"Uploaded to this post\";s:14:\"featured_image\";s:14:\"Featured Image\";s:18:\"set_featured_image\";s:18:\"Set featured image\";s:21:\"remove_featured_image\";s:21:\"Remove featured image\";s:18:\"use_featured_image\";s:21:\"Use as featured image\";s:17:\"filter_items_list\";s:17:\"Filter posts list\";s:21:\"items_list_navigation\";s:21:\"Posts list navigation\";s:10:\"items_list\";s:10:\"Posts list\";s:9:\"menu_name\";s:10:\"Changesets\";s:14:\"name_admin_bar\";s:9:\"Changeset\";}s:11:\"description\";s:0:\"\";s:6:\"public\";b:0;s:12:\"hierarchical\";b:0;s:19:\"exclude_from_search\";b:1;s:18:\"publicly_queryable\";b:0;s:7:\"show_ui\";b:0;s:12:\"show_in_menu\";b:0;s:17:\"show_in_nav_menus\";b:0;s:17:\"show_in_admin_bar\";b:0;s:15:\"capability_type\";s:19:\"customize_changeset\";s:3:\"cap\";O:8:\"stdClass\":15:{s:9:\"edit_post\";s:9:\"customize\";s:9:\"read_post\";s:9:\"customize\";s:11:\"delete_post\";s:9:\"customize\";s:10:\"edit_posts\";s:9:\"customize\";s:17:\"edit_others_posts\";s:9:\"customize\";s:13:\"publish_posts\";s:9:\"customize\";s:18:\"read_private_posts\";s:9:\"customize\";s:4:\"read\";s:4:\"read\";s:12:\"delete_posts\";s:9:\"customize\";s:20:\"delete_private_posts\";s:9:\"customize\";s:22:\"delete_published_posts\";s:9:\"customize\";s:19:\"delete_others_posts\";s:9:\"customize\";s:18:\"edit_private_posts\";s:9:\"customize\";s:20:\"edit_published_posts\";s:12:\"do_not_allow\";s:12:\"create_posts\";s:9:\"customize\";}s:12:\"map_meta_cap\";b:1;s:10:\"taxonomies\";a:0:{}s:11:\"has_archive\";b:0;s:7:\"rewrite\";b:0;s:9:\"query_var\";b:0;s:10:\"can_export\";b:0;s:16:\"delete_with_user\";b:0;s:12:\"show_in_rest\";b:0;s:9:\"rest_base\";b:0;s:8:\"_builtin\";b:1;s:10:\"_edit_link\";s:0:\"\";}s:12:\"oembed_cache\";O:8:\"stdClass\":26:{s:4:\"name\";s:12:\"oembed_cache\";s:5:\"label\";s:16:\"oEmbed Responses\";s:6:\"labels\";O:8:\"stdClass\":26:{s:4:\"name\";s:16:\"oEmbed Responses\";s:13:\"singular_name\";s:15:\"oEmbed Response\";s:7:\"add_new\";s:7:\"Add New\";s:12:\"add_new_item\";s:12:\"Add New Post\";s:9:\"edit_item\";s:9:\"Edit Post\";s:8:\"new_item\";s:8:\"New Post\";s:9:\"view_item\";s:9:\"View Post\";s:10:\"view_items\";s:10:\"View Posts\";s:12:\"search_items\";s:12:\"Search Posts\";s:9:\"not_found\";s:15:\"No posts found.\";s:18:\"not_found_in_trash\";s:24:\"No posts found in Trash.\";s:17:\"parent_item_colon\";N;s:9:\"all_items\";s:16:\"oEmbed Responses\";s:8:\"archives\";s:16:\"oEmbed Responses\";s:10:\"attributes\";s:15:\"Post Attributes\";s:16:\"insert_into_item\";s:16:\"Insert into post\";s:21:\"uploaded_to_this_item\";s:21:\"Uploaded to this post\";s:14:\"featured_image\";s:14:\"Featured Image\";s:18:\"set_featured_image\";s:18:\"Set featured image\";s:21:\"remove_featured_image\";s:21:\"Remove featured image\";s:18:\"use_featured_image\";s:21:\"Use as featured image\";s:17:\"filter_items_list\";s:17:\"Filter posts list\";s:21:\"items_list_navigation\";s:21:\"Posts list navigation\";s:10:\"items_list\";s:10:\"Posts list\";s:9:\"menu_name\";s:16:\"oEmbed Responses\";s:14:\"name_admin_bar\";s:15:\"oEmbed Response\";}s:11:\"description\";s:0:\"\";s:6:\"public\";b:0;s:12:\"hierarchical\";b:0;s:19:\"exclude_from_search\";b:1;s:18:\"publicly_queryable\";b:0;s:7:\"show_ui\";b:0;s:12:\"show_in_menu\";b:0;s:17:\"show_in_nav_menus\";b:0;s:17:\"show_in_admin_bar\";b:0;s:8:\"supports\";a:0:{}s:15:\"capability_type\";s:4:\"post\";s:3:\"cap\";O:8:\"stdClass\":15:{s:9:\"edit_post\";s:9:\"edit_post\";s:9:\"read_post\";s:9:\"read_post\";s:11:\"delete_post\";s:11:\"delete_post\";s:10:\"edit_posts\";s:10:\"edit_posts\";s:17:\"edit_others_posts\";s:17:\"edit_others_posts\";s:13:\"publish_posts\";s:13:\"publish_posts\";s:18:\"read_private_posts\";s:18:\"read_private_posts\";s:4:\"read\";s:4:\"read\";s:12:\"delete_posts\";s:12:\"delete_posts\";s:20:\"delete_private_posts\";s:20:\"delete_private_posts\";s:22:\"delete_published_posts\";s:22:\"delete_published_posts\";s:19:\"delete_others_posts\";s:19:\"delete_others_posts\";s:18:\"edit_private_posts\";s:18:\"edit_private_posts\";s:20:\"edit_published_posts\";s:20:\"edit_published_posts\";s:12:\"create_posts\";s:10:\"edit_posts\";}s:12:\"map_meta_cap\";b:1;s:10:\"taxonomies\";a:0:{}s:11:\"has_archive\";b:0;s:7:\"rewrite\";b:0;s:9:\"query_var\";b:0;s:10:\"can_export\";b:0;s:16:\"delete_with_user\";b:0;s:12:\"show_in_rest\";b:0;s:9:\"rest_base\";b:0;s:8:\"_builtin\";b:1;s:10:\"_edit_link\";s:0:\"\";}s:12:\"user_request\";O:8:\"stdClass\":26:{s:4:\"name\";s:12:\"user_request\";s:5:\"label\";s:13:\"User Requests\";s:6:\"labels\";O:8:\"stdClass\":26:{s:4:\"name\";s:13:\"User Requests\";s:13:\"singular_name\";s:12:\"User Request\";s:7:\"add_new\";s:7:\"Add New\";s:12:\"add_new_item\";s:12:\"Add New Post\";s:9:\"edit_item\";s:9:\"Edit Post\";s:8:\"new_item\";s:8:\"New Post\";s:9:\"view_item\";s:9:\"View Post\";s:10:\"view_items\";s:10:\"View Posts\";s:12:\"search_items\";s:12:\"Search Posts\";s:9:\"not_found\";s:15:\"No posts found.\";s:18:\"not_found_in_trash\";s:24:\"No posts found in Trash.\";s:17:\"parent_item_colon\";N;s:9:\"all_items\";s:13:\"User Requests\";s:8:\"archives\";s:13:\"User Requests\";s:10:\"attributes\";s:15:\"Post Attributes\";s:16:\"insert_into_item\";s:16:\"Insert into post\";s:21:\"uploaded_to_this_item\";s:21:\"Uploaded to this post\";s:14:\"featured_image\";s:14:\"Featured Image\";s:18:\"set_featured_image\";s:18:\"Set featured image\";s:21:\"remove_featured_image\";s:21:\"Remove featured image\";s:18:\"use_featured_image\";s:21:\"Use as featured image\";s:17:\"filter_items_list\";s:17:\"Filter posts list\";s:21:\"items_list_navigation\";s:21:\"Posts list navigation\";s:10:\"items_list\";s:10:\"Posts list\";s:9:\"menu_name\";s:13:\"User Requests\";s:14:\"name_admin_bar\";s:12:\"User Request\";}s:11:\"description\";s:0:\"\";s:6:\"public\";b:0;s:12:\"hierarchical\";b:0;s:19:\"exclude_from_search\";b:1;s:18:\"publicly_queryable\";b:0;s:7:\"show_ui\";b:0;s:12:\"show_in_menu\";b:0;s:17:\"show_in_nav_menus\";b:0;s:17:\"show_in_admin_bar\";b:0;s:8:\"supports\";a:0:{}s:15:\"capability_type\";s:4:\"post\";s:3:\"cap\";O:8:\"stdClass\":15:{s:9:\"edit_post\";s:9:\"edit_post\";s:9:\"read_post\";s:9:\"read_post\";s:11:\"delete_post\";s:11:\"delete_post\";s:10:\"edit_posts\";s:10:\"edit_posts\";s:17:\"edit_others_posts\";s:17:\"edit_others_posts\";s:13:\"publish_posts\";s:13:\"publish_posts\";s:18:\"read_private_posts\";s:18:\"read_private_posts\";s:4:\"read\";s:4:\"read\";s:12:\"delete_posts\";s:12:\"delete_posts\";s:20:\"delete_private_posts\";s:20:\"delete_private_posts\";s:22:\"delete_published_posts\";s:22:\"delete_published_posts\";s:19:\"delete_others_posts\";s:19:\"delete_others_posts\";s:18:\"edit_private_posts\";s:18:\"edit_private_posts\";s:20:\"edit_published_posts\";s:20:\"edit_published_posts\";s:12:\"create_posts\";s:10:\"edit_posts\";}s:12:\"map_meta_cap\";b:1;s:10:\"taxonomies\";a:0:{}s:11:\"has_archive\";b:0;s:7:\"rewrite\";b:0;s:9:\"query_var\";b:0;s:10:\"can_export\";b:0;s:16:\"delete_with_user\";b:0;s:12:\"show_in_rest\";b:0;s:9:\"rest_base\";b:0;s:8:\"_builtin\";b:1;s:10:\"_edit_link\";s:0:\"\";}s:7:\"product\";O:8:\"stdClass\":24:{s:4:\"name\";s:7:\"product\";s:5:\"label\";s:8:\"Products\";s:6:\"labels\";O:8:\"stdClass\":28:{s:4:\"name\";s:8:\"Products\";s:13:\"singular_name\";s:7:\"Product\";s:7:\"add_new\";s:7:\"Add New\";s:12:\"add_new_item\";s:15:\"Add new product\";s:9:\"edit_item\";s:12:\"Edit product\";s:8:\"new_item\";s:11:\"New product\";s:9:\"view_item\";s:12:\"View product\";s:10:\"view_items\";s:13:\"View products\";s:12:\"search_items\";s:15:\"Search products\";s:9:\"not_found\";s:17:\"No products found\";s:18:\"not_found_in_trash\";s:26:\"No products found in trash\";s:17:\"parent_item_colon\";N;s:9:\"all_items\";s:12:\"All Products\";s:8:\"archives\";s:12:\"All Products\";s:10:\"attributes\";s:15:\"Post Attributes\";s:16:\"insert_into_item\";s:19:\"Insert into product\";s:21:\"uploaded_to_this_item\";s:24:\"Uploaded to this product\";s:14:\"featured_image\";s:13:\"Product image\";s:18:\"set_featured_image\";s:17:\"Set product image\";s:21:\"remove_featured_image\";s:20:\"Remove product image\";s:18:\"use_featured_image\";s:20:\"Use as product image\";s:17:\"filter_items_list\";s:15:\"Filter products\";s:21:\"items_list_navigation\";s:19:\"Products navigation\";s:10:\"items_list\";s:13:\"Products list\";s:9:\"menu_name\";s:8:\"Products\";s:4:\"edit\";s:4:\"Edit\";s:6:\"parent\";s:14:\"Parent product\";s:14:\"name_admin_bar\";s:7:\"Product\";}s:11:\"description\";s:53:\"This is where you can add new products to your store.\";s:6:\"public\";b:1;s:12:\"hierarchical\";b:0;s:19:\"exclude_from_search\";b:0;s:18:\"publicly_queryable\";b:1;s:7:\"show_ui\";b:1;s:12:\"show_in_menu\";b:1;s:17:\"show_in_nav_menus\";b:1;s:17:\"show_in_admin_bar\";b:1;s:15:\"capability_type\";s:7:\"product\";s:3:\"cap\";O:8:\"stdClass\":15:{s:9:\"edit_post\";s:12:\"edit_product\";s:9:\"read_post\";s:12:\"read_product\";s:11:\"delete_post\";s:14:\"delete_product\";s:10:\"edit_posts\";s:13:\"edit_products\";s:17:\"edit_others_posts\";s:20:\"edit_others_products\";s:13:\"publish_posts\";s:16:\"publish_products\";s:18:\"read_private_posts\";s:21:\"read_private_products\";s:4:\"read\";s:4:\"read\";s:12:\"delete_posts\";s:15:\"delete_products\";s:20:\"delete_private_posts\";s:23:\"delete_private_products\";s:22:\"delete_published_posts\";s:25:\"delete_published_products\";s:19:\"delete_others_posts\";s:22:\"delete_others_products\";s:18:\"edit_private_posts\";s:21:\"edit_private_products\";s:20:\"edit_published_posts\";s:23:\"edit_published_products\";s:12:\"create_posts\";s:13:\"edit_products\";}s:12:\"map_meta_cap\";b:1;s:10:\"taxonomies\";a:0:{}s:11:\"has_archive\";s:4:\"shop\";s:7:\"rewrite\";a:5:{s:4:\"slug\";s:7:\"product\";s:10:\"with_front\";b:0;s:5:\"feeds\";b:1;s:5:\"pages\";b:1;s:7:\"ep_mask\";i:1;}s:9:\"query_var\";s:7:\"product\";s:10:\"can_export\";b:1;s:12:\"show_in_rest\";b:1;s:9:\"rest_base\";b:0;s:8:\"_builtin\";b:0;s:10:\"_edit_link\";s:16:\"post.php?post=%d\";}s:17:\"product_variation\";O:8:\"stdClass\":25:{s:4:\"name\";s:17:\"product_variation\";s:5:\"label\";s:10:\"Variations\";s:6:\"labels\";O:8:\"stdClass\":26:{s:4:\"name\";s:10:\"Variations\";s:13:\"singular_name\";s:10:\"Variations\";s:7:\"add_new\";s:7:\"Add New\";s:12:\"add_new_item\";s:12:\"Add New Post\";s:9:\"edit_item\";s:9:\"Edit Post\";s:8:\"new_item\";s:8:\"New Post\";s:9:\"view_item\";s:9:\"View Post\";s:10:\"view_items\";s:10:\"View Posts\";s:12:\"search_items\";s:12:\"Search Posts\";s:9:\"not_found\";s:15:\"No posts found.\";s:18:\"not_found_in_trash\";s:24:\"No posts found in Trash.\";s:17:\"parent_item_colon\";N;s:9:\"all_items\";s:10:\"Variations\";s:8:\"archives\";s:10:\"Variations\";s:10:\"attributes\";s:15:\"Post Attributes\";s:16:\"insert_into_item\";s:16:\"Insert into post\";s:21:\"uploaded_to_this_item\";s:21:\"Uploaded to this post\";s:14:\"featured_image\";s:14:\"Featured Image\";s:18:\"set_featured_image\";s:18:\"Set featured image\";s:21:\"remove_featured_image\";s:21:\"Remove featured image\";s:18:\"use_featured_image\";s:21:\"Use as featured image\";s:17:\"filter_items_list\";s:17:\"Filter posts list\";s:21:\"items_list_navigation\";s:21:\"Posts list navigation\";s:10:\"items_list\";s:10:\"Posts list\";s:9:\"menu_name\";s:10:\"Variations\";s:14:\"name_admin_bar\";s:10:\"Variations\";}s:11:\"description\";s:0:\"\";s:6:\"public\";b:0;s:12:\"hierarchical\";b:0;s:19:\"exclude_from_search\";b:1;s:18:\"publicly_queryable\";b:0;s:7:\"show_ui\";b:0;s:12:\"show_in_menu\";b:0;s:17:\"show_in_nav_menus\";b:0;s:17:\"show_in_admin_bar\";b:0;s:8:\"supports\";b:0;s:15:\"capability_type\";s:7:\"product\";s:3:\"cap\";O:8:\"stdClass\":8:{s:9:\"edit_post\";s:12:\"edit_product\";s:9:\"read_post\";s:12:\"read_product\";s:11:\"delete_post\";s:14:\"delete_product\";s:10:\"edit_posts\";s:13:\"edit_products\";s:17:\"edit_others_posts\";s:20:\"edit_others_products\";s:13:\"publish_posts\";s:16:\"publish_products\";s:18:\"read_private_posts\";s:21:\"read_private_products\";s:12:\"create_posts\";s:13:\"edit_products\";}s:12:\"map_meta_cap\";b:0;s:10:\"taxonomies\";a:0:{}s:11:\"has_archive\";b:0;s:7:\"rewrite\";b:0;s:9:\"query_var\";s:17:\"product_variation\";s:10:\"can_export\";b:1;s:12:\"show_in_rest\";b:0;s:9:\"rest_base\";b:0;s:8:\"_builtin\";b:0;s:10:\"_edit_link\";s:0:\"\";}s:10:\"shop_order\";O:8:\"stdClass\":24:{s:4:\"name\";s:10:\"shop_order\";s:5:\"label\";s:6:\"Orders\";s:6:\"labels\";O:8:\"stdClass\":28:{s:4:\"name\";s:6:\"Orders\";s:13:\"singular_name\";s:5:\"Order\";s:7:\"add_new\";s:9:\"Add order\";s:12:\"add_new_item\";s:13:\"Add new order\";s:9:\"edit_item\";s:10:\"Edit order\";s:8:\"new_item\";s:9:\"New order\";s:9:\"view_item\";s:10:\"View order\";s:10:\"view_items\";s:10:\"View Posts\";s:12:\"search_items\";s:13:\"Search orders\";s:9:\"not_found\";s:15:\"No orders found\";s:18:\"not_found_in_trash\";s:24:\"No orders found in trash\";s:17:\"parent_item_colon\";N;s:9:\"all_items\";s:6:\"Orders\";s:8:\"archives\";s:6:\"Orders\";s:10:\"attributes\";s:15:\"Post Attributes\";s:16:\"insert_into_item\";s:16:\"Insert into post\";s:21:\"uploaded_to_this_item\";s:21:\"Uploaded to this post\";s:14:\"featured_image\";s:14:\"Featured Image\";s:18:\"set_featured_image\";s:18:\"Set featured image\";s:21:\"remove_featured_image\";s:21:\"Remove featured image\";s:18:\"use_featured_image\";s:21:\"Use as featured image\";s:17:\"filter_items_list\";s:13:\"Filter orders\";s:21:\"items_list_navigation\";s:17:\"Orders navigation\";s:10:\"items_list\";s:11:\"Orders list\";s:9:\"menu_name\";s:6:\"Orders\";s:4:\"edit\";s:4:\"Edit\";s:6:\"parent\";s:13:\"Parent orders\";s:14:\"name_admin_bar\";s:5:\"Order\";}s:11:\"description\";s:38:\"This is where store orders are stored.\";s:6:\"public\";b:0;s:12:\"hierarchical\";b:0;s:19:\"exclude_from_search\";b:1;s:18:\"publicly_queryable\";b:0;s:7:\"show_ui\";b:1;s:12:\"show_in_menu\";s:11:\"woocommerce\";s:17:\"show_in_nav_menus\";b:0;s:17:\"show_in_admin_bar\";b:1;s:15:\"capability_type\";s:10:\"shop_order\";s:3:\"cap\";O:8:\"stdClass\":15:{s:9:\"edit_post\";s:15:\"edit_shop_order\";s:9:\"read_post\";s:15:\"read_shop_order\";s:11:\"delete_post\";s:17:\"delete_shop_order\";s:10:\"edit_posts\";s:16:\"edit_shop_orders\";s:17:\"edit_others_posts\";s:23:\"edit_others_shop_orders\";s:13:\"publish_posts\";s:19:\"publish_shop_orders\";s:18:\"read_private_posts\";s:24:\"read_private_shop_orders\";s:4:\"read\";s:4:\"read\";s:12:\"delete_posts\";s:18:\"delete_shop_orders\";s:20:\"delete_private_posts\";s:26:\"delete_private_shop_orders\";s:22:\"delete_published_posts\";s:28:\"delete_published_shop_orders\";s:19:\"delete_others_posts\";s:25:\"delete_others_shop_orders\";s:18:\"edit_private_posts\";s:24:\"edit_private_shop_orders\";s:20:\"edit_published_posts\";s:26:\"edit_published_shop_orders\";s:12:\"create_posts\";s:16:\"edit_shop_orders\";}s:12:\"map_meta_cap\";b:1;s:10:\"taxonomies\";a:0:{}s:11:\"has_archive\";b:0;s:7:\"rewrite\";b:0;s:9:\"query_var\";b:0;s:10:\"can_export\";b:1;s:12:\"show_in_rest\";b:0;s:9:\"rest_base\";b:0;s:8:\"_builtin\";b:0;s:10:\"_edit_link\";s:16:\"post.php?post=%d\";}s:17:\"shop_order_refund\";O:8:\"stdClass\":25:{s:4:\"name\";s:17:\"shop_order_refund\";s:5:\"label\";s:7:\"Refunds\";s:6:\"labels\";O:8:\"stdClass\":26:{s:4:\"name\";s:7:\"Refunds\";s:13:\"singular_name\";s:7:\"Refunds\";s:7:\"add_new\";s:7:\"Add New\";s:12:\"add_new_item\";s:12:\"Add New Post\";s:9:\"edit_item\";s:9:\"Edit Post\";s:8:\"new_item\";s:8:\"New Post\";s:9:\"view_item\";s:9:\"View Post\";s:10:\"view_items\";s:10:\"View Posts\";s:12:\"search_items\";s:12:\"Search Posts\";s:9:\"not_found\";s:15:\"No posts found.\";s:18:\"not_found_in_trash\";s:24:\"No posts found in Trash.\";s:17:\"parent_item_colon\";N;s:9:\"all_items\";s:7:\"Refunds\";s:8:\"archives\";s:7:\"Refunds\";s:10:\"attributes\";s:15:\"Post Attributes\";s:16:\"insert_into_item\";s:16:\"Insert into post\";s:21:\"uploaded_to_this_item\";s:21:\"Uploaded to this post\";s:14:\"featured_image\";s:14:\"Featured Image\";s:18:\"set_featured_image\";s:18:\"Set featured image\";s:21:\"remove_featured_image\";s:21:\"Remove featured image\";s:18:\"use_featured_image\";s:21:\"Use as featured image\";s:17:\"filter_items_list\";s:17:\"Filter posts list\";s:21:\"items_list_navigation\";s:21:\"Posts list navigation\";s:10:\"items_list\";s:10:\"Posts list\";s:9:\"menu_name\";s:7:\"Refunds\";s:14:\"name_admin_bar\";s:7:\"Refunds\";}s:11:\"description\";s:0:\"\";s:6:\"public\";b:0;s:12:\"hierarchical\";b:0;s:19:\"exclude_from_search\";b:1;s:18:\"publicly_queryable\";b:0;s:7:\"show_ui\";b:0;s:12:\"show_in_menu\";b:0;s:17:\"show_in_nav_menus\";b:0;s:17:\"show_in_admin_bar\";b:0;s:8:\"supports\";b:0;s:15:\"capability_type\";s:10:\"shop_order\";s:3:\"cap\";O:8:\"stdClass\":8:{s:9:\"edit_post\";s:15:\"edit_shop_order\";s:9:\"read_post\";s:15:\"read_shop_order\";s:11:\"delete_post\";s:17:\"delete_shop_order\";s:10:\"edit_posts\";s:16:\"edit_shop_orders\";s:17:\"edit_others_posts\";s:23:\"edit_others_shop_orders\";s:13:\"publish_posts\";s:19:\"publish_shop_orders\";s:18:\"read_private_posts\";s:24:\"read_private_shop_orders\";s:12:\"create_posts\";s:16:\"edit_shop_orders\";}s:12:\"map_meta_cap\";b:0;s:10:\"taxonomies\";a:0:{}s:11:\"has_archive\";b:0;s:7:\"rewrite\";b:0;s:9:\"query_var\";s:17:\"shop_order_refund\";s:10:\"can_export\";b:1;s:12:\"show_in_rest\";b:0;s:9:\"rest_base\";b:0;s:8:\"_builtin\";b:0;s:10:\"_edit_link\";s:0:\"\";}s:11:\"shop_coupon\";O:8:\"stdClass\":24:{s:4:\"name\";s:11:\"shop_coupon\";s:5:\"label\";s:7:\"Coupons\";s:6:\"labels\";O:8:\"stdClass\":28:{s:4:\"name\";s:7:\"Coupons\";s:13:\"singular_name\";s:6:\"Coupon\";s:7:\"add_new\";s:10:\"Add coupon\";s:12:\"add_new_item\";s:14:\"Add new coupon\";s:9:\"edit_item\";s:11:\"Edit coupon\";s:8:\"new_item\";s:10:\"New coupon\";s:9:\"view_item\";s:11:\"View coupon\";s:10:\"view_items\";s:10:\"View Posts\";s:12:\"search_items\";s:14:\"Search coupons\";s:9:\"not_found\";s:16:\"No coupons found\";s:18:\"not_found_in_trash\";s:25:\"No coupons found in trash\";s:17:\"parent_item_colon\";N;s:9:\"all_items\";s:7:\"Coupons\";s:8:\"archives\";s:7:\"Coupons\";s:10:\"attributes\";s:15:\"Post Attributes\";s:16:\"insert_into_item\";s:16:\"Insert into post\";s:21:\"uploaded_to_this_item\";s:21:\"Uploaded to this post\";s:14:\"featured_image\";s:14:\"Featured Image\";s:18:\"set_featured_image\";s:18:\"Set featured image\";s:21:\"remove_featured_image\";s:21:\"Remove featured image\";s:18:\"use_featured_image\";s:21:\"Use as featured image\";s:17:\"filter_items_list\";s:14:\"Filter coupons\";s:21:\"items_list_navigation\";s:18:\"Coupons navigation\";s:10:\"items_list\";s:12:\"Coupons list\";s:9:\"menu_name\";s:7:\"Coupons\";s:4:\"edit\";s:4:\"Edit\";s:6:\"parent\";s:13:\"Parent coupon\";s:14:\"name_admin_bar\";s:6:\"Coupon\";}s:11:\"description\";s:75:\"This is where you can add new coupons that customers can use in your store.\";s:6:\"public\";b:0;s:12:\"hierarchical\";b:0;s:19:\"exclude_from_search\";b:1;s:18:\"publicly_queryable\";b:0;s:7:\"show_ui\";b:1;s:12:\"show_in_menu\";s:11:\"woocommerce\";s:17:\"show_in_nav_menus\";b:0;s:17:\"show_in_admin_bar\";b:1;s:15:\"capability_type\";s:11:\"shop_coupon\";s:3:\"cap\";O:8:\"stdClass\":15:{s:9:\"edit_post\";s:16:\"edit_shop_coupon\";s:9:\"read_post\";s:16:\"read_shop_coupon\";s:11:\"delete_post\";s:18:\"delete_shop_coupon\";s:10:\"edit_posts\";s:17:\"edit_shop_coupons\";s:17:\"edit_others_posts\";s:24:\"edit_others_shop_coupons\";s:13:\"publish_posts\";s:20:\"publish_shop_coupons\";s:18:\"read_private_posts\";s:25:\"read_private_shop_coupons\";s:4:\"read\";s:4:\"read\";s:12:\"delete_posts\";s:19:\"delete_shop_coupons\";s:20:\"delete_private_posts\";s:27:\"delete_private_shop_coupons\";s:22:\"delete_published_posts\";s:29:\"delete_published_shop_coupons\";s:19:\"delete_others_posts\";s:26:\"delete_others_shop_coupons\";s:18:\"edit_private_posts\";s:25:\"edit_private_shop_coupons\";s:20:\"edit_published_posts\";s:27:\"edit_published_shop_coupons\";s:12:\"create_posts\";s:17:\"edit_shop_coupons\";}s:12:\"map_meta_cap\";b:1;s:10:\"taxonomies\";a:0:{}s:11:\"has_archive\";b:0;s:7:\"rewrite\";b:0;s:9:\"query_var\";b:0;s:10:\"can_export\";b:1;s:12:\"show_in_rest\";b:0;s:9:\"rest_base\";b:0;s:8:\"_builtin\";b:0;s:10:\"_edit_link\";s:16:\"post.php?post=%d\";}}}i:2;i:1;i:3;d:1534250818.955893;i:4;b:0;i:5;a:13:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:5:\"admin\";s:10:\"user_email\";s:20:\"tudor@contursoft.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;s:6:\"is_cli\";b:0;}}', 'no'),
(551, 'jpsq_sync-1534250818.958097-284599-37', 'a:6:{i:0;s:21:\"jetpack_sync_callable\";i:1;a:2:{i:0;s:18:\"post_type_features\";i:1;a:14:{s:4:\"post\";a:10:{s:5:\"title\";b:1;s:6:\"editor\";b:1;s:6:\"author\";b:1;s:9:\"thumbnail\";b:1;s:7:\"excerpt\";b:1;s:10:\"trackbacks\";b:1;s:13:\"custom-fields\";b:1;s:8:\"comments\";b:1;s:9:\"revisions\";b:1;s:12:\"post-formats\";b:1;}s:4:\"page\";a:8:{s:5:\"title\";b:1;s:6:\"editor\";b:1;s:6:\"author\";b:1;s:9:\"thumbnail\";b:1;s:15:\"page-attributes\";b:1;s:13:\"custom-fields\";b:1;s:8:\"comments\";b:1;s:9:\"revisions\";b:1;}s:10:\"attachment\";a:3:{s:5:\"title\";b:1;s:6:\"author\";b:1;s:8:\"comments\";b:1;}s:16:\"attachment:audio\";a:1:{s:9:\"thumbnail\";b:1;}s:16:\"attachment:video\";a:1:{s:9:\"thumbnail\";b:1;}s:8:\"revision\";a:1:{s:6:\"author\";b:1;}s:13:\"nav_menu_item\";a:2:{s:5:\"title\";b:1;s:6:\"editor\";b:1;}s:10:\"custom_css\";a:2:{s:5:\"title\";b:1;s:9:\"revisions\";b:1;}s:19:\"customize_changeset\";a:2:{s:5:\"title\";b:1;s:6:\"author\";b:1;}s:12:\"oembed_cache\";a:2:{s:5:\"title\";b:1;s:6:\"editor\";b:1;}s:12:\"user_request\";a:2:{s:5:\"title\";b:1;s:6:\"editor\";b:1;}s:7:\"product\";a:7:{s:9:\"thumbnail\";b:1;s:5:\"title\";b:1;s:6:\"editor\";b:1;s:7:\"excerpt\";b:1;s:13:\"custom-fields\";b:1;s:9:\"publicize\";b:1;s:14:\"wpcom-markdown\";b:1;}s:10:\"shop_order\";a:3:{s:5:\"title\";b:1;s:8:\"comments\";b:1;s:13:\"custom-fields\";b:1;}s:11:\"shop_coupon\";a:1:{s:5:\"title\";b:1;}}}i:2;i:1;i:3;d:1534250818.958039;i:4;b:0;i:5;a:13:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:5:\"admin\";s:10:\"user_email\";s:20:\"tudor@contursoft.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;s:6:\"is_cli\";b:0;}}', 'no'),
(552, 'jpsq_sync-1534250818.958432-284599-38', 'a:6:{i:0;s:21:\"jetpack_sync_callable\";i:1;a:2:{i:0;s:10:\"shortcodes\";i:1;a:27:{i:0;s:10:\"wp_caption\";i:1;s:7:\"caption\";i:2;s:7:\"gallery\";i:3;s:8:\"playlist\";i:4;s:5:\"audio\";i:5;s:5:\"video\";i:6;s:5:\"embed\";i:7;s:7:\"product\";i:8;s:12:\"product_page\";i:9;s:16:\"product_category\";i:10;s:18:\"product_categories\";i:11;s:11:\"add_to_cart\";i:12;s:15:\"add_to_cart_url\";i:13;s:8:\"products\";i:14;s:15:\"recent_products\";i:15;s:13:\"sale_products\";i:16;s:21:\"best_selling_products\";i:17;s:18:\"top_rated_products\";i:18;s:17:\"featured_products\";i:19;s:17:\"product_attribute\";i:20;s:16:\"related_products\";i:21;s:13:\"shop_messages\";i:22;s:26:\"woocommerce_order_tracking\";i:23;s:16:\"woocommerce_cart\";i:24;s:20:\"woocommerce_checkout\";i:25;s:22:\"woocommerce_my_account\";i:26;s:20:\"woocommerce_messages\";}}i:2;i:1;i:3;d:1534250818.958405;i:4;b:0;i:5;a:13:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:5:\"admin\";s:10:\"user_email\";s:20:\"tudor@contursoft.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;s:6:\"is_cli\";b:0;}}', 'no'),
(553, 'jpsq_sync-1534250818.958706-284599-39', 'a:6:{i:0;s:21:\"jetpack_sync_callable\";i:1;a:2:{i:0;s:27:\"rest_api_allowed_post_types\";i:1;a:4:{i:0;s:4:\"post\";i:1;s:4:\"page\";i:2;s:8:\"revision\";i:3;s:7:\"product\";}}i:2;i:1;i:3;d:1534250818.958681;i:4;b:0;i:5;a:13:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:5:\"admin\";s:10:\"user_email\";s:20:\"tudor@contursoft.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;s:6:\"is_cli\";b:0;}}', 'no'),
(554, 'jpsq_sync-1534250818.958955-284599-40', 'a:6:{i:0;s:21:\"jetpack_sync_callable\";i:1;a:2:{i:0;s:32:\"rest_api_allowed_public_metadata\";i:1;a:0:{}}i:2;i:1;i:3;d:1534250818.958929;i:4;b:0;i:5;a:13:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:5:\"admin\";s:10:\"user_email\";s:20:\"tudor@contursoft.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;s:6:\"is_cli\";b:0;}}', 'no'),
(555, 'jpsq_sync-1534250818.959258-284599-41', 'a:6:{i:0;s:21:\"jetpack_sync_callable\";i:1;a:2:{i:0;s:24:\"sso_is_two_step_required\";i:1;b:0;}i:2;i:1;i:3;d:1534250818.959234;i:4;b:0;i:5;a:13:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:5:\"admin\";s:10:\"user_email\";s:20:\"tudor@contursoft.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;s:6:\"is_cli\";b:0;}}', 'no'),
(556, 'jpsq_sync-1534250818.959583-284599-42', 'a:6:{i:0;s:21:\"jetpack_sync_callable\";i:1;a:2:{i:0;s:26:\"sso_should_hide_login_form\";i:1;b:0;}i:2;i:1;i:3;d:1534250818.959549;i:4;b:0;i:5;a:13:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:5:\"admin\";s:10:\"user_email\";s:20:\"tudor@contursoft.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;s:6:\"is_cli\";b:0;}}', 'no'),
(557, 'jpsq_sync-1534250818.959887-284599-43', 'a:6:{i:0;s:21:\"jetpack_sync_callable\";i:1;a:2:{i:0;s:18:\"sso_match_by_email\";i:1;b:1;}i:2;i:1;i:3;d:1534250818.95986;i:4;b:0;i:5;a:13:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:5:\"admin\";s:10:\"user_email\";s:20:\"tudor@contursoft.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;s:6:\"is_cli\";b:0;}}', 'no'),
(558, 'jpsq_sync-1534250818.969835-284599-44', 'a:6:{i:0;s:21:\"jetpack_sync_callable\";i:1;a:2:{i:0;s:21:\"sso_new_user_override\";i:1;b:0;}i:2;i:1;i:3;d:1534250818.969782;i:4;b:0;i:5;a:13:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:5:\"admin\";s:10:\"user_email\";s:20:\"tudor@contursoft.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;s:6:\"is_cli\";b:0;}}', 'no'),
(559, 'jpsq_sync-1534250818.970250-284599-45', 'a:6:{i:0;s:21:\"jetpack_sync_callable\";i:1;a:2:{i:0;s:29:\"sso_bypass_default_login_form\";i:1;b:0;}i:2;i:1;i:3;d:1534250818.970211;i:4;b:0;i:5;a:13:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:5:\"admin\";s:10:\"user_email\";s:20:\"tudor@contursoft.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;s:6:\"is_cli\";b:0;}}', 'no'),
(560, 'jpsq_sync-1534250818.970597-284599-46', 'a:6:{i:0;s:21:\"jetpack_sync_callable\";i:1;a:2:{i:0;s:10:\"wp_version\";i:1;s:5:\"4.9.8\";}i:2;i:1;i:3;d:1534250818.970569;i:4;b:0;i:5;a:13:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:5:\"admin\";s:10:\"user_email\";s:20:\"tudor@contursoft.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;s:6:\"is_cli\";b:0;}}', 'no');
INSERT INTO `r8ISTpM09o_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(561, 'jpsq_sync-1534250818.971111-284599-47', 'a:6:{i:0;s:21:\"jetpack_sync_callable\";i:1;a:2:{i:0;s:11:\"get_plugins\";i:1;a:11:{s:19:\"akismet/akismet.php\";a:13:{s:20:\"WC requires at least\";s:0:\"\";s:15:\"WC tested up to\";s:0:\"\";s:4:\"Name\";s:17:\"Akismet Anti-Spam\";s:9:\"PluginURI\";s:20:\"https://akismet.com/\";s:7:\"Version\";s:5:\"4.0.8\";s:11:\"Description\";s:279:\"Used by millions, Akismet is quite possibly the best way in the world to <strong>protect your blog from spam</strong>. It keeps your site protected even while you sleep. To get started: activate the Akismet plugin and then go to your Akismet Settings page to set up your API key.\";s:6:\"Author\";s:10:\"Automattic\";s:9:\"AuthorURI\";s:41:\"https://automattic.com/wordpress-plugins/\";s:10:\"TextDomain\";s:7:\"akismet\";s:10:\"DomainPath\";s:0:\"\";s:7:\"Network\";b:0;s:5:\"Title\";s:17:\"Akismet Anti-Spam\";s:10:\"AuthorName\";s:10:\"Automattic\";}s:43:\"custom-post-type-ui/custom-post-type-ui.php\";a:13:{s:20:\"WC requires at least\";s:0:\"\";s:15:\"WC tested up to\";s:0:\"\";s:4:\"Name\";s:19:\"Custom Post Type UI\";s:9:\"PluginURI\";s:53:\"https://github.com/WebDevStudios/custom-post-type-ui/\";s:7:\"Version\";s:5:\"1.5.8\";s:11:\"Description\";s:77:\"Admin panel for creating custom post types and custom taxonomies in WordPress\";s:6:\"Author\";s:13:\"WebDevStudios\";s:9:\"AuthorURI\";s:26:\"https://webdevstudios.com/\";s:10:\"TextDomain\";s:19:\"custom-post-type-ui\";s:10:\"DomainPath\";s:10:\"/languages\";s:7:\"Network\";b:0;s:5:\"Title\";s:19:\"Custom Post Type UI\";s:10:\"AuthorName\";s:13:\"WebDevStudios\";}s:9:\"hello.php\";a:13:{s:20:\"WC requires at least\";s:0:\"\";s:15:\"WC tested up to\";s:0:\"\";s:4:\"Name\";s:11:\"Hello Dolly\";s:9:\"PluginURI\";s:41:\"http://wordpress.org/plugins/hello-dolly/\";s:7:\"Version\";s:3:\"1.7\";s:11:\"Description\";s:295:\"This is not just a plugin, it symbolizes the hope and enthusiasm of an entire generation summed up in two words sung most famously by Louis Armstrong: Hello, Dolly. When activated you will randomly see a lyric from <cite>Hello, Dolly</cite> in the upper right of your admin screen on every page.\";s:6:\"Author\";s:14:\"Matt Mullenweg\";s:9:\"AuthorURI\";s:13:\"http://ma.tt/\";s:10:\"TextDomain\";s:0:\"\";s:10:\"DomainPath\";s:0:\"\";s:7:\"Network\";b:0;s:5:\"Title\";s:11:\"Hello Dolly\";s:10:\"AuthorName\";s:14:\"Matt Mullenweg\";}s:19:\"jetpack/jetpack.php\";a:13:{s:20:\"WC requires at least\";s:0:\"\";s:15:\"WC tested up to\";s:0:\"\";s:4:\"Name\";s:24:\"Jetpack by WordPress.com\";s:9:\"PluginURI\";s:19:\"https://jetpack.com\";s:7:\"Version\";s:5:\"6.4.2\";s:11:\"Description\";s:218:\"Bring the power of the WordPress.com cloud to your self-hosted WordPress. Jetpack enables you to connect your blog to a WordPress.com account to use the powerful features normally only available to WordPress.com users.\";s:6:\"Author\";s:10:\"Automattic\";s:9:\"AuthorURI\";s:19:\"https://jetpack.com\";s:10:\"TextDomain\";s:7:\"jetpack\";s:10:\"DomainPath\";s:11:\"/languages/\";s:7:\"Network\";b:0;s:5:\"Title\";s:24:\"Jetpack by WordPress.com\";s:10:\"AuthorName\";s:10:\"Automattic\";}s:51:\"mailchimp-for-woocommerce/mailchimp-woocommerce.php\";a:13:{s:20:\"WC requires at least\";s:0:\"\";s:15:\"WC tested up to\";s:0:\"\";s:4:\"Name\";s:25:\"MailChimp for WooCommerce\";s:9:\"PluginURI\";s:41:\"https://mailchimp.com/connect-your-store/\";s:7:\"Version\";s:5:\"2.1.9\";s:11:\"Description\";s:30:\"MailChimp - WooCommerce plugin\";s:6:\"Author\";s:9:\"MailChimp\";s:9:\"AuthorURI\";s:21:\"https://mailchimp.com\";s:10:\"TextDomain\";s:21:\"mailchimp-woocommerce\";s:10:\"DomainPath\";s:10:\"/languages\";s:7:\"Network\";b:0;s:5:\"Title\";s:25:\"MailChimp for WooCommerce\";s:10:\"AuthorName\";s:9:\"MailChimp\";}s:27:\"woocommerce/woocommerce.php\";a:13:{s:20:\"WC requires at least\";s:0:\"\";s:15:\"WC tested up to\";s:0:\"\";s:4:\"Name\";s:11:\"WooCommerce\";s:9:\"PluginURI\";s:24:\"https://woocommerce.com/\";s:7:\"Version\";s:5:\"3.4.4\";s:11:\"Description\";s:63:\"An eCommerce toolkit that helps you sell anything. Beautifully.\";s:6:\"Author\";s:10:\"Automattic\";s:9:\"AuthorURI\";s:23:\"https://woocommerce.com\";s:10:\"TextDomain\";s:11:\"woocommerce\";s:10:\"DomainPath\";s:16:\"/i18n/languages/\";s:7:\"Network\";b:0;s:5:\"Title\";s:11:\"WooCommerce\";s:10:\"AuthorName\";s:10:\"Automattic\";}s:63:\"woocommerce-my-account-widget/woocommerce-my-account-widget.php\";a:13:{s:20:\"WC requires at least\";s:5:\"3.0.0\";s:15:\"WC tested up to\";s:5:\"3.3.5\";s:4:\"Name\";s:29:\"WooCommerce My Account Widget\";s:9:\"PluginURI\";s:66:\"http://wordpress.org/extend/plugins/woocommerce-my-account-widget/\";s:7:\"Version\";s:5:\"0.6.4\";s:11:\"Description\";s:57:\"WooCommerce My Account Widget shows order & account data.\";s:6:\"Author\";s:12:\"Bart Pluijms\";s:9:\"AuthorURI\";s:19:\"http://www.geev.nl/\";s:10:\"TextDomain\";s:29:\"woocommerce-my-account-widget\";s:10:\"DomainPath\";s:0:\"\";s:7:\"Network\";b:0;s:5:\"Title\";s:29:\"WooCommerce My Account Widget\";s:10:\"AuthorName\";s:12:\"Bart Pluijms\";}s:91:\"woocommerce-gateway-paypal-express-checkout/woocommerce-gateway-paypal-express-checkout.php\";a:13:{s:20:\"WC requires at least\";s:3:\"2.6\";s:15:\"WC tested up to\";s:3:\"3.4\";s:4:\"Name\";s:35:\"WooCommerce PayPal Checkout Gateway\";s:9:\"PluginURI\";s:77:\"https://woocommerce.com/products/woocommerce-gateway-paypal-express-checkout/\";s:7:\"Version\";s:5:\"1.6.1\";s:11:\"Description\";s:94:\"A payment gateway for PayPal Checkout (https://www.paypal.com/us/webapps/mpp/paypal-checkout).\";s:6:\"Author\";s:11:\"WooCommerce\";s:9:\"AuthorURI\";s:23:\"https://woocommerce.com\";s:10:\"TextDomain\";s:43:\"woocommerce-gateway-paypal-express-checkout\";s:10:\"DomainPath\";s:10:\"/languages\";s:7:\"Network\";b:0;s:5:\"Title\";s:35:\"WooCommerce PayPal Checkout Gateway\";s:10:\"AuthorName\";s:11:\"WooCommerce\";}s:45:\"woocommerce-services/woocommerce-services.php\";a:13:{s:20:\"WC requires at least\";s:5:\"3.0.0\";s:15:\"WC tested up to\";s:5:\"3.4.3\";s:4:\"Name\";s:20:\"WooCommerce Services\";s:9:\"PluginURI\";s:24:\"https://woocommerce.com/\";s:7:\"Version\";s:6:\"1.15.1\";s:11:\"Description\";s:133:\"Hosted services for WooCommerce: automated tax calculation, live shipping rates, shipping label printing, and smoother payment setup.\";s:6:\"Author\";s:10:\"Automattic\";s:9:\"AuthorURI\";s:24:\"https://woocommerce.com/\";s:10:\"TextDomain\";s:20:\"woocommerce-services\";s:10:\"DomainPath\";s:16:\"/i18n/languages/\";s:7:\"Network\";b:0;s:5:\"Title\";s:20:\"WooCommerce Services\";s:10:\"AuthorName\";s:10:\"Automattic\";}s:57:\"woocommerce-gateway-stripe/woocommerce-gateway-stripe.php\";a:13:{s:20:\"WC requires at least\";s:3:\"2.6\";s:15:\"WC tested up to\";s:3:\"3.4\";s:4:\"Name\";s:26:\"WooCommerce Stripe Gateway\";s:9:\"PluginURI\";s:57:\"https://wordpress.org/plugins/woocommerce-gateway-stripe/\";s:7:\"Version\";s:5:\"4.1.8\";s:11:\"Description\";s:53:\"Take credit card payments on your store using Stripe.\";s:6:\"Author\";s:11:\"WooCommerce\";s:9:\"AuthorURI\";s:24:\"https://woocommerce.com/\";s:10:\"TextDomain\";s:26:\"woocommerce-gateway-stripe\";s:10:\"DomainPath\";s:11:\"/languages/\";s:7:\"Network\";b:0;s:5:\"Title\";s:26:\"WooCommerce Stripe Gateway\";s:10:\"AuthorName\";s:11:\"WooCommerce\";}s:27:\"woosidebars/woosidebars.php\";a:13:{s:20:\"WC requires at least\";s:0:\"\";s:15:\"WC tested up to\";s:0:\"\";s:4:\"Name\";s:11:\"WooSidebars\";s:9:\"PluginURI\";s:35:\"http://woocommerce.com/woosidebars/\";s:7:\"Version\";s:5:\"1.4.5\";s:11:\"Description\";s:96:\"Replace widget areas in your theme for specific pages, archives and other sections of WordPress.\";s:6:\"Author\";s:11:\"WooCommerce\";s:9:\"AuthorURI\";s:23:\"http://woocommerce.com/\";s:10:\"TextDomain\";s:11:\"woosidebars\";s:10:\"DomainPath\";s:5:\"/lang\";s:7:\"Network\";b:0;s:5:\"Title\";s:11:\"WooSidebars\";s:10:\"AuthorName\";s:11:\"WooCommerce\";}}}i:2;i:1;i:3;d:1534250818.971067;i:4;b:0;i:5;a:13:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:5:\"admin\";s:10:\"user_email\";s:20:\"tudor@contursoft.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;s:6:\"is_cli\";b:0;}}', 'no'),
(562, 'jpsq_sync-1534250818.971737-284599-48', 'a:6:{i:0;s:21:\"jetpack_sync_callable\";i:1;a:2:{i:0;s:24:\"get_plugins_action_links\";i:1;a:0:{}}i:2;i:1;i:3;d:1534250818.971708;i:4;b:0;i:5;a:13:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:5:\"admin\";s:10:\"user_email\";s:20:\"tudor@contursoft.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;s:6:\"is_cli\";b:0;}}', 'no'),
(563, 'jpsq_sync-1534250818.972019-284599-49', 'a:6:{i:0;s:21:\"jetpack_sync_callable\";i:1;a:2:{i:0;s:14:\"active_modules\";i:1;a:0:{}}i:2;i:1;i:3;d:1534250818.971991;i:4;b:0;i:5;a:13:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:5:\"admin\";s:10:\"user_email\";s:20:\"tudor@contursoft.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;s:6:\"is_cli\";b:0;}}', 'no'),
(564, 'jpsq_sync-1534250818.972298-284599-50', 'a:6:{i:0;s:21:\"jetpack_sync_callable\";i:1;a:2:{i:0;s:16:\"hosting_provider\";i:1;s:7:\"unknown\";}i:2;i:1;i:3;d:1534250818.972271;i:4;b:0;i:5;a:13:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:5:\"admin\";s:10:\"user_email\";s:20:\"tudor@contursoft.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;s:6:\"is_cli\";b:0;}}', 'no'),
(565, 'jpsq_sync-1534250818.972568-284599-51', 'a:6:{i:0;s:21:\"jetpack_sync_callable\";i:1;a:2:{i:0;s:6:\"locale\";i:1;s:5:\"en_US\";}i:2;i:1;i:3;d:1534250818.972541;i:4;b:0;i:5;a:13:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:5:\"admin\";s:10:\"user_email\";s:20:\"tudor@contursoft.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;s:6:\"is_cli\";b:0;}}', 'no'),
(566, 'jpsq_sync-1534250818.972837-284599-52', 'a:6:{i:0;s:21:\"jetpack_sync_callable\";i:1;a:2:{i:0;s:13:\"site_icon_url\";i:1;b:0;}i:2;i:1;i:3;d:1534250818.97281;i:4;b:0;i:5;a:13:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:5:\"admin\";s:10:\"user_email\";s:20:\"tudor@contursoft.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;s:6:\"is_cli\";b:0;}}', 'no'),
(567, 'jpsq_sync-1534250818.973232-284599-53', 'a:6:{i:0;s:21:\"jetpack_sync_callable\";i:1;a:2:{i:0;s:5:\"roles\";i:1;a:7:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:114:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:18:\"manage_woocommerce\";b:1;s:24:\"view_woocommerce_reports\";b:1;s:12:\"edit_product\";b:1;s:12:\"read_product\";b:1;s:14:\"delete_product\";b:1;s:13:\"edit_products\";b:1;s:20:\"edit_others_products\";b:1;s:16:\"publish_products\";b:1;s:21:\"read_private_products\";b:1;s:15:\"delete_products\";b:1;s:23:\"delete_private_products\";b:1;s:25:\"delete_published_products\";b:1;s:22:\"delete_others_products\";b:1;s:21:\"edit_private_products\";b:1;s:23:\"edit_published_products\";b:1;s:20:\"manage_product_terms\";b:1;s:18:\"edit_product_terms\";b:1;s:20:\"delete_product_terms\";b:1;s:20:\"assign_product_terms\";b:1;s:15:\"edit_shop_order\";b:1;s:15:\"read_shop_order\";b:1;s:17:\"delete_shop_order\";b:1;s:16:\"edit_shop_orders\";b:1;s:23:\"edit_others_shop_orders\";b:1;s:19:\"publish_shop_orders\";b:1;s:24:\"read_private_shop_orders\";b:1;s:18:\"delete_shop_orders\";b:1;s:26:\"delete_private_shop_orders\";b:1;s:28:\"delete_published_shop_orders\";b:1;s:25:\"delete_others_shop_orders\";b:1;s:24:\"edit_private_shop_orders\";b:1;s:26:\"edit_published_shop_orders\";b:1;s:23:\"manage_shop_order_terms\";b:1;s:21:\"edit_shop_order_terms\";b:1;s:23:\"delete_shop_order_terms\";b:1;s:23:\"assign_shop_order_terms\";b:1;s:16:\"edit_shop_coupon\";b:1;s:16:\"read_shop_coupon\";b:1;s:18:\"delete_shop_coupon\";b:1;s:17:\"edit_shop_coupons\";b:1;s:24:\"edit_others_shop_coupons\";b:1;s:20:\"publish_shop_coupons\";b:1;s:25:\"read_private_shop_coupons\";b:1;s:19:\"delete_shop_coupons\";b:1;s:27:\"delete_private_shop_coupons\";b:1;s:29:\"delete_published_shop_coupons\";b:1;s:26:\"delete_others_shop_coupons\";b:1;s:25:\"edit_private_shop_coupons\";b:1;s:27:\"edit_published_shop_coupons\";b:1;s:24:\"manage_shop_coupon_terms\";b:1;s:22:\"edit_shop_coupon_terms\";b:1;s:24:\"delete_shop_coupon_terms\";b:1;s:24:\"assign_shop_coupon_terms\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}s:8:\"customer\";a:2:{s:4:\"name\";s:8:\"Customer\";s:12:\"capabilities\";a:1:{s:4:\"read\";b:1;}}s:12:\"shop_manager\";a:2:{s:4:\"name\";s:12:\"Shop manager\";s:12:\"capabilities\";a:92:{s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:4:\"read\";b:1;s:18:\"read_private_pages\";b:1;s:18:\"read_private_posts\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_posts\";b:1;s:10:\"edit_pages\";b:1;s:20:\"edit_published_posts\";b:1;s:20:\"edit_published_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"edit_private_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:17:\"edit_others_pages\";b:1;s:13:\"publish_posts\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_posts\";b:1;s:12:\"delete_pages\";b:1;s:20:\"delete_private_pages\";b:1;s:20:\"delete_private_posts\";b:1;s:22:\"delete_published_pages\";b:1;s:22:\"delete_published_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:19:\"delete_others_pages\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:17:\"moderate_comments\";b:1;s:12:\"upload_files\";b:1;s:6:\"export\";b:1;s:6:\"import\";b:1;s:10:\"list_users\";b:1;s:18:\"manage_woocommerce\";b:1;s:24:\"view_woocommerce_reports\";b:1;s:12:\"edit_product\";b:1;s:12:\"read_product\";b:1;s:14:\"delete_product\";b:1;s:13:\"edit_products\";b:1;s:20:\"edit_others_products\";b:1;s:16:\"publish_products\";b:1;s:21:\"read_private_products\";b:1;s:15:\"delete_products\";b:1;s:23:\"delete_private_products\";b:1;s:25:\"delete_published_products\";b:1;s:22:\"delete_others_products\";b:1;s:21:\"edit_private_products\";b:1;s:23:\"edit_published_products\";b:1;s:20:\"manage_product_terms\";b:1;s:18:\"edit_product_terms\";b:1;s:20:\"delete_product_terms\";b:1;s:20:\"assign_product_terms\";b:1;s:15:\"edit_shop_order\";b:1;s:15:\"read_shop_order\";b:1;s:17:\"delete_shop_order\";b:1;s:16:\"edit_shop_orders\";b:1;s:23:\"edit_others_shop_orders\";b:1;s:19:\"publish_shop_orders\";b:1;s:24:\"read_private_shop_orders\";b:1;s:18:\"delete_shop_orders\";b:1;s:26:\"delete_private_shop_orders\";b:1;s:28:\"delete_published_shop_orders\";b:1;s:25:\"delete_others_shop_orders\";b:1;s:24:\"edit_private_shop_orders\";b:1;s:26:\"edit_published_shop_orders\";b:1;s:23:\"manage_shop_order_terms\";b:1;s:21:\"edit_shop_order_terms\";b:1;s:23:\"delete_shop_order_terms\";b:1;s:23:\"assign_shop_order_terms\";b:1;s:16:\"edit_shop_coupon\";b:1;s:16:\"read_shop_coupon\";b:1;s:18:\"delete_shop_coupon\";b:1;s:17:\"edit_shop_coupons\";b:1;s:24:\"edit_others_shop_coupons\";b:1;s:20:\"publish_shop_coupons\";b:1;s:25:\"read_private_shop_coupons\";b:1;s:19:\"delete_shop_coupons\";b:1;s:27:\"delete_private_shop_coupons\";b:1;s:29:\"delete_published_shop_coupons\";b:1;s:26:\"delete_others_shop_coupons\";b:1;s:25:\"edit_private_shop_coupons\";b:1;s:27:\"edit_published_shop_coupons\";b:1;s:24:\"manage_shop_coupon_terms\";b:1;s:22:\"edit_shop_coupon_terms\";b:1;s:24:\"delete_shop_coupon_terms\";b:1;s:24:\"assign_shop_coupon_terms\";b:1;}}}}i:2;i:1;i:3;d:1534250818.973205;i:4;b:0;i:5;a:13:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:5:\"admin\";s:10:\"user_email\";s:20:\"tudor@contursoft.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;s:6:\"is_cli\";b:0;}}', 'no'),
(568, 'jpsq_sync-1534250818.973877-284599-54', 'a:6:{i:0;s:21:\"jetpack_sync_callable\";i:1;a:2:{i:0;s:8:\"timezone\";i:1;s:5:\"UTC+0\";}i:2;i:1;i:3;d:1534250818.973851;i:4;b:0;i:5;a:13:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:5:\"admin\";s:10:\"user_email\";s:20:\"tudor@contursoft.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;s:6:\"is_cli\";b:0;}}', 'no'),
(569, 'jetpack_callables_sync_checksum', 'a:30:{s:18:\"wp_max_upload_size\";i:4150424846;s:15:\"is_main_network\";i:734881840;s:13:\"is_multi_site\";i:734881840;s:17:\"main_network_site\";i:2545500651;s:8:\"site_url\";i:2545500651;s:8:\"home_url\";i:2545500651;s:16:\"single_user_site\";i:734881840;s:7:\"updates\";i:3425443202;s:28:\"has_file_system_write_access\";i:4261170317;s:21:\"is_version_controlled\";i:734881840;s:10:\"taxonomies\";i:3990238582;s:10:\"post_types\";i:1437270184;s:18:\"post_type_features\";i:1174118575;s:10:\"shortcodes\";i:1978426851;s:27:\"rest_api_allowed_post_types\";i:2759734118;s:32:\"rest_api_allowed_public_metadata\";i:223132457;s:24:\"sso_is_two_step_required\";i:734881840;s:26:\"sso_should_hide_login_form\";i:734881840;s:18:\"sso_match_by_email\";i:4261170317;s:21:\"sso_new_user_override\";i:734881840;s:29:\"sso_bypass_default_login_form\";i:734881840;s:10:\"wp_version\";i:1499136715;s:11:\"get_plugins\";i:485791841;s:24:\"get_plugins_action_links\";i:223132457;s:14:\"active_modules\";i:223132457;s:16:\"hosting_provider\";i:769900095;s:6:\"locale\";i:110763218;s:13:\"site_icon_url\";i:734881840;s:5:\"roles\";i:2254693364;s:8:\"timezone\";i:3808505409;}', 'no'),
(570, 'jpsq_sync_checkout', '0:0', 'no'),
(573, 'jetpack_next_sync_time_sync', '1534250879', 'yes'),
(574, 'jetpack_next_sync_time_full-sync-enqueue', '1534250829', 'yes'),
(581, 'hm_custom_css_js_first_activate', '1534250978', 'yes'),
(582, 'hm_custom_javascript_minify', '1', 'yes'),
(583, 'hm_custom_javascript_ver', '1534410598', 'yes'),
(585, 'hm_custom_css_minify', '1', 'yes'),
(586, 'hm_custom_css_ver', '1534411591', 'yes'),
(677, 'lrrfw_basic', 'a:2:{s:30:\"lrrfw_basic_login_redirect_url\";s:0:\"\";s:33:\"lrrfw_basic_register_redirect_url\";s:48:\"https://cndeveloper.loc/favorites/\";}', 'yes'),
(688, '_transient_timeout_external_ip_address_66.249.93.78', '1535014809', 'no'),
(689, '_transient_external_ip_address_66.249.93.78', '87.239.23.31', 'no'),
(712, '_transient_is_multi_author', '0', 'yes'),
(728, '_transient_timeout_external_ip_address_35.205.157.220', '1535108467', 'no'),
(729, '_transient_external_ip_address_35.205.157.220', '87.239.23.31', 'no'),
(746, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1534677074;s:7:\"checked\";a:14:{s:19:\"akismet/akismet.php\";s:5:\"4.0.8\";s:55:\"custom-css-and-javascript/custom-css-and-javascript.php\";s:5:\"2.0.9\";s:43:\"custom-post-type-ui/custom-post-type-ui.php\";s:5:\"1.5.8\";s:9:\"hello.php\";s:3:\"1.7\";s:19:\"jetpack/jetpack.php\";s:5:\"6.4.2\";s:63:\"login-register-redirect-for-woocommerce/lrr-for-woocommerce.php\";s:5:\"1.0.0\";s:51:\"mailchimp-for-woocommerce/mailchimp-woocommerce.php\";s:5:\"2.1.9\";s:67:\"woocommerce-simple-registration/woocommerce-simple-registration.php\";s:5:\"1.5.2\";s:27:\"woocommerce/woocommerce.php\";s:5:\"3.4.4\";s:63:\"woocommerce-my-account-widget/woocommerce-my-account-widget.php\";s:5:\"0.6.4\";s:91:\"woocommerce-gateway-paypal-express-checkout/woocommerce-gateway-paypal-express-checkout.php\";s:5:\"1.6.1\";s:45:\"woocommerce-services/woocommerce-services.php\";s:6:\"1.15.1\";s:57:\"woocommerce-gateway-stripe/woocommerce-gateway-stripe.php\";s:5:\"4.1.8\";s:27:\"woosidebars/woosidebars.php\";s:5:\"1.4.5\";}s:8:\"response\";a:1:{s:91:\"woocommerce-gateway-paypal-express-checkout/woocommerce-gateway-paypal-express-checkout.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:57:\"w.org/plugins/woocommerce-gateway-paypal-express-checkout\";s:4:\"slug\";s:43:\"woocommerce-gateway-paypal-express-checkout\";s:6:\"plugin\";s:91:\"woocommerce-gateway-paypal-express-checkout/woocommerce-gateway-paypal-express-checkout.php\";s:11:\"new_version\";s:5:\"1.6.3\";s:3:\"url\";s:74:\"https://wordpress.org/plugins/woocommerce-gateway-paypal-express-checkout/\";s:7:\"package\";s:92:\"https://downloads.wordpress.org/plugin/woocommerce-gateway-paypal-express-checkout.1.6.3.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:96:\"https://ps.w.org/woocommerce-gateway-paypal-express-checkout/assets/icon-256x256.png?rev=1900204\";s:2:\"1x\";s:96:\"https://ps.w.org/woocommerce-gateway-paypal-express-checkout/assets/icon-128x128.png?rev=1900204\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:99:\"https://ps.w.org/woocommerce-gateway-paypal-express-checkout/assets/banner-1544x500.png?rev=1900204\";s:2:\"1x\";s:98:\"https://ps.w.org/woocommerce-gateway-paypal-express-checkout/assets/banner-772x250.png?rev=1900204\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"4.9.8\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:13:{s:19:\"akismet/akismet.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:21:\"w.org/plugins/akismet\";s:4:\"slug\";s:7:\"akismet\";s:6:\"plugin\";s:19:\"akismet/akismet.php\";s:11:\"new_version\";s:5:\"4.0.8\";s:3:\"url\";s:38:\"https://wordpress.org/plugins/akismet/\";s:7:\"package\";s:56:\"https://downloads.wordpress.org/plugin/akismet.4.0.8.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:59:\"https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272\";s:2:\"1x\";s:59:\"https://ps.w.org/akismet/assets/icon-128x128.png?rev=969272\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:61:\"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904\";}s:11:\"banners_rtl\";a:0:{}}s:55:\"custom-css-and-javascript/custom-css-and-javascript.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:39:\"w.org/plugins/custom-css-and-javascript\";s:4:\"slug\";s:25:\"custom-css-and-javascript\";s:6:\"plugin\";s:55:\"custom-css-and-javascript/custom-css-and-javascript.php\";s:11:\"new_version\";s:5:\"2.0.9\";s:3:\"url\";s:56:\"https://wordpress.org/plugins/custom-css-and-javascript/\";s:7:\"package\";s:74:\"https://downloads.wordpress.org/plugin/custom-css-and-javascript.2.0.9.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:78:\"https://ps.w.org/custom-css-and-javascript/assets/icon-256x256.png?rev=1311319\";s:2:\"1x\";s:78:\"https://ps.w.org/custom-css-and-javascript/assets/icon-128x128.png?rev=1311319\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:81:\"https://ps.w.org/custom-css-and-javascript/assets/banner-1544x500.png?rev=1333199\";s:2:\"1x\";s:80:\"https://ps.w.org/custom-css-and-javascript/assets/banner-772x250.png?rev=1333199\";}s:11:\"banners_rtl\";a:0:{}}s:43:\"custom-post-type-ui/custom-post-type-ui.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:33:\"w.org/plugins/custom-post-type-ui\";s:4:\"slug\";s:19:\"custom-post-type-ui\";s:6:\"plugin\";s:43:\"custom-post-type-ui/custom-post-type-ui.php\";s:11:\"new_version\";s:5:\"1.5.8\";s:3:\"url\";s:50:\"https://wordpress.org/plugins/custom-post-type-ui/\";s:7:\"package\";s:68:\"https://downloads.wordpress.org/plugin/custom-post-type-ui.1.5.8.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:72:\"https://ps.w.org/custom-post-type-ui/assets/icon-256x256.png?rev=1069557\";s:2:\"1x\";s:72:\"https://ps.w.org/custom-post-type-ui/assets/icon-128x128.png?rev=1069557\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:75:\"https://ps.w.org/custom-post-type-ui/assets/banner-1544x500.png?rev=1069557\";s:2:\"1x\";s:74:\"https://ps.w.org/custom-post-type-ui/assets/banner-772x250.png?rev=1069557\";}s:11:\"banners_rtl\";a:0:{}}s:9:\"hello.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/hello-dolly\";s:4:\"slug\";s:11:\"hello-dolly\";s:6:\"plugin\";s:9:\"hello.php\";s:11:\"new_version\";s:3:\"1.6\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/hello-dolly/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/plugin/hello-dolly.1.6.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:63:\"https://ps.w.org/hello-dolly/assets/icon-256x256.jpg?rev=969907\";s:2:\"1x\";s:63:\"https://ps.w.org/hello-dolly/assets/icon-128x128.jpg?rev=969907\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:65:\"https://ps.w.org/hello-dolly/assets/banner-772x250.png?rev=478342\";}s:11:\"banners_rtl\";a:0:{}}s:19:\"jetpack/jetpack.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:21:\"w.org/plugins/jetpack\";s:4:\"slug\";s:7:\"jetpack\";s:6:\"plugin\";s:19:\"jetpack/jetpack.php\";s:11:\"new_version\";s:5:\"6.4.2\";s:3:\"url\";s:38:\"https://wordpress.org/plugins/jetpack/\";s:7:\"package\";s:56:\"https://downloads.wordpress.org/plugin/jetpack.6.4.2.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:60:\"https://ps.w.org/jetpack/assets/icon-256x256.png?rev=1791404\";s:2:\"1x\";s:52:\"https://ps.w.org/jetpack/assets/icon.svg?rev=1791404\";s:3:\"svg\";s:52:\"https://ps.w.org/jetpack/assets/icon.svg?rev=1791404\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:63:\"https://ps.w.org/jetpack/assets/banner-1544x500.png?rev=1791404\";s:2:\"1x\";s:62:\"https://ps.w.org/jetpack/assets/banner-772x250.png?rev=1791404\";}s:11:\"banners_rtl\";a:0:{}}s:63:\"login-register-redirect-for-woocommerce/lrr-for-woocommerce.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:53:\"w.org/plugins/login-register-redirect-for-woocommerce\";s:4:\"slug\";s:39:\"login-register-redirect-for-woocommerce\";s:6:\"plugin\";s:63:\"login-register-redirect-for-woocommerce/lrr-for-woocommerce.php\";s:11:\"new_version\";s:5:\"1.0.0\";s:3:\"url\";s:70:\"https://wordpress.org/plugins/login-register-redirect-for-woocommerce/\";s:7:\"package\";s:82:\"https://downloads.wordpress.org/plugin/login-register-redirect-for-woocommerce.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:92:\"https://ps.w.org/login-register-redirect-for-woocommerce/assets/icon-256x256.jpg?rev=1829744\";s:2:\"1x\";s:92:\"https://ps.w.org/login-register-redirect-for-woocommerce/assets/icon-256x256.jpg?rev=1829744\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:94:\"https://ps.w.org/login-register-redirect-for-woocommerce/assets/banner-772x250.jpg?rev=1829749\";}s:11:\"banners_rtl\";a:0:{}}s:51:\"mailchimp-for-woocommerce/mailchimp-woocommerce.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:39:\"w.org/plugins/mailchimp-for-woocommerce\";s:4:\"slug\";s:25:\"mailchimp-for-woocommerce\";s:6:\"plugin\";s:51:\"mailchimp-for-woocommerce/mailchimp-woocommerce.php\";s:11:\"new_version\";s:5:\"2.1.9\";s:3:\"url\";s:56:\"https://wordpress.org/plugins/mailchimp-for-woocommerce/\";s:7:\"package\";s:74:\"https://downloads.wordpress.org/plugin/mailchimp-for-woocommerce.2.1.9.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:78:\"https://ps.w.org/mailchimp-for-woocommerce/assets/icon-256x256.png?rev=1509501\";s:2:\"1x\";s:78:\"https://ps.w.org/mailchimp-for-woocommerce/assets/icon-256x256.png?rev=1509501\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:81:\"https://ps.w.org/mailchimp-for-woocommerce/assets/banner-1544x500.png?rev=1557817\";s:2:\"1x\";s:80:\"https://ps.w.org/mailchimp-for-woocommerce/assets/banner-772x250.jpg?rev=1557817\";}s:11:\"banners_rtl\";a:0:{}}s:67:\"woocommerce-simple-registration/woocommerce-simple-registration.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:45:\"w.org/plugins/woocommerce-simple-registration\";s:4:\"slug\";s:31:\"woocommerce-simple-registration\";s:6:\"plugin\";s:67:\"woocommerce-simple-registration/woocommerce-simple-registration.php\";s:11:\"new_version\";s:5:\"1.5.2\";s:3:\"url\";s:62:\"https://wordpress.org/plugins/woocommerce-simple-registration/\";s:7:\"package\";s:80:\"https://downloads.wordpress.org/plugin/woocommerce-simple-registration.1.5.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:84:\"https://ps.w.org/woocommerce-simple-registration/assets/icon-256x256.png?rev=1488276\";s:2:\"1x\";s:84:\"https://ps.w.org/woocommerce-simple-registration/assets/icon-256x256.png?rev=1488276\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:87:\"https://ps.w.org/woocommerce-simple-registration/assets/banner-1544x500.png?rev=1488276\";s:2:\"1x\";s:86:\"https://ps.w.org/woocommerce-simple-registration/assets/banner-772x250.png?rev=1488276\";}s:11:\"banners_rtl\";a:0:{}}s:27:\"woocommerce/woocommerce.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/woocommerce\";s:4:\"slug\";s:11:\"woocommerce\";s:6:\"plugin\";s:27:\"woocommerce/woocommerce.php\";s:11:\"new_version\";s:5:\"3.4.4\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/woocommerce/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/woocommerce.3.4.4.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:64:\"https://ps.w.org/woocommerce/assets/icon-256x256.png?rev=1440831\";s:2:\"1x\";s:64:\"https://ps.w.org/woocommerce/assets/icon-128x128.png?rev=1440831\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/woocommerce/assets/banner-1544x500.png?rev=1629184\";s:2:\"1x\";s:66:\"https://ps.w.org/woocommerce/assets/banner-772x250.png?rev=1629184\";}s:11:\"banners_rtl\";a:0:{}}s:63:\"woocommerce-my-account-widget/woocommerce-my-account-widget.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:43:\"w.org/plugins/woocommerce-my-account-widget\";s:4:\"slug\";s:29:\"woocommerce-my-account-widget\";s:6:\"plugin\";s:63:\"woocommerce-my-account-widget/woocommerce-my-account-widget.php\";s:11:\"new_version\";s:5:\"0.6.4\";s:3:\"url\";s:60:\"https://wordpress.org/plugins/woocommerce-my-account-widget/\";s:7:\"package\";s:78:\"https://downloads.wordpress.org/plugin/woocommerce-my-account-widget.0.6.4.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:81:\"https://ps.w.org/woocommerce-my-account-widget/assets/icon-256x256.png?rev=978833\";s:2:\"1x\";s:73:\"https://ps.w.org/woocommerce-my-account-widget/assets/icon.svg?rev=978833\";s:3:\"svg\";s:73:\"https://ps.w.org/woocommerce-my-account-widget/assets/icon.svg?rev=978833\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:84:\"https://ps.w.org/woocommerce-my-account-widget/assets/banner-1544x500.jpg?rev=690567\";s:2:\"1x\";s:83:\"https://ps.w.org/woocommerce-my-account-widget/assets/banner-772x250.jpg?rev=690567\";}s:11:\"banners_rtl\";a:0:{}}s:45:\"woocommerce-services/woocommerce-services.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:34:\"w.org/plugins/woocommerce-services\";s:4:\"slug\";s:20:\"woocommerce-services\";s:6:\"plugin\";s:45:\"woocommerce-services/woocommerce-services.php\";s:11:\"new_version\";s:6:\"1.15.1\";s:3:\"url\";s:51:\"https://wordpress.org/plugins/woocommerce-services/\";s:7:\"package\";s:70:\"https://downloads.wordpress.org/plugin/woocommerce-services.1.15.1.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:73:\"https://ps.w.org/woocommerce-services/assets/icon-256x256.png?rev=1910075\";s:2:\"1x\";s:73:\"https://ps.w.org/woocommerce-services/assets/icon-128x128.png?rev=1910075\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:76:\"https://ps.w.org/woocommerce-services/assets/banner-1544x500.png?rev=1910075\";s:2:\"1x\";s:75:\"https://ps.w.org/woocommerce-services/assets/banner-772x250.png?rev=1910075\";}s:11:\"banners_rtl\";a:0:{}}s:57:\"woocommerce-gateway-stripe/woocommerce-gateway-stripe.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:40:\"w.org/plugins/woocommerce-gateway-stripe\";s:4:\"slug\";s:26:\"woocommerce-gateway-stripe\";s:6:\"plugin\";s:57:\"woocommerce-gateway-stripe/woocommerce-gateway-stripe.php\";s:11:\"new_version\";s:5:\"4.1.8\";s:3:\"url\";s:57:\"https://wordpress.org/plugins/woocommerce-gateway-stripe/\";s:7:\"package\";s:75:\"https://downloads.wordpress.org/plugin/woocommerce-gateway-stripe.4.1.8.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:79:\"https://ps.w.org/woocommerce-gateway-stripe/assets/icon-256x256.png?rev=1917495\";s:2:\"1x\";s:79:\"https://ps.w.org/woocommerce-gateway-stripe/assets/icon-128x128.png?rev=1917495\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:82:\"https://ps.w.org/woocommerce-gateway-stripe/assets/banner-1544x500.png?rev=1917495\";s:2:\"1x\";s:81:\"https://ps.w.org/woocommerce-gateway-stripe/assets/banner-772x250.png?rev=1917495\";}s:11:\"banners_rtl\";a:0:{}}s:27:\"woosidebars/woosidebars.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/woosidebars\";s:4:\"slug\";s:11:\"woosidebars\";s:6:\"plugin\";s:27:\"woosidebars/woosidebars.php\";s:11:\"new_version\";s:5:\"1.4.5\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/woosidebars/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/woosidebars.1.4.5.zip\";s:5:\"icons\";a:1:{s:7:\"default\";s:62:\"https://s.w.org/plugins/geopattern-icon/woosidebars_a9c7b8.svg\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:66:\"https://ps.w.org/woosidebars/assets/banner-1544x500.png?rev=633896\";s:2:\"1x\";s:65:\"https://ps.w.org/woosidebars/assets/banner-772x250.png?rev=633896\";}s:11:\"banners_rtl\";a:0:{}}}}', 'no'),
(750, '_transient_doing_cron', '1534719618.0671119689941406250000', 'yes'),
(751, '_site_transient_timeout_theme_roots', '1534721419', 'no'),
(752, '_site_transient_theme_roots', 'a:3:{s:13:\"twentyfifteen\";s:7:\"/themes\";s:15:\"twentyseventeen\";s:7:\"/themes\";s:13:\"twentysixteen\";s:7:\"/themes\";}', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `r8ISTpM09o_postmeta`
--

CREATE TABLE `r8ISTpM09o_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `r8ISTpM09o_postmeta`
--

INSERT INTO `r8ISTpM09o_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 3, '_wp_page_template', 'default'),
(3, 5, '_wp_attached_file', '2018/08/espresso.jpg'),
(4, 5, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:2000;s:6:\"height\";i:1200;s:4:\"file\";s:20:\"2018/08/espresso.jpg\";s:5:\"sizes\";a:8:{s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:20:\"espresso-250x250.jpg\";s:5:\"width\";i:250;s:6:\"height\";i:250;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:1;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:20:\"espresso-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:20:\"espresso-350x210.jpg\";s:5:\"width\";i:350;s:6:\"height\";i:210;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"espresso-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"espresso-300x180.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:180;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:20:\"espresso-768x461.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:461;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:21:\"espresso-1024x614.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:614;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:20:\"espresso-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(5, 5, '_starter_content_theme', 'twentyseventeen'),
(6, 5, '_customize_draft_post_name', 'espresso'),
(7, 6, '_wp_attached_file', '2018/08/sandwich.jpg'),
(8, 6, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:2000;s:6:\"height\";i:1200;s:4:\"file\";s:20:\"2018/08/sandwich.jpg\";s:5:\"sizes\";a:8:{s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:20:\"sandwich-250x250.jpg\";s:5:\"width\";i:250;s:6:\"height\";i:250;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:1;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:20:\"sandwich-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:20:\"sandwich-350x210.jpg\";s:5:\"width\";i:350;s:6:\"height\";i:210;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"sandwich-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"sandwich-300x180.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:180;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:20:\"sandwich-768x461.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:461;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:21:\"sandwich-1024x614.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:614;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:20:\"sandwich-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(9, 6, '_starter_content_theme', 'twentyseventeen'),
(10, 6, '_customize_draft_post_name', 'sandwich'),
(11, 7, '_wp_attached_file', '2018/08/coffee.jpg'),
(12, 7, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:2000;s:6:\"height\";i:1200;s:4:\"file\";s:18:\"2018/08/coffee.jpg\";s:5:\"sizes\";a:8:{s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:18:\"coffee-250x250.jpg\";s:5:\"width\";i:250;s:6:\"height\";i:250;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:1;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:18:\"coffee-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:18:\"coffee-350x210.jpg\";s:5:\"width\";i:350;s:6:\"height\";i:210;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"coffee-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:18:\"coffee-300x180.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:180;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:18:\"coffee-768x461.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:461;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:19:\"coffee-1024x614.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:614;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:18:\"coffee-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(13, 7, '_starter_content_theme', 'twentyseventeen'),
(14, 7, '_customize_draft_post_name', 'coffee'),
(15, 8, '_customize_draft_post_name', 'home'),
(16, 8, '_customize_changeset_uuid', 'b2d825d5-f760-49b3-9fc6-e30c49ae7a37'),
(17, 9, '_thumbnail_id', '6'),
(18, 9, '_customize_draft_post_name', 'about'),
(19, 9, '_customize_changeset_uuid', 'b2d825d5-f760-49b3-9fc6-e30c49ae7a37'),
(20, 10, '_thumbnail_id', '5'),
(21, 10, '_customize_draft_post_name', 'contact'),
(22, 10, '_customize_changeset_uuid', 'b2d825d5-f760-49b3-9fc6-e30c49ae7a37'),
(23, 11, '_thumbnail_id', '7'),
(24, 11, '_customize_draft_post_name', 'blog'),
(25, 11, '_customize_changeset_uuid', 'b2d825d5-f760-49b3-9fc6-e30c49ae7a37'),
(26, 12, '_thumbnail_id', '5'),
(27, 12, '_customize_draft_post_name', 'a-homepage-section'),
(28, 12, '_customize_changeset_uuid', 'b2d825d5-f760-49b3-9fc6-e30c49ae7a37'),
(29, 18, '_wc_review_count', '0'),
(30, 18, '_wc_rating_count', 'a:0:{}'),
(31, 18, '_wc_average_rating', '0'),
(32, 18, '_edit_last', '1'),
(33, 18, '_edit_lock', '1534242949:1'),
(36, 20, '_wp_attached_file', '2018/08/interstellar1.jpg'),
(37, 20, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1000;s:6:\"height\";i:1481;s:4:\"file\";s:25:\"2018/08/interstellar1.jpg\";s:5:\"sizes\";a:12:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"interstellar1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:25:\"interstellar1-203x300.jpg\";s:5:\"width\";i:203;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:26:\"interstellar1-768x1137.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:1137;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:26:\"interstellar1-691x1024.jpg\";s:5:\"width\";i:691;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:30:\"twentyseventeen-featured-image\";a:4:{s:4:\"file\";s:27:\"interstellar1-1000x1200.jpg\";s:5:\"width\";i:1000;s:6:\"height\";i:1200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:25:\"interstellar1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:25:\"interstellar1-250x250.jpg\";s:5:\"width\";i:250;s:6:\"height\";i:250;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:1;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:25:\"interstellar1-350x518.jpg\";s:5:\"width\";i:350;s:6:\"height\";i:518;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:25:\"interstellar1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:5:{s:4:\"file\";s:25:\"interstellar1-250x250.jpg\";s:5:\"width\";i:250;s:6:\"height\";i:250;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:1;}s:11:\"shop_single\";a:4:{s:4:\"file\";s:25:\"interstellar1-350x518.jpg\";s:5:\"width\";i:350;s:6:\"height\";i:518;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:25:\"interstellar1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(38, 18, '_sku', ''),
(39, 18, '_regular_price', '32'),
(40, 18, '_sale_price', '30'),
(41, 18, '_sale_price_dates_from', ''),
(42, 18, '_sale_price_dates_to', ''),
(43, 18, 'total_sales', '0'),
(44, 18, '_tax_status', 'none'),
(45, 18, '_tax_class', ''),
(46, 18, '_manage_stock', 'no'),
(47, 18, '_backorders', 'no'),
(48, 18, '_sold_individually', 'no'),
(49, 18, '_weight', ''),
(50, 18, '_length', ''),
(51, 18, '_width', ''),
(52, 18, '_height', ''),
(53, 18, '_upsell_ids', 'a:0:{}'),
(54, 18, '_crosssell_ids', 'a:0:{}'),
(55, 18, '_purchase_note', ''),
(56, 18, '_default_attributes', 'a:0:{}'),
(57, 18, '_virtual', 'no'),
(58, 18, '_downloadable', 'no'),
(59, 18, '_product_image_gallery', ''),
(60, 18, '_download_limit', '-1'),
(61, 18, '_download_expiry', '-1'),
(62, 18, '_stock', NULL),
(63, 18, '_stock_status', 'instock'),
(64, 18, '_product_attributes', 'a:0:{}'),
(65, 18, '_product_version', '3.4.4'),
(66, 18, '_price', '30'),
(67, 18, '_thumbnail_id', '20'),
(68, 13, '_customize_restore_dismissed', '1'),
(69, 21, '_edit_lock', '1534243181:1'),
(70, 21, '_wp_trash_meta_status', 'publish'),
(71, 21, '_wp_trash_meta_time', '1534243203'),
(72, 24, '_wp_trash_meta_status', 'publish'),
(73, 24, '_wp_trash_meta_time', '1534243232'),
(74, 26, '_wp_trash_meta_status', 'publish'),
(75, 26, '_wp_trash_meta_time', '1534243518'),
(76, 27, '_wp_trash_meta_status', 'publish'),
(77, 27, '_wp_trash_meta_time', '1534243552'),
(78, 15, '_edit_lock', '1534243538:1'),
(79, 14, '_edit_lock', '1534406969:1'),
(80, 29, '_wp_trash_meta_status', 'publish'),
(81, 29, '_wp_trash_meta_time', '1534244867'),
(82, 17, '_edit_lock', '1534407895:1'),
(110, 33, '_wp_trash_meta_status', 'publish'),
(111, 33, '_wp_trash_meta_time', '1534247445'),
(112, 35, '_wp_trash_meta_status', 'publish'),
(113, 35, '_wp_trash_meta_time', '1534247520'),
(114, 37, '_wp_trash_meta_status', 'publish'),
(115, 37, '_wp_trash_meta_time', '1534247868'),
(116, 38, '_wp_trash_meta_status', 'publish'),
(117, 38, '_wp_trash_meta_time', '1534247968'),
(118, 40, '_edit_lock', '1534248442:1'),
(119, 40, '_edit_last', '1'),
(125, 40, '_sidebar_to_replace', 'sidebar-1'),
(131, 40, '_condition', 'post-15'),
(132, 40, '_condition', 'post-16'),
(133, 40, '_condition', 'post-17'),
(134, 40, '_condition', 'post-14'),
(135, 46, '_edit_lock', '1534410240:1'),
(136, 46, '_edit_last', '1');

-- --------------------------------------------------------

--
-- Table structure for table `r8ISTpM09o_posts`
--

CREATE TABLE `r8ISTpM09o_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `r8ISTpM09o_posts`
--

INSERT INTO `r8ISTpM09o_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2018-08-14 09:01:28', '2018-08-14 09:01:28', 'Welcome to WordPress. This is your first post. Edit or delete it, then start writing!', 'Hello world!', '', 'publish', 'open', 'open', '', 'hello-world', '', '', '2018-08-14 09:01:28', '2018-08-14 09:01:28', '', 0, 'https://cndeveloper.loc/?p=1', 0, 'post', '', 1),
(2, 1, '2018-08-14 09:01:28', '2018-08-14 09:01:28', 'This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:\n\n<blockquote>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin\' caught in the rain.)</blockquote>\n\n...or something like this:\n\n<blockquote>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</blockquote>\n\nAs a new WordPress user, you should go to <a href=\"https://cndeveloper.loc/wp-admin/\">your dashboard</a> to delete this page and create new pages for your content. Have fun!', 'Sample Page', '', 'publish', 'closed', 'open', '', 'sample-page', '', '', '2018-08-14 09:01:28', '2018-08-14 09:01:28', '', 0, 'https://cndeveloper.loc/?page_id=2', 0, 'page', '', 0),
(3, 1, '2018-08-14 09:01:28', '2018-08-14 09:01:28', '<h2>Who we are</h2><p>Our website address is: https://cndeveloper.loc.</p><h2>What personal data we collect and why we collect it</h2><h3>Comments</h3><p>When visitors leave comments on the site we collect the data shown in the comments form, and also the visitor&#8217;s IP address and browser user agent string to help spam detection.</p><p>An anonymized string created from your email address (also called a hash) may be provided to the Gravatar service to see if you are using it. The Gravatar service privacy policy is available here: https://automattic.com/privacy/. After approval of your comment, your profile picture is visible to the public in the context of your comment.</p><h3>Media</h3><p>If you upload images to the website, you should avoid uploading images with embedded location data (EXIF GPS) included. Visitors to the website can download and extract any location data from images on the website.</p><h3>Contact forms</h3><h3>Cookies</h3><p>If you leave a comment on our site you may opt-in to saving your name, email address and website in cookies. These are for your convenience so that you do not have to fill in your details again when you leave another comment. These cookies will last for one year.</p><p>If you have an account and you log in to this site, we will set a temporary cookie to determine if your browser accepts cookies. This cookie contains no personal data and is discarded when you close your browser.</p><p>When you log in, we will also set up several cookies to save your login information and your screen display choices. Login cookies last for two days, and screen options cookies last for a year. If you select &quot;Remember Me&quot;, your login will persist for two weeks. If you log out of your account, the login cookies will be removed.</p><p>If you edit or publish an article, an additional cookie will be saved in your browser. This cookie includes no personal data and simply indicates the post ID of the article you just edited. It expires after 1 day.</p><h3>Embedded content from other websites</h3><p>Articles on this site may include embedded content (e.g. videos, images, articles, etc.). Embedded content from other websites behaves in the exact same way as if the visitor has visited the other website.</p><p>These websites may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracking your interaction with the embedded content if you have an account and are logged in to that website.</p><h3>Analytics</h3><h2>Who we share your data with</h2><h2>How long we retain your data</h2><p>If you leave a comment, the comment and its metadata are retained indefinitely. This is so we can recognize and approve any follow-up comments automatically instead of holding them in a moderation queue.</p><p>For users that register on our website (if any), we also store the personal information they provide in their user profile. All users can see, edit, or delete their personal information at any time (except they cannot change their username). Website administrators can also see and edit that information.</p><h2>What rights you have over your data</h2><p>If you have an account on this site, or have left comments, you can request to receive an exported file of the personal data we hold about you, including any data you have provided to us. You can also request that we erase any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes.</p><h2>Where we send your data</h2><p>Visitor comments may be checked through an automated spam detection service.</p><h2>Your contact information</h2><h2>Additional information</h2><h3>How we protect your data</h3><h3>What data breach procedures we have in place</h3><h3>What third parties we receive data from</h3><h3>What automated decision making and/or profiling we do with user data</h3><h3>Industry regulatory disclosure requirements</h3>', 'Privacy Policy', '', 'draft', 'closed', 'open', '', 'privacy-policy', '', '', '2018-08-14 09:01:28', '2018-08-14 09:01:28', '', 0, 'https://cndeveloper.loc/?page_id=3', 0, 'page', '', 0),
(4, 1, '2018-08-14 09:02:09', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2018-08-14 09:02:09', '0000-00-00 00:00:00', '', 0, 'https://cndeveloper.loc/?p=4', 0, 'post', '', 0),
(5, 1, '2018-08-14 09:07:01', '0000-00-00 00:00:00', '', 'Espresso', '', 'auto-draft', 'open', 'closed', '', '', '', '', '2018-08-14 09:06:59', '0000-00-00 00:00:00', '', 0, 'https://cndeveloper.loc/wp-content/uploads/2018/08/espresso.jpg', 0, 'attachment', 'image/jpeg', 0),
(6, 1, '2018-08-14 09:07:01', '0000-00-00 00:00:00', '', 'Sandwich', '', 'auto-draft', 'open', 'closed', '', '', '', '', '2018-08-14 09:07:01', '0000-00-00 00:00:00', '', 0, 'https://cndeveloper.loc/wp-content/uploads/2018/08/sandwich.jpg', 0, 'attachment', 'image/jpeg', 0),
(7, 1, '2018-08-14 09:07:01', '0000-00-00 00:00:00', '', 'Coffee', '', 'auto-draft', 'open', 'closed', '', '', '', '', '2018-08-14 09:07:01', '0000-00-00 00:00:00', '', 0, 'https://cndeveloper.loc/wp-content/uploads/2018/08/coffee.jpg', 0, 'attachment', 'image/jpeg', 0),
(8, 1, '2018-08-14 09:07:01', '0000-00-00 00:00:00', 'Welcome to your site! This is your homepage, which is what most visitors will see when they come to your site for the first time.', 'Home', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-08-14 09:07:01', '0000-00-00 00:00:00', '', 0, 'https://cndeveloper.loc/?page_id=8', 0, 'page', '', 0),
(9, 1, '2018-08-14 09:07:01', '0000-00-00 00:00:00', 'You might be an artist who would like to introduce yourself and your work here or maybe you&rsquo;re a business with a mission to describe.', 'About', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-08-14 09:07:01', '0000-00-00 00:00:00', '', 0, 'https://cndeveloper.loc/?page_id=9', 0, 'page', '', 0),
(10, 1, '2018-08-14 09:07:01', '0000-00-00 00:00:00', 'This is a page with some basic contact information, such as an address and phone number. You might also try a plugin to add a contact form.', 'Contact', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-08-14 09:07:01', '0000-00-00 00:00:00', '', 0, 'https://cndeveloper.loc/?page_id=10', 0, 'page', '', 0),
(11, 1, '2018-08-14 09:07:01', '0000-00-00 00:00:00', '', 'Blog', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-08-14 09:07:01', '0000-00-00 00:00:00', '', 0, 'https://cndeveloper.loc/?page_id=11', 0, 'page', '', 0),
(12, 1, '2018-08-14 09:07:01', '0000-00-00 00:00:00', 'This is an example of a homepage section. Homepage sections can be any page other than the homepage itself, including the page that shows your latest blog posts.', 'A homepage section', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-08-14 09:07:01', '0000-00-00 00:00:00', '', 0, 'https://cndeveloper.loc/?page_id=12', 0, 'page', '', 0),
(13, 1, '2018-08-14 09:07:01', '0000-00-00 00:00:00', '{\n    \"widget_text[2]\": {\n        \"starter_content\": true,\n        \"value\": {\n            \"encoded_serialized_instance\": \"YTo0OntzOjU6InRpdGxlIjtzOjc6IkZpbmQgVXMiO3M6NDoidGV4dCI7czoxNjg6IjxzdHJvbmc+QWRkcmVzczwvc3Ryb25nPgoxMjMgTWFpbiBTdHJlZXQKTmV3IFlvcmssIE5ZIDEwMDAxCgo8c3Ryb25nPkhvdXJzPC9zdHJvbmc+Ck1vbmRheSZtZGFzaDtGcmlkYXk6IDk6MDBBTSZuZGFzaDs1OjAwUE0KU2F0dXJkYXkgJmFtcDsgU3VuZGF5OiAxMTowMEFNJm5kYXNoOzM6MDBQTSI7czo2OiJmaWx0ZXIiO2I6MTtzOjY6InZpc3VhbCI7YjoxO30=\",\n            \"title\": \"Find Us\",\n            \"is_widget_customizer_js_value\": true,\n            \"instance_hash_key\": \"4ef675689181a86ff7afe93962ef7db9\"\n        },\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-08-14 09:07:01\"\n    },\n    \"widget_search[3]\": {\n        \"starter_content\": true,\n        \"value\": {\n            \"encoded_serialized_instance\": \"YToxOntzOjU6InRpdGxlIjtzOjY6IlNlYXJjaCI7fQ==\",\n            \"title\": \"Search\",\n            \"is_widget_customizer_js_value\": true,\n            \"instance_hash_key\": \"9ce5abaed2cedbfd9fa1b6322823b361\"\n        },\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-08-14 09:07:01\"\n    },\n    \"widget_text[3]\": {\n        \"starter_content\": true,\n        \"value\": {\n            \"encoded_serialized_instance\": \"YTo0OntzOjU6InRpdGxlIjtzOjE1OiJBYm91dCBUaGlzIFNpdGUiO3M6NDoidGV4dCI7czo4NToiVGhpcyBtYXkgYmUgYSBnb29kIHBsYWNlIHRvIGludHJvZHVjZSB5b3Vyc2VsZiBhbmQgeW91ciBzaXRlIG9yIGluY2x1ZGUgc29tZSBjcmVkaXRzLiI7czo2OiJmaWx0ZXIiO2I6MTtzOjY6InZpc3VhbCI7YjoxO30=\",\n            \"title\": \"About This Site\",\n            \"is_widget_customizer_js_value\": true,\n            \"instance_hash_key\": \"70f5ca450a9abba0a6162090219891d0\"\n        },\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-08-14 09:07:01\"\n    },\n    \"sidebars_widgets[sidebar-1]\": {\n        \"starter_content\": true,\n        \"value\": [\n            \"text-2\",\n            \"search-3\",\n            \"text-3\"\n        ],\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-08-14 09:07:01\"\n    },\n    \"widget_text[4]\": {\n        \"starter_content\": true,\n        \"value\": {\n            \"encoded_serialized_instance\": \"YTo0OntzOjU6InRpdGxlIjtzOjc6IkZpbmQgVXMiO3M6NDoidGV4dCI7czoxNjg6IjxzdHJvbmc+QWRkcmVzczwvc3Ryb25nPgoxMjMgTWFpbiBTdHJlZXQKTmV3IFlvcmssIE5ZIDEwMDAxCgo8c3Ryb25nPkhvdXJzPC9zdHJvbmc+Ck1vbmRheSZtZGFzaDtGcmlkYXk6IDk6MDBBTSZuZGFzaDs1OjAwUE0KU2F0dXJkYXkgJmFtcDsgU3VuZGF5OiAxMTowMEFNJm5kYXNoOzM6MDBQTSI7czo2OiJmaWx0ZXIiO2I6MTtzOjY6InZpc3VhbCI7YjoxO30=\",\n            \"title\": \"Find Us\",\n            \"is_widget_customizer_js_value\": true,\n            \"instance_hash_key\": \"4ef675689181a86ff7afe93962ef7db9\"\n        },\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-08-14 09:07:01\"\n    },\n    \"sidebars_widgets[sidebar-2]\": {\n        \"starter_content\": true,\n        \"value\": [\n            \"text-4\"\n        ],\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-08-14 09:07:01\"\n    },\n    \"widget_text[5]\": {\n        \"starter_content\": true,\n        \"value\": {\n            \"encoded_serialized_instance\": \"YTo0OntzOjU6InRpdGxlIjtzOjE1OiJBYm91dCBUaGlzIFNpdGUiO3M6NDoidGV4dCI7czo4NToiVGhpcyBtYXkgYmUgYSBnb29kIHBsYWNlIHRvIGludHJvZHVjZSB5b3Vyc2VsZiBhbmQgeW91ciBzaXRlIG9yIGluY2x1ZGUgc29tZSBjcmVkaXRzLiI7czo2OiJmaWx0ZXIiO2I6MTtzOjY6InZpc3VhbCI7YjoxO30=\",\n            \"title\": \"About This Site\",\n            \"is_widget_customizer_js_value\": true,\n            \"instance_hash_key\": \"70f5ca450a9abba0a6162090219891d0\"\n        },\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-08-14 09:07:01\"\n    },\n    \"widget_search[4]\": {\n        \"starter_content\": true,\n        \"value\": {\n            \"encoded_serialized_instance\": \"YToxOntzOjU6InRpdGxlIjtzOjY6IlNlYXJjaCI7fQ==\",\n            \"title\": \"Search\",\n            \"is_widget_customizer_js_value\": true,\n            \"instance_hash_key\": \"9ce5abaed2cedbfd9fa1b6322823b361\"\n        },\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-08-14 09:07:01\"\n    },\n    \"sidebars_widgets[sidebar-3]\": {\n        \"starter_content\": true,\n        \"value\": [\n            \"text-5\",\n            \"search-4\"\n        ],\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-08-14 09:07:01\"\n    },\n    \"nav_menus_created_posts\": {\n        \"starter_content\": true,\n        \"value\": [\n            5,\n            6,\n            7,\n            8,\n            9,\n            10,\n            11,\n            12\n        ],\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-08-14 09:07:01\"\n    },\n    \"nav_menu[-1]\": {\n        \"starter_content\": true,\n        \"value\": {\n            \"name\": \"Top Menu\"\n        },\n        \"type\": \"nav_menu\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-08-14 09:07:01\"\n    },\n    \"nav_menu_item[-1]\": {\n        \"starter_content\": true,\n        \"value\": {\n            \"type\": \"custom\",\n            \"title\": \"Home\",\n            \"url\": \"https://cndeveloper.loc/\",\n            \"position\": 0,\n            \"nav_menu_term_id\": -1,\n            \"object_id\": 0\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-08-14 09:07:01\"\n    },\n    \"nav_menu_item[-2]\": {\n        \"starter_content\": true,\n        \"value\": {\n            \"type\": \"post_type\",\n            \"object\": \"page\",\n            \"object_id\": 9,\n            \"position\": 1,\n            \"nav_menu_term_id\": -1,\n            \"title\": \"About\"\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-08-14 09:07:01\"\n    },\n    \"nav_menu_item[-3]\": {\n        \"starter_content\": true,\n        \"value\": {\n            \"type\": \"post_type\",\n            \"object\": \"page\",\n            \"object_id\": 11,\n            \"position\": 2,\n            \"nav_menu_term_id\": -1,\n            \"title\": \"Blog\"\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-08-14 09:07:01\"\n    },\n    \"nav_menu_item[-4]\": {\n        \"starter_content\": true,\n        \"value\": {\n            \"type\": \"post_type\",\n            \"object\": \"page\",\n            \"object_id\": 10,\n            \"position\": 3,\n            \"nav_menu_term_id\": -1,\n            \"title\": \"Contact\"\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-08-14 09:07:01\"\n    },\n    \"twentyseventeen::nav_menu_locations[top]\": {\n        \"starter_content\": true,\n        \"value\": -1,\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-08-14 09:07:01\"\n    },\n    \"nav_menu[-5]\": {\n        \"starter_content\": true,\n        \"value\": {\n            \"name\": \"Social Links Menu\"\n        },\n        \"type\": \"nav_menu\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-08-14 09:07:01\"\n    },\n    \"nav_menu_item[-5]\": {\n        \"starter_content\": true,\n        \"value\": {\n            \"title\": \"Yelp\",\n            \"url\": \"https://www.yelp.com\",\n            \"position\": 0,\n            \"nav_menu_term_id\": -5,\n            \"object_id\": 0\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-08-14 09:07:01\"\n    },\n    \"nav_menu_item[-6]\": {\n        \"starter_content\": true,\n        \"value\": {\n            \"title\": \"Facebook\",\n            \"url\": \"https://www.facebook.com/wordpress\",\n            \"position\": 1,\n            \"nav_menu_term_id\": -5,\n            \"object_id\": 0\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-08-14 09:07:01\"\n    },\n    \"nav_menu_item[-7]\": {\n        \"starter_content\": true,\n        \"value\": {\n            \"title\": \"Twitter\",\n            \"url\": \"https://twitter.com/wordpress\",\n            \"position\": 2,\n            \"nav_menu_term_id\": -5,\n            \"object_id\": 0\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-08-14 09:07:01\"\n    },\n    \"nav_menu_item[-8]\": {\n        \"starter_content\": true,\n        \"value\": {\n            \"title\": \"Instagram\",\n            \"url\": \"https://www.instagram.com/explore/tags/wordcamp/\",\n            \"position\": 3,\n            \"nav_menu_term_id\": -5,\n            \"object_id\": 0\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-08-14 09:07:01\"\n    },\n    \"nav_menu_item[-9]\": {\n        \"starter_content\": true,\n        \"value\": {\n            \"title\": \"Email\",\n            \"url\": \"mailto:wordpress@example.com\",\n            \"position\": 4,\n            \"nav_menu_term_id\": -5,\n            \"object_id\": 0\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-08-14 09:07:01\"\n    },\n    \"twentyseventeen::nav_menu_locations[social]\": {\n        \"starter_content\": true,\n        \"value\": -5,\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-08-14 09:07:01\"\n    },\n    \"show_on_front\": {\n        \"starter_content\": true,\n        \"value\": \"page\",\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-08-14 09:07:01\"\n    },\n    \"page_on_front\": {\n        \"starter_content\": true,\n        \"value\": 8,\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-08-14 09:07:01\"\n    },\n    \"page_for_posts\": {\n        \"starter_content\": true,\n        \"value\": 11,\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-08-14 09:07:01\"\n    },\n    \"twentyseventeen::panel_1\": {\n        \"starter_content\": true,\n        \"value\": 12,\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-08-14 09:07:01\"\n    },\n    \"twentyseventeen::panel_2\": {\n        \"starter_content\": true,\n        \"value\": 9,\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-08-14 09:07:01\"\n    },\n    \"twentyseventeen::panel_3\": {\n        \"starter_content\": true,\n        \"value\": 11,\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-08-14 09:07:01\"\n    },\n    \"twentyseventeen::panel_4\": {\n        \"starter_content\": true,\n        \"value\": 10,\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-08-14 09:07:01\"\n    }\n}', '', '', 'auto-draft', 'closed', 'closed', '', 'b2d825d5-f760-49b3-9fc6-e30c49ae7a37', '', '', '2018-08-14 09:07:01', '0000-00-00 00:00:00', '', 0, 'https://cndeveloper.loc/?p=13', 0, 'customize_changeset', '', 0),
(14, 1, '2018-08-14 09:57:02', '2018-08-14 09:57:02', '', 'Shop', '', 'publish', 'closed', 'closed', '', 'shop', '', '', '2018-08-14 09:57:02', '2018-08-14 09:57:02', '', 0, 'https://cndeveloper.loc/?page_id=14', 0, 'page', '', 0),
(15, 1, '2018-08-14 09:57:02', '2018-08-14 09:57:02', '[woocommerce_cart]', 'Cart', '', 'publish', 'closed', 'closed', '', 'cart', '', '', '2018-08-14 09:57:02', '2018-08-14 09:57:02', '', 0, 'https://cndeveloper.loc/?page_id=15', 0, 'page', '', 0),
(16, 1, '2018-08-14 09:57:02', '2018-08-14 09:57:02', '[woocommerce_checkout]', 'Checkout', '', 'publish', 'closed', 'closed', '', 'checkout', '', '', '2018-08-14 09:57:02', '2018-08-14 09:57:02', '', 0, 'https://cndeveloper.loc/?page_id=16', 0, 'page', '', 0),
(17, 1, '2018-08-14 09:57:02', '2018-08-14 09:57:02', '[woocommerce_my_account]', 'My account', '', 'publish', 'closed', 'closed', '', 'my-account', '', '', '2018-08-14 09:57:02', '2018-08-14 09:57:02', '', 0, 'https://cndeveloper.loc/?page_id=17', 0, 'page', '', 0),
(18, 1, '2018-08-14 10:30:11', '2018-08-14 10:30:11', '', 'Heading', 'content', 'publish', 'open', 'closed', '', 'heading', '', '', '2018-08-14 10:33:11', '2018-08-14 10:33:11', '', 0, 'https://cndeveloper.loc/?post_type=product&#038;p=18', 0, 'product', '', 0),
(20, 1, '2018-08-14 10:29:35', '2018-08-14 10:29:35', '', 'interstellar1', '', 'inherit', 'open', 'closed', '', 'interstellar1', '', '', '2018-08-14 10:29:35', '2018-08-14 10:29:35', '', 18, 'https://cndeveloper.loc/wp-content/uploads/2018/08/interstellar1.jpg', 0, 'attachment', 'image/jpeg', 0),
(21, 1, '2018-08-14 10:40:02', '2018-08-14 10:40:02', '{\n    \"custom_css[twentyseventeen]\": {\n        \"value\": \".single-product #secondary{\\n\\tdisplay: none !important;\\n}\",\n        \"type\": \"custom_css\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-08-14 10:40:02\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'a34a3270-99c8-4068-af6f-a71abd96a121', '', '', '2018-08-14 10:40:02', '2018-08-14 10:40:02', '', 0, 'https://cndeveloper.loc/?p=21', 0, 'customize_changeset', '', 0),
(22, 1, '2018-08-14 10:40:02', '2018-08-14 10:40:02', '', 'twentyseventeen', '', 'publish', 'closed', 'closed', '', 'twentyseventeen', '', '', '2018-08-14 11:59:28', '2018-08-14 11:59:28', '', 0, 'https://cndeveloper.loc/?p=22', 0, 'custom_css', '', 0),
(23, 1, '2018-08-14 10:40:02', '2018-08-14 10:40:02', '.single-product #secondary{\n	display: none !important;\n}', 'twentyseventeen', '', 'inherit', 'closed', 'closed', '', '22-revision-v1', '', '', '2018-08-14 10:40:02', '2018-08-14 10:40:02', '', 22, 'https://cndeveloper.loc/?p=23', 0, 'revision', '', 0),
(24, 1, '2018-08-14 10:40:32', '2018-08-14 10:40:32', '{\n    \"custom_css[twentyseventeen]\": {\n        \"value\": \".single-product #secondary{\\n\\tdisplay: none !important;\\n}\\n.has-sidebar.woocommerce-page:not(.error404) #primary {\\n    width: 100% !important;\\n}\",\n        \"type\": \"custom_css\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-08-14 10:40:32\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '105850e0-e11c-4ee0-aab7-91fd6778e61e', '', '', '2018-08-14 10:40:32', '2018-08-14 10:40:32', '', 0, 'https://cndeveloper.loc/?p=24', 0, 'customize_changeset', '', 0),
(25, 1, '2018-08-14 10:40:32', '2018-08-14 10:40:32', '.single-product #secondary{\n	display: none !important;\n}\n.has-sidebar.woocommerce-page:not(.error404) #primary {\n    width: 100% !important;\n}', 'twentyseventeen', '', 'inherit', 'closed', 'closed', '', '22-revision-v1', '', '', '2018-08-14 10:40:32', '2018-08-14 10:40:32', '', 22, 'https://cndeveloper.loc/?p=25', 0, 'revision', '', 0),
(26, 1, '2018-08-14 10:45:18', '2018-08-14 10:45:18', '{\n    \"show_on_front\": {\n        \"value\": \"page\",\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-08-14 10:45:18\"\n    },\n    \"page_on_front\": {\n        \"value\": \"14\",\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-08-14 10:45:18\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '9684aff9-8f85-475c-a250-ed75f9ca7ea8', '', '', '2018-08-14 10:45:18', '2018-08-14 10:45:18', '', 0, 'https://cndeveloper.loc/?p=26', 0, 'customize_changeset', '', 0),
(27, 1, '2018-08-14 10:45:52', '2018-08-14 10:45:52', '{\n    \"custom_css[twentyseventeen]\": {\n        \"value\": \"#secondary{\\n\\tdisplay: none !important;\\n}\\n.has-sidebar.woocommerce-page:not(.error404) #primary {\\n    width: 100% !important;\\n}\",\n        \"type\": \"custom_css\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-08-14 10:45:52\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '8fba598f-3281-4640-9b37-b02a9817b49e', '', '', '2018-08-14 10:45:52', '2018-08-14 10:45:52', '', 0, 'https://cndeveloper.loc/?p=27', 0, 'customize_changeset', '', 0),
(28, 1, '2018-08-14 10:45:52', '2018-08-14 10:45:52', '#secondary{\n	display: none !important;\n}\n.has-sidebar.woocommerce-page:not(.error404) #primary {\n    width: 100% !important;\n}', 'twentyseventeen', '', 'inherit', 'closed', 'closed', '', '22-revision-v1', '', '', '2018-08-14 10:45:52', '2018-08-14 10:45:52', '', 22, 'https://cndeveloper.loc/?p=28', 0, 'revision', '', 0),
(29, 1, '2018-08-14 11:07:47', '2018-08-14 11:07:47', '{\n    \"twentyseventeen::header_image\": {\n        \"value\": \"remove-header\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-08-14 11:07:47\"\n    },\n    \"twentyseventeen::header_image_data\": {\n        \"value\": \"remove-header\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-08-14 11:07:47\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'acb04ed4-6616-4094-a682-d2a7f54deb9e', '', '', '2018-08-14 11:07:47', '2018-08-14 11:07:47', '', 0, 'https://cndeveloper.loc/acb04ed4-6616-4094-a682-d2a7f54deb9e/', 0, 'customize_changeset', '', 0),
(33, 1, '2018-08-14 11:50:45', '2018-08-14 11:50:45', '{\n    \"custom_css[twentyseventeen]\": {\n        \"value\": \"/* #secondary{\\n\\tdisplay: none !important;\\n}\\n.has-sidebar.woocommerce-page:not(.error404) #primary {\\n    width: 100% !important;\\n}\\n\\n*/\",\n        \"type\": \"custom_css\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-08-14 11:50:45\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'b82c0fa2-2364-4bfa-88d9-a29809ff89e6', '', '', '2018-08-14 11:50:45', '2018-08-14 11:50:45', '', 0, 'https://cndeveloper.loc/b82c0fa2-2364-4bfa-88d9-a29809ff89e6/', 0, 'customize_changeset', '', 0),
(34, 1, '2018-08-14 11:50:45', '2018-08-14 11:50:45', '/* #secondary{\n	display: none !important;\n}\n.has-sidebar.woocommerce-page:not(.error404) #primary {\n    width: 100% !important;\n}\n\n*/', 'twentyseventeen', '', 'inherit', 'closed', 'closed', '', '22-revision-v1', '', '', '2018-08-14 11:50:45', '2018-08-14 11:50:45', '', 22, 'https://cndeveloper.loc/22-revision-v1/', 0, 'revision', '', 0),
(35, 1, '2018-08-14 11:52:00', '2018-08-14 11:52:00', '{\n    \"twentyseventeen::page_layout\": {\n        \"value\": \"two-column\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-08-14 11:52:00\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'ba92a27b-bc6e-4bd4-8f53-b55f5712ea7b', '', '', '2018-08-14 11:52:00', '2018-08-14 11:52:00', '', 0, 'https://cndeveloper.loc/ba92a27b-bc6e-4bd4-8f53-b55f5712ea7b/', 0, 'customize_changeset', '', 0),
(37, 1, '2018-08-14 11:57:48', '2018-08-14 11:57:48', '{\n    \"blogdescription\": {\n        \"value\": \"\",\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-08-14 11:57:48\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '97ae8b6f-51d6-4544-9034-44821bc4232b', '', '', '2018-08-14 11:57:48', '2018-08-14 11:57:48', '', 0, 'https://cndeveloper.loc/97ae8b6f-51d6-4544-9034-44821bc4232b/', 0, 'customize_changeset', '', 0),
(38, 1, '2018-08-14 11:59:28', '2018-08-14 11:59:28', '{\n    \"custom_css[twentyseventeen]\": {\n        \"value\": \"\",\n        \"type\": \"custom_css\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-08-14 11:59:28\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'cc16c9de-042c-4280-ab92-8a3975f63ae0', '', '', '2018-08-14 11:59:28', '2018-08-14 11:59:28', '', 0, 'https://cndeveloper.loc/cc16c9de-042c-4280-ab92-8a3975f63ae0/', 0, 'customize_changeset', '', 0),
(39, 1, '2018-08-14 11:59:28', '2018-08-14 11:59:28', '', 'twentyseventeen', '', 'inherit', 'closed', 'closed', '', '22-revision-v1', '', '', '2018-08-14 11:59:28', '2018-08-14 11:59:28', '', 22, 'https://cndeveloper.loc/22-revision-v1/', 0, 'revision', '', 0),
(40, 1, '2018-08-14 12:02:38', '2018-08-14 12:02:38', '', 'Shop sidebar', '', 'publish', 'closed', 'closed', '', 'shop-sidebar', '', '', '2018-08-14 12:09:39', '2018-08-14 12:09:39', '', 0, 'https://cndeveloper.loc/?post_type=sidebar&#038;p=40', 0, 'sidebar', '', 0),
(41, 1, '2018-08-14 12:50:59', '2018-08-14 12:50:59', 'jQuery(document).ready( function(){\n	jQuery(\'body.home\').addClass(\'has-sidebar\');\n});', '', '', 'draft', 'closed', 'closed', '', '41', '', '', '2018-08-16 08:42:16', '2018-08-16 08:42:16', '', 0, 'https://cndeveloper.loc/?post_type=hm_custom_javascript&#038;p=41', 0, 'hm_custom_javascript', '', 0),
(42, 1, '2018-08-14 12:51:47', '2018-08-14 12:51:47', '.site-content-contain {\n    padding-top: 50px;\n}', '', '', 'draft', 'closed', 'closed', '', '42', '', '', '2018-08-14 12:52:29', '2018-08-14 12:52:29', '', 0, 'https://cndeveloper.loc/?post_type=hm_custom_css&#038;p=42', 0, 'hm_custom_css', '', 0),
(43, 1, '2018-08-14 12:52:29', '2018-08-14 12:52:29', '.site-content-contain {\n    padding-top: 50px;\n}\n\n.site-content {\n    padding: 0 !important;\n}', '', '', 'draft', 'closed', 'closed', '', '43', '', '', '2018-08-16 07:54:17', '2018-08-16 07:54:17', '', 0, 'https://cndeveloper.loc/?post_type=hm_custom_css&#038;p=43', 0, 'hm_custom_css', '', 0),
(44, 1, '2018-08-16 07:54:17', '2018-08-16 07:54:17', '.site-content-contain {\n    padding-top: 50px;\n}\n\n.site-content {\n    padding: 0 !important;\n}\n\n#content>.wrap{\n	max-width: 1280px;\n}', '', '', 'draft', 'closed', 'closed', '', '44', '', '', '2018-08-16 07:55:01', '2018-08-16 07:55:01', '', 0, 'https://cndeveloper.loc/?post_type=hm_custom_css&#038;p=44', 0, 'hm_custom_css', '', 0),
(45, 1, '2018-08-16 07:55:01', '2018-08-16 07:55:01', '.site-content-contain {\n    padding-top: 50px;\n}\n\n.site-content {\n    padding: 0 !important;\n}\n\n#content>.wrap{\n	max-width: 1280px;\n}\n\n.woocommerce-MyAccount-navigation{\n	display: none;\n}', '', '', 'draft', 'closed', 'closed', '', '45', '', '', '2018-08-16 09:10:20', '2018-08-16 09:10:20', '', 0, 'https://cndeveloper.loc/?post_type=hm_custom_css&#038;p=45', 0, 'hm_custom_css', '', 0),
(46, 1, '2018-08-16 08:03:08', '2018-08-16 08:03:08', '', 'Favorites', '', 'publish', 'closed', 'closed', '', 'favorites', '', '', '2018-08-16 08:03:08', '2018-08-16 08:03:08', '', 0, 'https://cndeveloper.loc/?page_id=46', 0, 'page', '', 0),
(47, 1, '2018-08-16 08:03:08', '2018-08-16 08:03:08', '', 'Favorites', '', 'inherit', 'closed', 'closed', '', '46-revision-v1', '', '', '2018-08-16 08:03:08', '2018-08-16 08:03:08', '', 46, 'https://cndeveloper.loc/46-revision-v1/', 0, 'revision', '', 0),
(49, 1, '2018-08-16 08:42:16', '2018-08-16 08:42:16', 'jQuery(document).ready( function(){\n	jQuery(\'body.home\').addClass(\'has-sidebar\');\n  	jQuery(\'.woocommerce-Button.button\').on(\'click\', function(){\n    	window.location.href = \"https://cndeveloper.loc/favorites/\";\n    });\n});', '', '', 'draft', 'closed', 'closed', '', '49', '', '', '2018-08-16 08:43:23', '2018-08-16 08:43:23', '', 0, 'https://cndeveloper.loc/?post_type=hm_custom_javascript&#038;p=49', 0, 'hm_custom_javascript', '', 0),
(50, 1, '2018-08-16 08:43:23', '2018-08-16 08:43:23', 'jQuery(document).ready( function(){\n	jQuery(\'body.home\').addClass(\'has-sidebar\');\n  	jQuery(\'.woocommerce-Button.button\').on(\'click\', function(){\n      	alert(1);\n    	window.location.href = \"https://cndeveloper.loc/favorites/\";\n    });\n});', '', '', 'draft', 'closed', 'closed', '', '50', '', '', '2018-08-16 08:50:03', '2018-08-16 08:50:03', '', 0, 'https://cndeveloper.loc/?post_type=hm_custom_javascript&#038;p=50', 0, 'hm_custom_javascript', '', 0),
(51, 1, '2018-08-16 08:50:03', '2018-08-16 08:50:03', 'jQuery(document).ready( function(){\n	jQuery(\'body.home\').addClass(\'has-sidebar\');\n  	jQuery(\'.woocommerce-Button.button\').on(\'click\', function(){\n      	\n    	window.location.href = \"https://cndeveloper.loc/favorites/\";\n    });\n});', '', '', 'draft', 'closed', 'closed', '', '51', '', '', '2018-08-16 09:08:30', '2018-08-16 09:08:30', '', 0, 'https://cndeveloper.loc/?post_type=hm_custom_javascript&#038;p=51', 0, 'hm_custom_javascript', '', 0),
(52, 1, '2018-08-16 09:08:30', '2018-08-16 09:08:30', 'jQuery(document).ready( function(){\n	jQuery(\'body.home\').addClass(\'has-sidebar\');\n  	jQuery(\'.woocommerce-LoopProduct-link.woocommerce-loop-product__link\').append(\'<h4 class=\"sub-ttl\">Subheading</h4>\')\n});', '', '', 'draft', 'closed', 'closed', '', '52', '', '', '2018-08-16 09:09:02', '2018-08-16 09:09:02', '', 0, 'https://cndeveloper.loc/?post_type=hm_custom_javascript&#038;p=52', 0, 'hm_custom_javascript', '', 0),
(53, 1, '2018-08-16 09:09:02', '2018-08-16 09:09:02', 'jQuery(document).ready( function(){\n	jQuery(\'body.home\').addClass(\'has-sidebar\');\n  	jQuery(\'.woocommerce-LoopProduct-link.woocommerce-loop-product__link\').prepend(\'<h4 class=\"sub-ttl\">Subheading</h4>\')\n});', '', '', 'draft', 'closed', 'closed', '', '53', '', '', '2018-08-16 09:09:42', '2018-08-16 09:09:42', '', 0, 'https://cndeveloper.loc/?post_type=hm_custom_javascript&#038;p=53', 0, 'hm_custom_javascript', '', 0),
(54, 1, '2018-08-16 09:09:42', '2018-08-16 09:09:42', 'jQuery(document).ready( function(){\n	jQuery(\'body.home\').addClass(\'has-sidebar\');\n  	jQuery(\'.woocommerce-loop-product__title\').append(\'<h4 class=\"sub-ttl\">Subheading</h4>\')\n});', '', '', 'draft', 'closed', 'closed', '', '54', '', '', '2018-08-16 09:09:58', '2018-08-16 09:09:58', '', 0, 'https://cndeveloper.loc/?post_type=hm_custom_javascript&#038;p=54', 0, 'hm_custom_javascript', '', 0),
(55, 1, '2018-08-16 09:09:58', '2018-08-16 09:09:58', 'jQuery(document).ready( function(){\n	jQuery(\'body.home\').addClass(\'has-sidebar\');\n  	jQuery(\'.woocommerce-loop-product__title\').append(\'<h6 class=\"sub-ttl\">Subheading</h6>\')\n});', '', '', 'publish', 'closed', 'closed', '', '55', '', '', '2018-08-16 09:09:58', '2018-08-16 09:09:58', '', 0, 'https://cndeveloper.loc/?post_type=hm_custom_javascript&#038;p=55', 0, 'hm_custom_javascript', '', 0),
(56, 1, '2018-08-16 09:10:20', '2018-08-16 09:10:20', '.site-content-contain {\n    padding-top: 50px;\n}\n\n.site-content {\n    padding: 0 !important;\n}\n\n#content>.wrap{\n	max-width: 1280px;\n}\n\n.woocommerce-MyAccount-navigation{\n	display: none;\n}\n\n.sub-ttl{\n	font-size: 18px;\n}', '', '', 'draft', 'closed', 'closed', '', '56', '', '', '2018-08-16 09:10:31', '2018-08-16 09:10:31', '', 0, 'https://cndeveloper.loc/?post_type=hm_custom_css&#038;p=56', 0, 'hm_custom_css', '', 0),
(57, 1, '2018-08-16 09:10:31', '2018-08-16 09:10:31', '.site-content-contain {\n    padding-top: 50px;\n}\n\n.site-content {\n    padding: 0 !important;\n}\n\n#content>.wrap{\n	max-width: 1280px;\n}\n\n.woocommerce-MyAccount-navigation{\n	display: none;\n}\n\nh6.sub-ttl{\n	font-size: 18px;\n}', '', '', 'draft', 'closed', 'closed', '', '57', '', '', '2018-08-16 09:10:44', '2018-08-16 09:10:44', '', 0, 'https://cndeveloper.loc/?post_type=hm_custom_css&#038;p=57', 0, 'hm_custom_css', '', 0),
(58, 1, '2018-08-16 09:10:44', '2018-08-16 09:10:44', '.site-content-contain {\n    padding-top: 50px;\n}\n\n.site-content {\n    padding: 0 !important;\n}\n\n#content>.wrap{\n	max-width: 1280px;\n}\n\n.woocommerce-MyAccount-navigation{\n	display: none;\n}\n\nh6.sub-ttl{\n	font-size: 14px;\n}', '', '', 'draft', 'closed', 'closed', '', '58', '', '', '2018-08-16 09:11:20', '2018-08-16 09:11:20', '', 0, 'https://cndeveloper.loc/?post_type=hm_custom_css&#038;p=58', 0, 'hm_custom_css', '', 0),
(59, 1, '2018-08-16 09:11:20', '2018-08-16 09:11:20', '.site-content-contain {\n    padding-top: 50px;\n}\n\n.site-content {\n    padding: 0 !important;\n}\n\n#content>.wrap{\n	max-width: 1280px;\n}\n\n.woocommerce-MyAccount-navigation{\n	display: none;\n}\n\nh6.sub-ttl{\n	font-size: 12px;\n}', '', '', 'draft', 'closed', 'closed', '', '59', '', '', '2018-08-16 09:26:31', '2018-08-16 09:26:31', '', 0, 'https://cndeveloper.loc/?post_type=hm_custom_css&#038;p=59', 0, 'hm_custom_css', '', 0),
(60, 1, '2018-08-16 09:26:31', '2018-08-16 09:26:31', '.site-content-contain {\n    padding-top: 50px;\n}\n\n.site-content {\n    padding: 0 !important;\n}\n\n#content>.wrap, .site-branding>.wrap{\n	max-width: 1280px;\n}\n\n.woocommerce-MyAccount-navigation{\n	display: none;\n}\n\nh6.sub-ttl{\n	font-size: 12px;\n}', '', '', 'publish', 'closed', 'closed', '', '60', '', '', '2018-08-16 09:26:31', '2018-08-16 09:26:31', '', 0, 'https://cndeveloper.loc/?post_type=hm_custom_css&#038;p=60', 0, 'hm_custom_css', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `r8ISTpM09o_queue`
--

CREATE TABLE `r8ISTpM09o_queue` (
  `id` bigint(20) NOT NULL,
  `job` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(1) NOT NULL DEFAULT '0',
  `locked` tinyint(1) NOT NULL DEFAULT '0',
  `locked_at` datetime DEFAULT NULL,
  `available_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `r8ISTpM09o_termmeta`
--

CREATE TABLE `r8ISTpM09o_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `r8ISTpM09o_termmeta`
--

INSERT INTO `r8ISTpM09o_termmeta` (`meta_id`, `term_id`, `meta_key`, `meta_value`) VALUES
(1, 16, 'order', '0'),
(2, 16, 'display_type', ''),
(3, 16, 'thumbnail_id', '0'),
(4, 16, 'product_count_product_cat', '1');

-- --------------------------------------------------------

--
-- Table structure for table `r8ISTpM09o_terms`
--

CREATE TABLE `r8ISTpM09o_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `r8ISTpM09o_terms`
--

INSERT INTO `r8ISTpM09o_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0),
(2, 'simple', 'simple', 0),
(3, 'grouped', 'grouped', 0),
(4, 'variable', 'variable', 0),
(5, 'external', 'external', 0),
(6, 'exclude-from-search', 'exclude-from-search', 0),
(7, 'exclude-from-catalog', 'exclude-from-catalog', 0),
(8, 'featured', 'featured', 0),
(9, 'outofstock', 'outofstock', 0),
(10, 'rated-1', 'rated-1', 0),
(11, 'rated-2', 'rated-2', 0),
(12, 'rated-3', 'rated-3', 0),
(13, 'rated-4', 'rated-4', 0),
(14, 'rated-5', 'rated-5', 0),
(15, 'Uncategorized', 'uncategorized', 0),
(16, 'Movies', 'movies', 0),
(17, 'main menu', 'main-menu', 0);

-- --------------------------------------------------------

--
-- Table structure for table `r8ISTpM09o_term_relationships`
--

CREATE TABLE `r8ISTpM09o_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `r8ISTpM09o_term_relationships`
--

INSERT INTO `r8ISTpM09o_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(18, 2, 0),
(18, 16, 0);

-- --------------------------------------------------------

--
-- Table structure for table `r8ISTpM09o_term_taxonomy`
--

CREATE TABLE `r8ISTpM09o_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `r8ISTpM09o_term_taxonomy`
--

INSERT INTO `r8ISTpM09o_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 1),
(2, 2, 'product_type', '', 0, 1),
(3, 3, 'product_type', '', 0, 0),
(4, 4, 'product_type', '', 0, 0),
(5, 5, 'product_type', '', 0, 0),
(6, 6, 'product_visibility', '', 0, 0),
(7, 7, 'product_visibility', '', 0, 0),
(8, 8, 'product_visibility', '', 0, 0),
(9, 9, 'product_visibility', '', 0, 0),
(10, 10, 'product_visibility', '', 0, 0),
(11, 11, 'product_visibility', '', 0, 0),
(12, 12, 'product_visibility', '', 0, 0),
(13, 13, 'product_visibility', '', 0, 0),
(14, 14, 'product_visibility', '', 0, 0),
(15, 15, 'product_cat', '', 0, 0),
(16, 16, 'product_cat', '', 0, 1),
(17, 17, 'nav_menu', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `r8ISTpM09o_usermeta`
--

CREATE TABLE `r8ISTpM09o_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `r8ISTpM09o_usermeta`
--

INSERT INTO `r8ISTpM09o_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'tudor_dpk09ou5'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'r8ISTpM09o_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'r8ISTpM09o_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', 'wp496_privacy,theme_editor_notice,text_widget_custom_html'),
(15, 1, 'show_welcome_panel', '1'),
(16, 1, 'session_tokens', 'a:10:{s:64:\"fdf4a8f61baed7d5161bc15ef5ca95d988325fce1411187d873a743bec50abfd\";a:4:{s:10:\"expiration\";i:1534418386;s:2:\"ip\";s:11:\"86.122.8.23\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36\";s:5:\"login\";i:1534245586;}s:64:\"f49062fc09a48be4573943b78ef3a9d23e238db380c75b192ce8b23aa9307171\";a:4:{s:10:\"expiration\";i:1534420738;s:2:\"ip\";s:11:\"86.122.8.23\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36\";s:5:\"login\";i:1534247938;}s:64:\"188bd7be3ca8ca56f52ae2a3284e8d96aa7d4a50442444b6db797fb6110c3a49\";a:4:{s:10:\"expiration\";i:1534421451;s:2:\"ip\";s:11:\"86.122.8.23\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36\";s:5:\"login\";i:1534248651;}s:64:\"a26f03061c48d7b6a52ebe04a12bca7d60658d6c45fd668d28b408fc46141908\";a:4:{s:10:\"expiration\";i:1534423419;s:2:\"ip\";s:11:\"86.122.9.33\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36\";s:5:\"login\";i:1534250619;}s:64:\"1bff178a7b69963592faf39b01aaad12e7ccb541fcd48d9ed32f1298e29ae77a\";a:4:{s:10:\"expiration\";i:1534423437;s:2:\"ip\";s:11:\"86.122.9.33\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36\";s:5:\"login\";i:1534250637;}s:64:\"dd20ac7ef51486b98f425d88aeacc8149ff504102d0fb5a7b55befe6729990c0\";a:4:{s:10:\"expiration\";i:1535460243;s:2:\"ip\";s:11:\"86.122.9.33\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36\";s:5:\"login\";i:1534250643;}s:64:\"9e7b0fe2a9ed743829f842495a6dff9572c877679618c08b486a2efa7790f2ca\";a:4:{s:10:\"expiration\";i:1534578830;s:2:\"ip\";s:13:\"89.136.111.62\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36\";s:5:\"login\";i:1534406030;}s:64:\"f38c9c3a0973b94c98ee8badfb9bb3a4fedaf92a304e4cf46f4d7acd5be976d4\";a:4:{s:10:\"expiration\";i:1534581406;s:2:\"ip\";s:13:\"89.136.111.62\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36\";s:5:\"login\";i:1534408606;}s:64:\"bb8a1b53bc8b109a1bc98b1c7f1ded8d1ba0fa840a7911f3f017f726049f213a\";a:4:{s:10:\"expiration\";i:1534581784;s:2:\"ip\";s:13:\"89.136.111.62\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36\";s:5:\"login\";i:1534408984;}s:64:\"f9977b4feb20b193ab6494a7084e7cebf3b660100f10a45f9d1acfd112e5c392\";a:4:{s:10:\"expiration\";i:1534583196;s:2:\"ip\";s:13:\"89.136.111.62\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36\";s:5:\"login\";i:1534410396;}}'),
(17, 1, 'r8ISTpM09o_dashboard_quick_press_last_post_id', '4'),
(18, 1, 'community-events-location', 'a:1:{s:2:\"ip\";s:12:\"89.136.111.0\";}'),
(19, 1, 'jetpack_tracks_anon_id', 'jetpack:kDwtbDmQFoACeLcA1Hf+8igu'),
(20, 1, '_woocommerce_persistent_cart_1', 'a:1:{s:4:\"cart\";a:1:{s:32:\"6f4922f45568161a8cdf4ad2299f6d23\";a:11:{s:3:\"key\";s:32:\"6f4922f45568161a8cdf4ad2299f6d23\";s:10:\"product_id\";i:18;s:12:\"variation_id\";i:0;s:9:\"variation\";a:0:{}s:8:\"quantity\";i:3;s:9:\"data_hash\";s:32:\"b5c1d5ca8bae6d4896cf1807cdf763f0\";s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:90;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:90;s:8:\"line_tax\";i:0;}}}'),
(21, 1, 'wc_last_active', '1534377600'),
(22, 1, 'r8ISTpM09o_user-settings', 'libraryContent=browse&editor=tinymce'),
(23, 1, 'r8ISTpM09o_user-settings-time', '1534248199'),
(24, 1, 'closedpostboxes_product', 'a:1:{i:0;s:10:\"postcustom\";}'),
(25, 1, 'metaboxhidden_product', 'a:1:{i:0;s:7:\"slugdiv\";}'),
(26, 1, 'meta-box-order_product', 'a:3:{s:4:\"side\";s:84:\"submitdiv,product_catdiv,tagsdiv-product_tag,postimagediv,woocommerce-product-images\";s:6:\"normal\";s:67:\"woocommerce-product-data,postcustom,slugdiv,postexcerpt,commentsdiv\";s:8:\"advanced\";s:0:\"\";}'),
(27, 1, 'screen_layout_product', '2'),
(28, 1, 'managenav-menuscolumnshidden', 'a:4:{i:0;s:11:\"link-target\";i:1;s:15:\"title-attribute\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";}'),
(29, 1, 'metaboxhidden_nav-menus', 'a:5:{i:0;s:21:\"add-post-type-product\";i:1;s:12:\"add-post_tag\";i:2;s:15:\"add-post_format\";i:3;s:15:\"add-product_cat\";i:4;s:15:\"add-product_tag\";}'),
(30, 1, 'nav_menu_recently_edited', '17'),
(31, 2, 'nickname', 'muranyiclara'),
(32, 2, 'first_name', ''),
(33, 2, 'last_name', ''),
(34, 2, 'description', ''),
(35, 2, 'rich_editing', 'true'),
(36, 2, 'syntax_highlighting', 'true'),
(37, 2, 'comment_shortcuts', 'false'),
(38, 2, 'admin_color', 'fresh'),
(39, 2, 'use_ssl', '0'),
(40, 2, 'show_admin_bar_front', 'true'),
(41, 2, 'locale', ''),
(42, 2, 'r8ISTpM09o_capabilities', 'a:1:{s:8:\"customer\";b:1;}'),
(43, 2, 'r8ISTpM09o_user_level', '0'),
(45, 2, '_woocommerce_persistent_cart_1', 'a:1:{s:4:\"cart\";a:0:{}}'),
(46, 2, 'wc_last_active', '1534204800'),
(47, 3, 'nickname', 'alba.lincoln'),
(48, 3, 'first_name', 'fasfa'),
(49, 3, 'last_name', 'fasfa'),
(50, 3, 'description', ''),
(51, 3, 'rich_editing', 'true'),
(52, 3, 'syntax_highlighting', 'true'),
(53, 3, 'comment_shortcuts', 'false'),
(54, 3, 'admin_color', 'fresh'),
(55, 3, 'use_ssl', '0'),
(56, 3, 'show_admin_bar_front', 'true'),
(57, 3, 'locale', ''),
(58, 3, 'r8ISTpM09o_capabilities', 'a:1:{s:8:\"customer\";b:1;}'),
(59, 3, 'r8ISTpM09o_user_level', '0'),
(60, 3, 'last_update', '1534406557'),
(62, 3, '_woocommerce_persistent_cart_1', 'a:1:{s:4:\"cart\";a:0:{}}'),
(63, 3, 'wc_last_active', '1534377600'),
(64, 4, 'nickname', 'sasaa'),
(65, 4, 'first_name', 'fasfa'),
(66, 4, 'last_name', 'fasfa'),
(67, 4, 'description', ''),
(68, 4, 'rich_editing', 'true'),
(69, 4, 'syntax_highlighting', 'true'),
(70, 4, 'comment_shortcuts', 'false'),
(71, 4, 'admin_color', 'fresh'),
(72, 4, 'use_ssl', '0'),
(73, 4, 'show_admin_bar_front', 'true'),
(74, 4, 'locale', ''),
(75, 4, 'r8ISTpM09o_capabilities', 'a:1:{s:8:\"customer\";b:1;}'),
(76, 4, 'r8ISTpM09o_user_level', '0'),
(77, 4, 'last_update', '1534407226'),
(79, 4, '_woocommerce_persistent_cart_1', 'a:1:{s:4:\"cart\";a:0:{}}'),
(80, 4, 'wc_last_active', '1534377600'),
(81, 5, 'nickname', 'eada'),
(82, 5, 'first_name', 'sdfsdfs'),
(83, 5, 'last_name', 'fdsdfs'),
(84, 5, 'description', ''),
(85, 5, 'rich_editing', 'true'),
(86, 5, 'syntax_highlighting', 'true'),
(87, 5, 'comment_shortcuts', 'false'),
(88, 5, 'admin_color', 'fresh'),
(89, 5, 'use_ssl', '0'),
(90, 5, 'show_admin_bar_front', 'true'),
(91, 5, 'locale', ''),
(92, 5, 'r8ISTpM09o_capabilities', 'a:1:{s:8:\"customer\";b:1;}'),
(93, 5, 'r8ISTpM09o_user_level', '0'),
(94, 5, 'last_update', '1534407339'),
(96, 5, '_woocommerce_persistent_cart_1', 'a:1:{s:4:\"cart\";a:0:{}}'),
(97, 5, 'wc_last_active', '1534377600'),
(98, 6, 'nickname', 'asasa'),
(99, 6, 'first_name', 'fasfa'),
(100, 6, 'last_name', 'dada'),
(101, 6, 'description', ''),
(102, 6, 'rich_editing', 'true'),
(103, 6, 'syntax_highlighting', 'true'),
(104, 6, 'comment_shortcuts', 'false'),
(105, 6, 'admin_color', 'fresh'),
(106, 6, 'use_ssl', '0'),
(107, 6, 'show_admin_bar_front', 'true'),
(108, 6, 'locale', ''),
(109, 6, 'r8ISTpM09o_capabilities', 'a:1:{s:8:\"customer\";b:1;}'),
(110, 6, 'r8ISTpM09o_user_level', '0'),
(111, 6, 'last_update', '1534407441'),
(113, 6, '_woocommerce_persistent_cart_1', 'a:1:{s:4:\"cart\";a:0:{}}'),
(114, 6, 'wc_last_active', '1534377600'),
(115, 7, 'nickname', 'wasa'),
(116, 7, 'first_name', 'sdfsdfs'),
(117, 7, 'last_name', 'dasda'),
(118, 7, 'description', ''),
(119, 7, 'rich_editing', 'true'),
(120, 7, 'syntax_highlighting', 'true'),
(121, 7, 'comment_shortcuts', 'false'),
(122, 7, 'admin_color', 'fresh'),
(123, 7, 'use_ssl', '0'),
(124, 7, 'show_admin_bar_front', 'true'),
(125, 7, 'locale', ''),
(126, 7, 'r8ISTpM09o_capabilities', 'a:1:{s:8:\"customer\";b:1;}'),
(127, 7, 'r8ISTpM09o_user_level', '0'),
(128, 7, 'last_update', '1534407499'),
(130, 7, '_woocommerce_persistent_cart_1', 'a:1:{s:4:\"cart\";a:0:{}}'),
(131, 7, 'wc_last_active', '1534377600'),
(132, 8, 'nickname', 'aadada'),
(133, 8, 'first_name', 'dasda'),
(134, 8, 'last_name', 'dasd'),
(135, 8, 'description', ''),
(136, 8, 'rich_editing', 'true'),
(137, 8, 'syntax_highlighting', 'true'),
(138, 8, 'comment_shortcuts', 'false'),
(139, 8, 'admin_color', 'fresh'),
(140, 8, 'use_ssl', '0'),
(141, 8, 'show_admin_bar_front', 'true'),
(142, 8, 'locale', ''),
(143, 8, 'r8ISTpM09o_capabilities', 'a:1:{s:8:\"customer\";b:1;}'),
(144, 8, 'r8ISTpM09o_user_level', '0'),
(145, 8, 'last_update', '1534407524'),
(147, 8, '_woocommerce_persistent_cart_1', 'a:1:{s:4:\"cart\";a:0:{}}'),
(148, 8, 'wc_last_active', '1534377600'),
(149, 9, 'nickname', 'ascasa'),
(150, 9, 'first_name', 'dasda'),
(151, 9, 'last_name', 'dads'),
(152, 9, 'description', ''),
(153, 9, 'rich_editing', 'true'),
(154, 9, 'syntax_highlighting', 'true'),
(155, 9, 'comment_shortcuts', 'false'),
(156, 9, 'admin_color', 'fresh'),
(157, 9, 'use_ssl', '0'),
(158, 9, 'show_admin_bar_front', 'true'),
(159, 9, 'locale', ''),
(160, 9, 'r8ISTpM09o_capabilities', 'a:1:{s:8:\"customer\";b:1;}'),
(161, 9, 'r8ISTpM09o_user_level', '0'),
(162, 9, 'last_update', '1534407847'),
(164, 9, '_woocommerce_persistent_cart_1', 'a:1:{s:4:\"cart\";a:0:{}}'),
(165, 9, 'wc_last_active', '1534377600'),
(166, 10, 'nickname', 'dasdad'),
(167, 10, 'first_name', 'dsada'),
(168, 10, 'last_name', 'sadsd'),
(169, 10, 'description', ''),
(170, 10, 'rich_editing', 'true'),
(171, 10, 'syntax_highlighting', 'true'),
(172, 10, 'comment_shortcuts', 'false'),
(173, 10, 'admin_color', 'fresh'),
(174, 10, 'use_ssl', '0'),
(175, 10, 'show_admin_bar_front', 'true'),
(176, 10, 'locale', ''),
(177, 10, 'r8ISTpM09o_capabilities', 'a:1:{s:8:\"customer\";b:1;}'),
(178, 10, 'r8ISTpM09o_user_level', '0'),
(179, 10, 'last_update', '1534407885'),
(181, 10, '_woocommerce_persistent_cart_1', 'a:1:{s:4:\"cart\";a:0:{}}'),
(182, 10, 'wc_last_active', '1534377600'),
(183, 11, 'nickname', 'hsa'),
(184, 11, 'first_name', 'dasda'),
(185, 11, 'last_name', 'ccasa'),
(186, 11, 'description', ''),
(187, 11, 'rich_editing', 'true'),
(188, 11, 'syntax_highlighting', 'true'),
(189, 11, 'comment_shortcuts', 'false'),
(190, 11, 'admin_color', 'fresh'),
(191, 11, 'use_ssl', '0'),
(192, 11, 'show_admin_bar_front', 'true'),
(193, 11, 'locale', ''),
(194, 11, 'r8ISTpM09o_capabilities', 'a:1:{s:8:\"customer\";b:1;}'),
(195, 11, 'r8ISTpM09o_user_level', '0'),
(196, 11, 'last_update', '1534408010'),
(198, 11, '_woocommerce_persistent_cart_1', 'a:1:{s:4:\"cart\";a:0:{}}'),
(199, 11, 'wc_last_active', '1534377600'),
(200, 12, 'nickname', 'client02'),
(201, 12, 'first_name', ''),
(202, 12, 'last_name', ''),
(203, 12, 'description', ''),
(204, 12, 'rich_editing', 'true'),
(205, 12, 'syntax_highlighting', 'true'),
(206, 12, 'comment_shortcuts', 'false'),
(207, 12, 'admin_color', 'fresh'),
(208, 12, 'use_ssl', '0'),
(209, 12, 'show_admin_bar_front', 'true'),
(210, 12, 'locale', ''),
(211, 12, 'r8ISTpM09o_capabilities', 'a:1:{s:8:\"customer\";b:1;}'),
(212, 12, 'r8ISTpM09o_user_level', '0'),
(214, 12, '_woocommerce_persistent_cart_1', 'a:1:{s:4:\"cart\";a:1:{s:32:\"6f4922f45568161a8cdf4ad2299f6d23\";a:11:{s:3:\"key\";s:32:\"6f4922f45568161a8cdf4ad2299f6d23\";s:10:\"product_id\";i:18;s:12:\"variation_id\";i:0;s:9:\"variation\";a:0:{}s:8:\"quantity\";i:1;s:9:\"data_hash\";s:32:\"b5c1d5ca8bae6d4896cf1807cdf763f0\";s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:30;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:30;s:8:\"line_tax\";i:0;}}}'),
(215, 12, 'wc_last_active', '1534377600'),
(216, 13, 'nickname', 'asdaa'),
(217, 13, 'first_name', 'asda'),
(218, 13, 'last_name', 'casca'),
(219, 13, 'description', ''),
(220, 13, 'rich_editing', 'true'),
(221, 13, 'syntax_highlighting', 'true'),
(222, 13, 'comment_shortcuts', 'false'),
(223, 13, 'admin_color', 'fresh'),
(224, 13, 'use_ssl', '0'),
(225, 13, 'show_admin_bar_front', 'true'),
(226, 13, 'locale', ''),
(227, 13, 'r8ISTpM09o_capabilities', 'a:1:{s:8:\"customer\";b:1;}'),
(228, 13, 'r8ISTpM09o_user_level', '0'),
(229, 13, 'last_update', '1534408960'),
(231, 13, '_woocommerce_persistent_cart_1', 'a:1:{s:4:\"cart\";a:0:{}}'),
(232, 13, 'wc_last_active', '1534377600'),
(233, 14, 'nickname', 'asa'),
(234, 14, 'first_name', 'dasda'),
(235, 14, 'last_name', 'dasda'),
(236, 14, 'description', ''),
(237, 14, 'rich_editing', 'true'),
(238, 14, 'syntax_highlighting', 'true'),
(239, 14, 'comment_shortcuts', 'false'),
(240, 14, 'admin_color', 'fresh'),
(241, 14, 'use_ssl', '0'),
(242, 14, 'show_admin_bar_front', 'true'),
(243, 14, 'locale', ''),
(244, 14, 'r8ISTpM09o_capabilities', 'a:1:{s:8:\"customer\";b:1;}'),
(245, 14, 'r8ISTpM09o_user_level', '0'),
(246, 14, 'last_update', '1534409037'),
(248, 14, '_woocommerce_persistent_cart_1', 'a:1:{s:4:\"cart\";a:0:{}}'),
(249, 14, 'wc_last_active', '1534377600'),
(250, 15, 'nickname', 'lfksalf'),
(251, 15, 'first_name', 'casca'),
(252, 15, 'last_name', 'dafsafa'),
(253, 15, 'description', ''),
(254, 15, 'rich_editing', 'true'),
(255, 15, 'syntax_highlighting', 'true'),
(256, 15, 'comment_shortcuts', 'false'),
(257, 15, 'admin_color', 'fresh'),
(258, 15, 'use_ssl', '0'),
(259, 15, 'show_admin_bar_front', 'true'),
(260, 15, 'locale', ''),
(261, 15, 'r8ISTpM09o_capabilities', 'a:1:{s:8:\"customer\";b:1;}'),
(262, 15, 'r8ISTpM09o_user_level', '0'),
(263, 15, 'last_update', '1534409231'),
(265, 15, '_woocommerce_persistent_cart_1', 'a:1:{s:4:\"cart\";a:0:{}}'),
(266, 15, 'wc_last_active', '1534377600'),
(267, 16, 'nickname', 'yayay'),
(268, 16, 'first_name', 'dasda'),
(269, 16, 'last_name', 'dasda'),
(270, 16, 'description', ''),
(271, 16, 'rich_editing', 'true'),
(272, 16, 'syntax_highlighting', 'true'),
(273, 16, 'comment_shortcuts', 'false'),
(274, 16, 'admin_color', 'fresh'),
(275, 16, 'use_ssl', '0'),
(276, 16, 'show_admin_bar_front', 'true'),
(277, 16, 'locale', ''),
(278, 16, 'r8ISTpM09o_capabilities', 'a:1:{s:8:\"customer\";b:1;}'),
(279, 16, 'r8ISTpM09o_user_level', '0'),
(280, 16, 'last_update', '1534409359'),
(282, 16, '_woocommerce_persistent_cart_1', 'a:1:{s:4:\"cart\";a:0:{}}'),
(283, 16, 'wc_last_active', '1534377600'),
(284, 17, 'nickname', 'faafa'),
(285, 17, 'first_name', 'fsfass'),
(286, 17, 'last_name', 'fsdfs'),
(287, 17, 'description', ''),
(288, 17, 'rich_editing', 'true'),
(289, 17, 'syntax_highlighting', 'true'),
(290, 17, 'comment_shortcuts', 'false'),
(291, 17, 'admin_color', 'fresh'),
(292, 17, 'use_ssl', '0'),
(293, 17, 'show_admin_bar_front', 'true'),
(294, 17, 'locale', ''),
(295, 17, 'r8ISTpM09o_capabilities', 'a:1:{s:8:\"customer\";b:1;}'),
(296, 17, 'r8ISTpM09o_user_level', '0'),
(297, 17, 'last_update', '1534410315'),
(299, 17, '_woocommerce_persistent_cart_1', 'a:1:{s:4:\"cart\";a:0:{}}'),
(300, 17, 'wc_last_active', '1534377600'),
(301, 18, 'nickname', 'fafa'),
(302, 18, 'first_name', 'dasdasd'),
(303, 18, 'last_name', 'afsda'),
(304, 18, 'description', ''),
(305, 18, 'rich_editing', 'true'),
(306, 18, 'syntax_highlighting', 'true'),
(307, 18, 'comment_shortcuts', 'false'),
(308, 18, 'admin_color', 'fresh'),
(309, 18, 'use_ssl', '0'),
(310, 18, 'show_admin_bar_front', 'true'),
(311, 18, 'locale', ''),
(312, 18, 'r8ISTpM09o_capabilities', 'a:1:{s:8:\"customer\";b:1;}'),
(313, 18, 'r8ISTpM09o_user_level', '0'),
(314, 18, 'last_update', '1534410337'),
(316, 18, '_woocommerce_persistent_cart_1', 'a:1:{s:4:\"cart\";a:0:{}}'),
(317, 18, 'wc_last_active', '1534377600'),
(318, 19, 'nickname', 'test'),
(319, 19, 'first_name', 'test'),
(320, 19, 'last_name', 'test2'),
(321, 19, 'description', ''),
(322, 19, 'rich_editing', 'true'),
(323, 19, 'syntax_highlighting', 'true'),
(324, 19, 'comment_shortcuts', 'false'),
(325, 19, 'admin_color', 'fresh'),
(326, 19, 'use_ssl', '0'),
(327, 19, 'show_admin_bar_front', 'true'),
(328, 19, 'locale', ''),
(329, 19, 'r8ISTpM09o_capabilities', 'a:1:{s:8:\"customer\";b:1;}'),
(330, 19, 'r8ISTpM09o_user_level', '0'),
(331, 19, 'last_update', '1534422466'),
(332, 19, 'session_tokens', 'a:1:{s:64:\"b01e28bd22bf16d3ad3953dc7d676292bfd12c229fb4f31ca898cbf80c0e79dd\";a:4:{s:10:\"expiration\";i:1535632067;s:2:\"ip\";s:12:\"86.121.90.58\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36\";s:5:\"login\";i:1534422467;}}'),
(333, 19, '_woocommerce_persistent_cart_1', 'a:1:{s:4:\"cart\";a:0:{}}'),
(334, 19, 'wc_last_active', '1534377600');

-- --------------------------------------------------------

--
-- Table structure for table `r8ISTpM09o_users`
--

CREATE TABLE `r8ISTpM09o_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `r8ISTpM09o_users`
--

INSERT INTO `r8ISTpM09o_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'tudor_dpk09ou5', '$P$BReKR.BiBg9aBJldYLaq8L9TKrbci2/', 'tudor_dpk09ou5', 'tudor@contursoft.com', '', '2018-08-14 09:01:28', '', 0, 'admin'),
(2, 'muranyiclara', '$P$BbveOLCyhF93U1Nx5FUGiIZNdRSBC3.', 'muranyiclara', 'muranyiclara@yahoo.co.uk', '', '2018-08-14 11:54:22', '', 0, 'muranyiclara'),
(3, 'alba.lincoln', '$P$B0wDAp3jEtofnXuJaBUiPKZzJDSIS5.', 'alba-lincoln', 'alba.lincoln@gmail.com', '', '2018-08-16 08:02:37', '', 0, 'alba.lincoln'),
(4, 'sasaa', '$P$B95M7EoXSrFIcmr76YjojdLcOljdFA.', 'sasaa', 'sasaa@yahoo.com', '', '2018-08-16 08:13:46', '', 0, 'sasaa'),
(5, 'eada', '$P$BiToQmQwfnF9VMoPr5Juc67omVzcf2.', 'eada', 'eada@yahoo.com', '', '2018-08-16 08:15:39', '', 0, 'eada'),
(6, 'asasa', '$P$B0gVaifxvqR1HZguoZ941fpVwcwIsm0', 'asasa', 'asasa@yahoo.com', '', '2018-08-16 08:17:21', '', 0, 'asasa'),
(7, 'wasa', '$P$Bt7GFpuG7RgE5Tu/r45nq.cQTeI0H3/', 'wasa', 'wasa@yahoo.com', '', '2018-08-16 08:18:19', '', 0, 'wasa'),
(8, 'aadada', '$P$BvgjGE.dhnCcfSW811F8Kz0wRjI6X1.', 'aadada', 'aadada@yahoo.com', '', '2018-08-16 08:18:43', '', 0, 'aadada'),
(9, 'ascasa', '$P$BbUUNPUfJ.ZiTZAcG3K/C5mfXmbG1L/', 'ascasa', 'ascasa@yahoo.com', '', '2018-08-16 08:24:07', '', 0, 'ascasa'),
(10, 'dasdad', '$P$BLzo0QbKF28w1VZmAVGs8B4ItUkhZl/', 'dasdad', 'dasdad@yahoo.com', '', '2018-08-16 08:24:45', '', 0, 'dasdad'),
(11, 'hsa', '$P$BjNNM67uqR6KGD.ucjYi/DngoNbxM4.', 'hsa', 'hsa@yahoo.com', '', '2018-08-16 08:26:50', '', 0, 'hsa'),
(12, 'client02', '$P$BSfE9tdTQv8LCIqFkchK5Q3IW4qha71', 'client02', 'client02@magazin.ro', '', '2018-08-16 08:41:45', '', 0, 'client02'),
(13, 'asdaa', '$P$BFaetkvArAgRrTFqxp6UjR9YhtTj1o/', 'asdaa', 'asdaa@yahoo.com', '', '2018-08-16 08:42:40', '', 0, 'asdaa'),
(14, 'asa', '$P$BEOrDB.macD5lsbS9aRQYs4QoCoJo1.', 'asa', 'asa@yahoo.com', '', '2018-08-16 08:43:57', '', 0, 'asa'),
(15, 'lfksalf', '$P$BhR/OpyQzn2kbULQrszh5DbFYB.pM81', 'lfksalf', 'lfksalf@yahoo.com', '', '2018-08-16 08:47:11', '', 0, 'lfksalf'),
(16, 'yayay', '$P$Bddxoi.wXasZxzwmlAUwVls7G8kXk90', 'yayay', 'yayay@yahoo.com', '', '2018-08-16 08:49:19', '', 0, 'yayay'),
(17, 'faafa', '$P$BqBkHjs1le5wA3j7YlYlQ/ZVLbOPfI/', 'faafa', 'faafa@yahoo.com', '', '2018-08-16 09:05:15', '', 0, 'faafa'),
(18, 'fafa', '$P$BI8Vyikyf5FdMh2aunQVaeh8xwRxVp.', 'fafa', 'fafa@yahoo.com', '', '2018-08-16 09:05:37', '', 0, 'fafa'),
(19, 'test', '$P$BQyictiZ7OeK.tK1zC1C402VsxBksb/', 'test', 'test@test.ro', '', '2018-08-16 12:27:46', '', 0, 'test');

-- --------------------------------------------------------

--
-- Table structure for table `r8ISTpM09o_wc_download_log`
--

CREATE TABLE `r8ISTpM09o_wc_download_log` (
  `download_log_id` bigint(20) UNSIGNED NOT NULL,
  `timestamp` datetime NOT NULL,
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `user_ip_address` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `r8ISTpM09o_wc_webhooks`
--

CREATE TABLE `r8ISTpM09o_wc_webhooks` (
  `webhook_id` bigint(20) UNSIGNED NOT NULL,
  `status` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `delivery_url` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `topic` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_created_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `api_version` smallint(4) NOT NULL,
  `failure_count` smallint(10) NOT NULL DEFAULT '0',
  `pending_delivery` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `r8ISTpM09o_woocommerce_api_keys`
--

CREATE TABLE `r8ISTpM09o_woocommerce_api_keys` (
  `key_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `description` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permissions` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `consumer_key` char(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `consumer_secret` char(43) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nonces` longtext COLLATE utf8mb4_unicode_ci,
  `truncated_key` char(7) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_access` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `r8ISTpM09o_woocommerce_attribute_taxonomies`
--

CREATE TABLE `r8ISTpM09o_woocommerce_attribute_taxonomies` (
  `attribute_id` bigint(20) UNSIGNED NOT NULL,
  `attribute_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attribute_label` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attribute_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attribute_orderby` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attribute_public` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `r8ISTpM09o_woocommerce_downloadable_product_permissions`
--

CREATE TABLE `r8ISTpM09o_woocommerce_downloadable_product_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `download_id` varchar(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `order_key` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_email` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `downloads_remaining` varchar(9) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `access_granted` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access_expires` datetime DEFAULT NULL,
  `download_count` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `r8ISTpM09o_woocommerce_log`
--

CREATE TABLE `r8ISTpM09o_woocommerce_log` (
  `log_id` bigint(20) UNSIGNED NOT NULL,
  `timestamp` datetime NOT NULL,
  `level` smallint(4) NOT NULL,
  `source` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `context` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `r8ISTpM09o_woocommerce_order_itemmeta`
--

CREATE TABLE `r8ISTpM09o_woocommerce_order_itemmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `order_item_id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `r8ISTpM09o_woocommerce_order_items`
--

CREATE TABLE `r8ISTpM09o_woocommerce_order_items` (
  `order_item_id` bigint(20) UNSIGNED NOT NULL,
  `order_item_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_item_type` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `order_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `r8ISTpM09o_woocommerce_payment_tokenmeta`
--

CREATE TABLE `r8ISTpM09o_woocommerce_payment_tokenmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `payment_token_id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `r8ISTpM09o_woocommerce_payment_tokens`
--

CREATE TABLE `r8ISTpM09o_woocommerce_payment_tokens` (
  `token_id` bigint(20) UNSIGNED NOT NULL,
  `gateway_id` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `type` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `r8ISTpM09o_woocommerce_sessions`
--

CREATE TABLE `r8ISTpM09o_woocommerce_sessions` (
  `session_id` bigint(20) UNSIGNED NOT NULL,
  `session_key` char(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `session_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `session_expiry` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `r8ISTpM09o_woocommerce_shipping_zones`
--

CREATE TABLE `r8ISTpM09o_woocommerce_shipping_zones` (
  `zone_id` bigint(20) UNSIGNED NOT NULL,
  `zone_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `zone_order` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `r8ISTpM09o_woocommerce_shipping_zones`
--

INSERT INTO `r8ISTpM09o_woocommerce_shipping_zones` (`zone_id`, `zone_name`, `zone_order`) VALUES
(1, 'United States (US)', 0);

-- --------------------------------------------------------

--
-- Table structure for table `r8ISTpM09o_woocommerce_shipping_zone_locations`
--

CREATE TABLE `r8ISTpM09o_woocommerce_shipping_zone_locations` (
  `location_id` bigint(20) UNSIGNED NOT NULL,
  `zone_id` bigint(20) UNSIGNED NOT NULL,
  `location_code` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location_type` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `r8ISTpM09o_woocommerce_shipping_zone_locations`
--

INSERT INTO `r8ISTpM09o_woocommerce_shipping_zone_locations` (`location_id`, `zone_id`, `location_code`, `location_type`) VALUES
(1, 1, 'US', 'country');

-- --------------------------------------------------------

--
-- Table structure for table `r8ISTpM09o_woocommerce_shipping_zone_methods`
--

CREATE TABLE `r8ISTpM09o_woocommerce_shipping_zone_methods` (
  `zone_id` bigint(20) UNSIGNED NOT NULL,
  `instance_id` bigint(20) UNSIGNED NOT NULL,
  `method_id` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `method_order` bigint(20) UNSIGNED NOT NULL,
  `is_enabled` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `r8ISTpM09o_woocommerce_tax_rates`
--

CREATE TABLE `r8ISTpM09o_woocommerce_tax_rates` (
  `tax_rate_id` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_country` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tax_rate_state` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tax_rate` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tax_rate_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tax_rate_priority` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_compound` int(1) NOT NULL DEFAULT '0',
  `tax_rate_shipping` int(1) NOT NULL DEFAULT '1',
  `tax_rate_order` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_class` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `r8ISTpM09o_woocommerce_tax_rate_locations`
--

CREATE TABLE `r8ISTpM09o_woocommerce_tax_rate_locations` (
  `location_id` bigint(20) UNSIGNED NOT NULL,
  `location_code` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tax_rate_id` bigint(20) UNSIGNED NOT NULL,
  `location_type` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `r8ISTpM09o_commentmeta`
--
ALTER TABLE `r8ISTpM09o_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `r8ISTpM09o_comments`
--
ALTER TABLE `r8ISTpM09o_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10)),
  ADD KEY `woo_idx_comment_type` (`comment_type`);

--
-- Indexes for table `r8ISTpM09o_failed_jobs`
--
ALTER TABLE `r8ISTpM09o_failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `r8ISTpM09o_links`
--
ALTER TABLE `r8ISTpM09o_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Indexes for table `r8ISTpM09o_options`
--
ALTER TABLE `r8ISTpM09o_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`);

--
-- Indexes for table `r8ISTpM09o_postmeta`
--
ALTER TABLE `r8ISTpM09o_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `r8ISTpM09o_posts`
--
ALTER TABLE `r8ISTpM09o_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Indexes for table `r8ISTpM09o_queue`
--
ALTER TABLE `r8ISTpM09o_queue`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `r8ISTpM09o_termmeta`
--
ALTER TABLE `r8ISTpM09o_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `r8ISTpM09o_terms`
--
ALTER TABLE `r8ISTpM09o_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Indexes for table `r8ISTpM09o_term_relationships`
--
ALTER TABLE `r8ISTpM09o_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Indexes for table `r8ISTpM09o_term_taxonomy`
--
ALTER TABLE `r8ISTpM09o_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Indexes for table `r8ISTpM09o_usermeta`
--
ALTER TABLE `r8ISTpM09o_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `r8ISTpM09o_users`
--
ALTER TABLE `r8ISTpM09o_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- Indexes for table `r8ISTpM09o_wc_download_log`
--
ALTER TABLE `r8ISTpM09o_wc_download_log`
  ADD PRIMARY KEY (`download_log_id`),
  ADD KEY `permission_id` (`permission_id`),
  ADD KEY `timestamp` (`timestamp`);

--
-- Indexes for table `r8ISTpM09o_wc_webhooks`
--
ALTER TABLE `r8ISTpM09o_wc_webhooks`
  ADD PRIMARY KEY (`webhook_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `r8ISTpM09o_woocommerce_api_keys`
--
ALTER TABLE `r8ISTpM09o_woocommerce_api_keys`
  ADD PRIMARY KEY (`key_id`),
  ADD KEY `consumer_key` (`consumer_key`),
  ADD KEY `consumer_secret` (`consumer_secret`);

--
-- Indexes for table `r8ISTpM09o_woocommerce_attribute_taxonomies`
--
ALTER TABLE `r8ISTpM09o_woocommerce_attribute_taxonomies`
  ADD PRIMARY KEY (`attribute_id`),
  ADD KEY `attribute_name` (`attribute_name`(20));

--
-- Indexes for table `r8ISTpM09o_woocommerce_downloadable_product_permissions`
--
ALTER TABLE `r8ISTpM09o_woocommerce_downloadable_product_permissions`
  ADD PRIMARY KEY (`permission_id`),
  ADD KEY `download_order_key_product` (`product_id`,`order_id`,`order_key`(16),`download_id`),
  ADD KEY `download_order_product` (`download_id`,`order_id`,`product_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `r8ISTpM09o_woocommerce_log`
--
ALTER TABLE `r8ISTpM09o_woocommerce_log`
  ADD PRIMARY KEY (`log_id`),
  ADD KEY `level` (`level`);

--
-- Indexes for table `r8ISTpM09o_woocommerce_order_itemmeta`
--
ALTER TABLE `r8ISTpM09o_woocommerce_order_itemmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `order_item_id` (`order_item_id`),
  ADD KEY `meta_key` (`meta_key`(32));

--
-- Indexes for table `r8ISTpM09o_woocommerce_order_items`
--
ALTER TABLE `r8ISTpM09o_woocommerce_order_items`
  ADD PRIMARY KEY (`order_item_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `r8ISTpM09o_woocommerce_payment_tokenmeta`
--
ALTER TABLE `r8ISTpM09o_woocommerce_payment_tokenmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `payment_token_id` (`payment_token_id`),
  ADD KEY `meta_key` (`meta_key`(32));

--
-- Indexes for table `r8ISTpM09o_woocommerce_payment_tokens`
--
ALTER TABLE `r8ISTpM09o_woocommerce_payment_tokens`
  ADD PRIMARY KEY (`token_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `r8ISTpM09o_woocommerce_sessions`
--
ALTER TABLE `r8ISTpM09o_woocommerce_sessions`
  ADD PRIMARY KEY (`session_key`),
  ADD UNIQUE KEY `session_id` (`session_id`);

--
-- Indexes for table `r8ISTpM09o_woocommerce_shipping_zones`
--
ALTER TABLE `r8ISTpM09o_woocommerce_shipping_zones`
  ADD PRIMARY KEY (`zone_id`);

--
-- Indexes for table `r8ISTpM09o_woocommerce_shipping_zone_locations`
--
ALTER TABLE `r8ISTpM09o_woocommerce_shipping_zone_locations`
  ADD PRIMARY KEY (`location_id`),
  ADD KEY `location_id` (`location_id`),
  ADD KEY `location_type_code` (`location_type`(10),`location_code`(20));

--
-- Indexes for table `r8ISTpM09o_woocommerce_shipping_zone_methods`
--
ALTER TABLE `r8ISTpM09o_woocommerce_shipping_zone_methods`
  ADD PRIMARY KEY (`instance_id`);

--
-- Indexes for table `r8ISTpM09o_woocommerce_tax_rates`
--
ALTER TABLE `r8ISTpM09o_woocommerce_tax_rates`
  ADD PRIMARY KEY (`tax_rate_id`),
  ADD KEY `tax_rate_country` (`tax_rate_country`),
  ADD KEY `tax_rate_state` (`tax_rate_state`(2)),
  ADD KEY `tax_rate_class` (`tax_rate_class`(10)),
  ADD KEY `tax_rate_priority` (`tax_rate_priority`);

--
-- Indexes for table `r8ISTpM09o_woocommerce_tax_rate_locations`
--
ALTER TABLE `r8ISTpM09o_woocommerce_tax_rate_locations`
  ADD PRIMARY KEY (`location_id`),
  ADD KEY `tax_rate_id` (`tax_rate_id`),
  ADD KEY `location_type_code` (`location_type`(10),`location_code`(20));

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `r8ISTpM09o_commentmeta`
--
ALTER TABLE `r8ISTpM09o_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `r8ISTpM09o_comments`
--
ALTER TABLE `r8ISTpM09o_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `r8ISTpM09o_failed_jobs`
--
ALTER TABLE `r8ISTpM09o_failed_jobs`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `r8ISTpM09o_links`
--
ALTER TABLE `r8ISTpM09o_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `r8ISTpM09o_options`
--
ALTER TABLE `r8ISTpM09o_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=753;

--
-- AUTO_INCREMENT for table `r8ISTpM09o_postmeta`
--
ALTER TABLE `r8ISTpM09o_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=139;

--
-- AUTO_INCREMENT for table `r8ISTpM09o_posts`
--
ALTER TABLE `r8ISTpM09o_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT for table `r8ISTpM09o_queue`
--
ALTER TABLE `r8ISTpM09o_queue`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `r8ISTpM09o_termmeta`
--
ALTER TABLE `r8ISTpM09o_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `r8ISTpM09o_terms`
--
ALTER TABLE `r8ISTpM09o_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `r8ISTpM09o_term_taxonomy`
--
ALTER TABLE `r8ISTpM09o_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `r8ISTpM09o_usermeta`
--
ALTER TABLE `r8ISTpM09o_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=335;

--
-- AUTO_INCREMENT for table `r8ISTpM09o_users`
--
ALTER TABLE `r8ISTpM09o_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `r8ISTpM09o_wc_download_log`
--
ALTER TABLE `r8ISTpM09o_wc_download_log`
  MODIFY `download_log_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `r8ISTpM09o_wc_webhooks`
--
ALTER TABLE `r8ISTpM09o_wc_webhooks`
  MODIFY `webhook_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `r8ISTpM09o_woocommerce_api_keys`
--
ALTER TABLE `r8ISTpM09o_woocommerce_api_keys`
  MODIFY `key_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `r8ISTpM09o_woocommerce_attribute_taxonomies`
--
ALTER TABLE `r8ISTpM09o_woocommerce_attribute_taxonomies`
  MODIFY `attribute_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `r8ISTpM09o_woocommerce_downloadable_product_permissions`
--
ALTER TABLE `r8ISTpM09o_woocommerce_downloadable_product_permissions`
  MODIFY `permission_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `r8ISTpM09o_woocommerce_log`
--
ALTER TABLE `r8ISTpM09o_woocommerce_log`
  MODIFY `log_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `r8ISTpM09o_woocommerce_order_itemmeta`
--
ALTER TABLE `r8ISTpM09o_woocommerce_order_itemmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `r8ISTpM09o_woocommerce_order_items`
--
ALTER TABLE `r8ISTpM09o_woocommerce_order_items`
  MODIFY `order_item_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `r8ISTpM09o_woocommerce_payment_tokenmeta`
--
ALTER TABLE `r8ISTpM09o_woocommerce_payment_tokenmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `r8ISTpM09o_woocommerce_payment_tokens`
--
ALTER TABLE `r8ISTpM09o_woocommerce_payment_tokens`
  MODIFY `token_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `r8ISTpM09o_woocommerce_sessions`
--
ALTER TABLE `r8ISTpM09o_woocommerce_sessions`
  MODIFY `session_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=173;

--
-- AUTO_INCREMENT for table `r8ISTpM09o_woocommerce_shipping_zones`
--
ALTER TABLE `r8ISTpM09o_woocommerce_shipping_zones`
  MODIFY `zone_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `r8ISTpM09o_woocommerce_shipping_zone_locations`
--
ALTER TABLE `r8ISTpM09o_woocommerce_shipping_zone_locations`
  MODIFY `location_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `r8ISTpM09o_woocommerce_shipping_zone_methods`
--
ALTER TABLE `r8ISTpM09o_woocommerce_shipping_zone_methods`
  MODIFY `instance_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `r8ISTpM09o_woocommerce_tax_rates`
--
ALTER TABLE `r8ISTpM09o_woocommerce_tax_rates`
  MODIFY `tax_rate_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `r8ISTpM09o_woocommerce_tax_rate_locations`
--
ALTER TABLE `r8ISTpM09o_woocommerce_tax_rate_locations`
  MODIFY `location_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `r8ISTpM09o_wc_download_log`
--
ALTER TABLE `r8ISTpM09o_wc_download_log`
  ADD CONSTRAINT `fk_wc_download_log_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `r8ISTpM09o_woocommerce_downloadable_product_permissions` (`permission_id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
